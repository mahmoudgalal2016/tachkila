package net.nasmedia.tachkila.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.fragments.AddPlayersFragment;
import net.nasmedia.tachkila.fragments.AddressBookFragment;
import net.nasmedia.tachkila.fragments.BaseFragment;
import net.nasmedia.tachkila.fragments.EditGroupFragment;
import net.nasmedia.tachkila.fragments.GameDetailsFragment;
import net.nasmedia.tachkila.fragments.GroupDetailsFragment;
import net.nasmedia.tachkila.fragments.InvitePlayersFragment;
import net.nasmedia.tachkila.fragments.InvitePlayersGameFragment;
import net.nasmedia.tachkila.fragments.InviteUserNameFragment;
import net.nasmedia.tachkila.fragments.MatchHistoryFragment;
import net.nasmedia.tachkila.fragments.NewGroupFragment;
import net.nasmedia.tachkila.fragments.NewMatchFragment;
import net.nasmedia.tachkila.fragments.NotificationFragment;
import net.nasmedia.tachkila.fragments.OtherPlayerCardFragment;
import net.nasmedia.tachkila.fragments.SettingFragment;
import net.nasmedia.tachkila.fragments.WebViewFragment;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.widgets.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 03/12/2016.
 */

public class DetailsActivity extends AppCompatActivity {

    private static final String TAG = DetailsActivity.class.getSimpleName();
    @BindView(R.id.toolbarTitle)
    CustomTextView toolbarTitle;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        setToolBar();


        FragmentManager fragmentManager = getSupportFragmentManager();
        BaseFragment baseFragment = null;
        if (getIntent().getAction().equals(Constants.ACTION_SHOW_SETTINGS_FRAGMENT)) {
            baseFragment = SettingFragment.getInstance();
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "SettingFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_ADD_NEW_MATCH_FRAGMENT)) {
            baseFragment = NewMatchFragment.getInstance(getIntent().getIntExtra("groupId", 0));
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "NewMatch").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_ADD_NEW_Group_FRAGMENT)) {
            baseFragment = NewGroupFragment.getInstance();
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "CreateFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_GROUP_DETAILS_FRAGMENT)) {
            baseFragment = GroupDetailsFragment.getInstance(getIntent().getIntExtra(GroupDetailsFragment.ARG_GROUP_ID, 0));
            toolbarTitle.setText(getIntent().getStringExtra("name"));
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "GroupDetails").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_MATCH_HISTORY_FRAGMENT)) {
            baseFragment = MatchHistoryFragment.getInstance(getIntent().getIntExtra(MatchHistoryFragment.ARG_GROUP_ID, 0));
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "MatchHistoryFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_ADD_PLAYERS_FRAGMENT)) {
            baseFragment = AddPlayersFragment.getInstance(getIntent().getIntExtra(AddPlayersFragment.ARG_GROUP_ID, 0));
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "AddPlayersFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT)) {
            baseFragment = OtherPlayerCardFragment.getInstance(getIntent().getIntExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, 0), getIntent().getStringExtra(OtherPlayerCardFragment.ARG_TYPE));
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "OtherPlayerCardFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_NOTIFICATION_FRAGMENT)) {
            baseFragment = NotificationFragment.getInstance();
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "NotificationFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_GAME_DETAILS_FRAGMENT)) {
            baseFragment = GameDetailsFragment.getInstance(getIntent().getIntExtra(GameDetailsFragment.ARG_GAME_ID, 0));
            toolbarTitle.setText(getIntent().getStringExtra("name"));
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "GameDetailsFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_WEBVIEW_FRAGMENT)) {
            baseFragment = WebViewFragment.getInstance(getIntent().getStringExtra("url"));
            toolbarTitle.setText(getIntent().getStringExtra("title"));
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "WebViewFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_INVITE_PLAYERS_FRAGMENT)) {
            baseFragment = InvitePlayersFragment.getInstance();
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "InvitePlayersFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT)) {
            baseFragment = InviteUserNameFragment.getInstance();
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "InviteUserNameFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_ADDRESS_BOOK_FRAGMENT)) {
            baseFragment = AddressBookFragment.getInstance();
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "AddressBookFragment").commit();
        }else if (getIntent().getAction().equals(Constants.ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT)) {
            baseFragment = InvitePlayersGameFragment.getInstance(getIntent().getIntExtra(InvitePlayersGameFragment.ARG_GAME_ID, 0));
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "InvitePlayersGameFragment").commit();
        } else if (getIntent().getAction().equals(Constants.ACTION_SHOW_EDIT_GROUP_FRAGMENT)) {
            baseFragment = EditGroupFragment.getInstance(getIntent().getIntExtra(EditGroupFragment.ARG_GROUP_ID, 0));
            fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "EditGroupFragment").commit();
        } else {

        }

        if (baseFragment.getTitle() != null) {
            setTitle(baseFragment.getTitle());
        }

    }

    public void setTitle(String title) {
        toolbarTitle.setText(title);
    }

    private void setToolBar() {
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailsActivity.this.finish();
            }
        });
        myToolbar.setNavigationIcon(R.drawable.back_chevron);
    }
}
