package net.nasmedia.tachkila.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.fragments.LoginFragment;
import net.nasmedia.tachkila.fragments.RegisterFragment;
import net.nasmedia.tachkila.utils.Constants;

/**
 * Created by mahmoudgalal on 5/21/16.
 */
public class ForumActivity extends AppCompatActivity {

    private FragmentSwitcherReceiver mFragmentSwitcherReceiver = new FragmentSwitcherReceiver();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);

        getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginFragment.getInstance()).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter fragmentSwitcherFilter = new IntentFilter();
        fragmentSwitcherFilter.addCategory(Intent.CATEGORY_DEFAULT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_REGISTER_ACTIVITY);

        // Registers the receiver
        registerReceiver(mFragmentSwitcherReceiver,
                fragmentSwitcherFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(this.mFragmentSwitcherReceiver);
    }

    private class FragmentSwitcherReceiver extends BroadcastReceiver {
        public FragmentSwitcherReceiver() {
            super();
        }

        /**
         * Receives broadcast Intents for content Switching requests , and displays the
         * appropriate Fragment.
         *
         * @param context The current Context of the callback
         * @param intent  The broadcast Intent that triggered the callback
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            handleIntent(intent);
        }

    }

    private void handleIntent(Intent intent) {
        if (intent.getAction().equals(Constants.ACTION_SHOW_REGISTER_ACTIVITY)) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.container, RegisterFragment.getInstance(RegisterFragment.RegisterType.values()[intent.getIntExtra(RegisterFragment.ARG_REGISTER_TYPE, 0)], intent.getParcelableExtra(RegisterFragment.ARG_GOOGLE_ACCOUNT) == null ? null : (GoogleSignInAccount) intent.getParcelableExtra(RegisterFragment.ARG_GOOGLE_ACCOUNT)))
                    .addToBackStack(null)
                    .commit();
        }
    }


}
