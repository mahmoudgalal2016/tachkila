package net.nasmedia.tachkila.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.fragments.BaseFragment;
import net.nasmedia.tachkila.fragments.GroupFragment;
import net.nasmedia.tachkila.fragments.HomeFragment;
import net.nasmedia.tachkila.fragments.MatchHistoryFragment;
import net.nasmedia.tachkila.fragments.PlayerCardFragment;
import net.nasmedia.tachkila.fragments.PlayersContainerFragment;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.CircularProgressBar;
import net.nasmedia.tachkila.widgets.residemenu.ResideMenu;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Mahmoud Galal on 7/15/2016.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbarTitle)
    TextView toolbarTitle;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;
    @BindView(R.id.toolbarImg)
    ImageView toolbarImg;

    ImageView userImg;
    TextView userNameTxt;
    TextView userNickName;
    LinearLayout homeItemContainer;
    LinearLayout playerItemContainer;
    LinearLayout grouptemContainer;
    LinearLayout playerCardItemContainer;
    LinearLayout matchHistoryItemContainer;
    CircularProgressBar mCircularProgressBar;
    TextView mScoreTxt;
    ImageButton mSettingsBtn;

    private FragmentSwitcherReceiver fragmentSwitcherReceiver = new FragmentSwitcherReceiver();

    private ResideMenu mResideMenu;


    private Realm mRealm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });
        myToolbar.setNavigationIcon(R.drawable.menu);

        mResideMenu = new ResideMenu(this, R.layout.left_menu, -1);
        ButterKnife.bind(mResideMenu.getLeftMenuView());
        mResideMenu.setBackground(R.drawable.menu_bkg);
        mResideMenu.attachToActivity(this);
        mResideMenu.setScaleValue(0.5f);

        mResideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);

        grouptemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.grouptemContainer);
        grouptemContainer.setOnClickListener(this);
        homeItemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.homeItemContainer);
        homeItemContainer.setOnClickListener(this);


        playerCardItemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.playerCardItemContainer);
        playerCardItemContainer.setOnClickListener(this);
        playerItemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.playerItemContainer);
        playerItemContainer.setOnClickListener(this);
        matchHistoryItemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.matchHistoryItemContainer);
        matchHistoryItemContainer.setOnClickListener(this);
        mSettingsBtn = (ImageButton) mResideMenu.getLeftMenuView().findViewById(R.id.settingBtn);
        mSettingsBtn.setOnClickListener(this);
        mCircularProgressBar = (CircularProgressBar) mResideMenu.getLeftMenuView().findViewById(R.id.winProgress);
        mScoreTxt = (TextView) mResideMenu.getLeftMenuView().findViewById(R.id.scoreTxt);

        userImg = (ImageView) mResideMenu.getLeftMenuView().findViewById(R.id.userImg);
        userNameTxt = (TextView) mResideMenu.getLeftMenuView().findViewById(R.id.userNameTxt);
        userNickName = (TextView) mResideMenu.getLeftMenuView().findViewById(R.id.userNickName);

        homeItemContainer.setTag("Selected");
        homeItemContainer.setSelected(true);
        playerCardItemContainer.setTag("UnSelected");
        grouptemContainer.setTag("UnSelected");
        playerItemContainer.setTag("UnSelected");
        matchHistoryItemContainer.setTag("UnSelected");

        loadFragment(0, null);
    }

    private void loadImage(String url, ImageView imageView) {
        if (url == null || url.equals("")) {
            imageView.setImageResource(R.drawable.ic_user);
        } else {
            Glide.with(this)
                    .load(Constants.PIC_AVATAR_URL + url)
                    .placeholder(R.drawable.ic_user)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(this).getBitmapPool()))
                    .into(imageView);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mRealm = Realm.getDefaultInstance();
        loadUserData();

        //Intent filter for fragment switching broadcasts
        IntentFilter fragmentSwitcherFilter = new IntentFilter();
        fragmentSwitcherFilter.addCategory(Intent.CATEGORY_DEFAULT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_ADD_PLAYERS_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_NOTIFICATION_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_MATCH_HISTORY_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_WEBVIEW_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_GAME_DETAILS_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_INVITE_PLAYERS_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_ADDRESS_BOOK_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT);


        // Registers the receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(
                fragmentSwitcherReceiver,
                fragmentSwitcherFilter);
        //////////////////////////////////////////////////////////////////
    }

    @Override
    protected void onPause() {
        mRealm.close();
        super.onPause();
        // Unregisters the fragmentSwitcherReceiver instance
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.fragmentSwitcherReceiver);
    }

    private void loadUserData() {
        User user = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(this)).findFirst();
        loadImage(user.getAvatar(), userImg);
        userNameTxt.setText(user.getName());
        userNickName.setText("@" + user.getUsername());
        mScoreTxt.setText(user.getPoints() + "");
        mCircularProgressBar.setTitle("Win " + user.getWin());
        mCircularProgressBar.setLoss("Lose " + user.getLosses());
        mCircularProgressBar.setSubTitle("Total " + (user.getWin() + user.getLosses()));
        mCircularProgressBar.setMax(user.getWin() + user.getLosses());
        mCircularProgressBar.setProgress(user.getWin());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.settingBtn) {
            mResideMenu.closeMenu();
            Intent intent = new Intent(this, DetailsActivity.class);
            intent.setAction(Constants.ACTION_SHOW_SETTINGS_FRAGMENT);
            startActivity(intent);
            return;
        }
        if (!view.getTag().equals("Selected")) {
            unSelectSideMenuItem((LinearLayout) mResideMenu.getLeftMenuView().findViewWithTag("Selected"));
            switch (view.getId()) {
                case R.id.homeItemContainer:
                    selectSideMenuItem(homeItemContainer);
                    loadFragment(0, null);
                    break;
                case R.id.playerCardItemContainer:
                    selectSideMenuItem(playerCardItemContainer);
                    loadFragment(1, null);
                    break;
                case R.id.grouptemContainer:
                    selectSideMenuItem(grouptemContainer);
                    loadFragment(2, null);
                    break;
                case R.id.playerItemContainer:
                    selectSideMenuItem(playerItemContainer);
                    loadFragment(3, null);
                    break;
                case R.id.matchHistoryItemContainer:
                    selectSideMenuItem(matchHistoryItemContainer);
                    Intent intent = new Intent();
                    intent.putExtra(MatchHistoryFragment.ARG_GROUP_ID, 0);
                    loadFragment(4, intent);
                    break;
            }
        }
    }

    private void selectSideMenuItem(LinearLayout view) {
        view.setSelected(true);
        view.setTag("Selected");
        mResideMenu.closeMenu();
    }

    private void unSelectSideMenuItem(LinearLayout view) {
        view.setTag("UnSelected");
        view.setSelected(false);
    }

    public void loadFragment(int fragmentIndex, Intent intent) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        BaseFragment baseFragment = null;
        switch (fragmentIndex) {
            case 0:
                baseFragment = HomeFragment.getInstance();
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "HomeFragment").commit();
                break;
            case 1:
                baseFragment = PlayerCardFragment.getInstance();
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "PLayerCard").commit();
                break;
            case 2:
                baseFragment = GroupFragment.getInstance();
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "Groups").commit();
                break;
            case 3:
                baseFragment = PlayersContainerFragment.getInstance();
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "PlayersContainerFragment").commit();
                break;
            case 4:
                baseFragment = MatchHistoryFragment.getInstance(getIntent().getIntExtra(MatchHistoryFragment.ARG_GROUP_ID, 0));
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "MatchHistoryFragment").commit();
                break;

        }
        if (baseFragment.getTitle() != null) {
            toolbarTitle.setText(baseFragment.getTitle());
            if (baseFragment.getTitle().equals("Home")) {
                toolbarTitle.setVisibility(View.GONE);
                toolbarImg.setImageResource(R.drawable.ic_tachkila_logo);
                toolbarImg.setVisibility(View.VISIBLE);
            } else {
                toolbarTitle.setVisibility(View.VISIBLE);
                toolbarImg.setVisibility(View.GONE);
            }
        }
    }

    private class FragmentSwitcherReceiver extends BroadcastReceiver {
        public FragmentSwitcherReceiver() {
            super();
        }

        /**
         * Receives broadcast Intents for content Switching requests , and displays the
         * appropriate Fragment.
         *
         * @param context The current Context of the callback
         * @param intent  The broadcast Intent that triggered the callback
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION_SHOW_ADD_NEW_MATCH_FRAGMENT)) {
                loadFragment(3, null);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_ADD_NEW_Group_FRAGMENT)) {
                loadFragment(4, null);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_GROUP_DETAILS_FRAGMENT)) {
                loadFragment(5, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_ADD_PLAYERS_FRAGMENT)) {
                loadFragment(6, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_MATCH_HISTORY_FRAGMENT)) {
                loadFragment(8, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT)) {
                loadFragment(9, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_NOTIFICATION_FRAGMENT)) {
                loadFragment(10, null);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_WEBVIEW_FRAGMENT)) {
                loadFragment(12, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_GAME_DETAILS_FRAGMENT)) {
                loadFragment(13, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_INVITE_PLAYERS_FRAGMENT)) {
                loadFragment(14, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT)) {
                loadFragment(15, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_ADDRESS_BOOK_FRAGMENT)) {
                loadFragment(16, null);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT)) {
                loadFragment(17, intent);
            }
        }

    }
}
