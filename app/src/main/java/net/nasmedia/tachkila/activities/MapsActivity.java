package net.nasmedia.tachkila.activities;

import android.app.Dialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.utils.GPSTracker;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        double myLatitude = 0;
        double myLongitude = 0;
        GPSTracker gpsTracker = new GPSTracker(this);
        // Check if GPS enabled
        if (gpsTracker.canGetLocation()) {

            myLatitude = gpsTracker.getLatitude();
            myLongitude = gpsTracker.getLongitude();

            // \n is for new line
        } else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            gpsTracker.showSettingsAlert();
        }

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(myLatitude, myLongitude);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Set match location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        final double finalMyLongitude = myLongitude;
        final double finalMyLatitude = myLatitude;
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(final Marker marker) {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(MapsActivity.this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(finalMyLatitude, finalMyLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses != null && addresses.get(0) != null) {
                        if (addresses.get(0).getAddressLine(0) != null)
                            fullAddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        if (addresses.get(0).getLocality() != null)
                            fullAddress += addresses.get(0).getLocality();
                        if (addresses.get(0).getAdminArea() != null)
                            fullAddress += addresses.get(0).getAdminArea();
                        if (addresses.get(0).getCountryName() != null)
                            fullAddress += addresses.get(0).getCountryName();
                        if (addresses.get(0).getPostalCode() != null)
                            fullAddress += addresses.get(0).getPostalCode();
                        if (addresses.get(0).getFeatureName() != null)
                            fullAddress += addresses.get(0).getFeatureName();

                        Intent intent = new Intent();
                        intent.putExtra("location", fullAddress);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    String fullAddress = "";

}
