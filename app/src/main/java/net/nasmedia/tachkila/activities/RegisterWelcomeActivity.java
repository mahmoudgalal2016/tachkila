package net.nasmedia.tachkila.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.Settings;
import net.nasmedia.tachkila.utils.CustomFontUtils;
import net.nasmedia.tachkila.widgets.CustomTextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by Mahmoud Galal on 18/10/2016.
 */

public class RegisterWelcomeActivity extends AppCompatActivity {

    @BindView(R.id.registerWelcomeTxt)
    CustomTextView mRegisterWelcomeTxt;
    @BindView(R.id.continueBtn)
    Button mContinueBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regiter_welcome);
        ButterKnife.bind(this);

        CustomFontUtils.applyButtonCustomFont(mContinueBtn, CustomFontUtils.LIGHT, this);

        Pattern urlPattern = Pattern.compile(
                "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                        + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                        + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

        String welcomeTxt = Realm.getDefaultInstance().where(Settings.class).findFirst().getFirstWelcomeMessage();
        Matcher matcher = urlPattern.matcher(welcomeTxt);
        while (matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();
            // now you have the offsets of a URL match

            String matchString = welcomeTxt.substring(matchStart, matchEnd);
            welcomeTxt = welcomeTxt.replace(matchString, "<a href=\"" + matchString + "\">" + matchString + "</a> ");

        }

        welcomeTxt = welcomeTxt.replaceAll("\n", "<br />");

        setTextViewHTML(mRegisterWelcomeTxt, welcomeTxt);
    }

    protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                // Do something with span.getURL() to handle the link click...
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(RegisterWelcomeActivity.this.getResources().getColor(R.color.colorPrimary));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(RegisterWelcomeActivity.this, Uri.parse("http://tachkila.com/"));
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    protected void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @OnClick(R.id.continueBtn)
    public void onClick() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
