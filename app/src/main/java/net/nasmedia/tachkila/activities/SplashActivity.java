package net.nasmedia.tachkila.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;
import net.nasmedia.tachkila.viewmodels.SplashViewModel;

public class SplashActivity extends AppCompatActivity implements OnDataLoaded, NewMatchViewModel.OnError {


    private static final String TAG = SplashActivity.class.getSimpleName();
    private SplashViewModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mModel = new SplashViewModel(this, this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mModel.onStart();
    }

    @Override
    protected void onPause() {
        mModel.onStop();
        super.onPause();
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        switch (responseStates) {
            case ERROR:

                break;
            case NO_DATA:

                break;
            case NO_NETWORK:

                break;
            case START:

                break;
            case SUCCESS:
                if (PreferenceHelper.getInstance(this).isUserLogged()) {
                    startActivity(new Intent(this, MainActivity.class));
                }else{
                    startActivity(new Intent(this, ForumActivity.class));
                }

                this.finish();
                break;
        }
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(this)
                .title("Error")
                .content(error)
                .positiveText("OK").show();
    }
}
