package net.nasmedia.tachkila.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.afollestad.materialdialogs.MaterialDialog;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.utils.ConectivityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 19/10/2016.
 */

public class WebViewActivity extends AppCompatActivity {

    @BindView(R.id.webview)
    WebView mWebview;

    String mUrl;
    private MaterialDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_webview);
        ButterKnife.bind(this);

        mUrl =getIntent().getStringExtra("url");
        mProgressDialog = new MaterialDialog.Builder(this)
                .content("Please wait ...")
                .progress(true, 0)
                .build();

        if (ConectivityUtils.isDeviceConnectedToNetwork(this)) {

            mProgressDialog.show();

            mWebview.getSettings().setJavaScriptEnabled(true);
            mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
            mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
            mWebview.getSettings().setJavaScriptEnabled(true);
            mWebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
            mWebview.getSettings().setSupportMultipleWindows(false);
            mWebview.getSettings().setSupportZoom(true);
            mWebview.getSettings().setAllowContentAccess(true);
            mWebview.getSettings().setAllowFileAccess(true);
            mWebview.getSettings().setDatabaseEnabled(true);
            mWebview.getSettings().setDomStorageEnabled(true);
            mWebview.setVerticalScrollBarEnabled(false);
            mWebview.getSettings().setJavaScriptEnabled(true);
            mWebview.getSettings().setLoadWithOverviewMode(true);
            mWebview.getSettings().setUseWideViewPort(false);
            mWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            mWebview.setScrollbarFadingEnabled(false);
            mWebview.setHorizontalScrollBarEnabled(false);
            mWebview.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                }
            });
            mWebview.loadUrl(mUrl);

        } else {
            new MaterialDialog.Builder(this)
                    .title("Error")
                    .content("Please check internet connection")
                    .positiveText("OK").show();
        }
    }
}
