package net.nasmedia.tachkila.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.Settings;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.widgets.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by Mahmoud Galal on 18/10/2016.
 */

public class WelcomeActivity extends AppCompatActivity {

    @BindView(R.id.welcomeTxt)
    CustomTextView mWelcomeTxt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);

//        mWelcomeTxt.setText(Realm.getDefaultInstance().where(Settings.class).findFirst().getWelcomeMessage());


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                finish();
            }
        }, 3000);
    }
}
