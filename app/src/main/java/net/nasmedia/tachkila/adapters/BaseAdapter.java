package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;


import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mahmoud Galal on 9/18/2016.
 */
public abstract class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<My_current_friend> mUsersList;
    Context mContext;

    public BaseAdapter(Context context) {
        mContext = context;
        mUsersList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return mUsersList.size();
    }


    public void setUsers(List<My_current_friend> users) {
        mUsersList = users;
        notifyDataSetChanged();
    }


}
