package net.nasmedia.tachkila.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.ContactModel;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.widgets.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Sherif on 8/18/2016.
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    List<ContactModel> mItems;

    public ContactsAdapter(List<ContactModel> items) {
        mItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        ContactModel groupItem = mItems.get(position);
        holder.contactName.setText(groupItem.getName());
        holder.contactNumber.setText(groupItem.getPhone());
    }

    public String getPhoneNumberAtPosition(int position) {
        return mItems.get(position).getPhone();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.contactName)
        CustomTextView contactName;
        @BindView(R.id.contactNumber)
        CustomTextView contactNumber;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

