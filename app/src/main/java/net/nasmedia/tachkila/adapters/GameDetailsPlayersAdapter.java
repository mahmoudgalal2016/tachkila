package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.GamePlayersWithCaptain;
import net.nasmedia.tachkila.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by mahmoudgalal on 8/23/16.
 */
public class GameDetailsPlayersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GamePlayersWithCaptain> mFriendsList;
    private Context mContext;

    public GameDetailsPlayersAdapter(Context context, List<GamePlayersWithCaptain> friendsList) {
        this.mContext = context;
        mFriendsList = friendsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group_details_player, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        GamePlayersWithCaptain friend = mFriendsList.get(position);
        if (friend.getId() == Utils.getSavedUserIdInSharedPref(mContext)) {
            viewHolder.mPlayerName.setText(friend.getUserName() + " (YOU)");
        } else {
            viewHolder.mPlayerName.setText(friend.getUserName());
        }
        loadImage(Constants.PIC_AVATAR_URL + friend.getAvatar(), viewHolder.mPlayerImg);

    }

    private void loadImage(String url, ImageView imageView) {
        if (url == null || url.equals("")) {
            Glide.with(mContext)
                    .load(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
                    .into(imageView);
        } else {
            Glide.with(mContext)
                    .load(url)
                    .placeholder(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
                    .into(imageView);
        }
    }

    @Override
    public int getItemCount() {
        return mFriendsList.size();
    }

    public GamePlayersWithCaptain getItemAtPosition(int position) {
        return mFriendsList.get(position);
    }

    public void addItemAtPosition(GamePlayersWithCaptain player, int position) {
        mFriendsList.add(position, player);
        notifyDataSetChanged();
    }

    public void removeItemAtPosition(int position) {
        mFriendsList.remove(position);
        notifyItemRemoved(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.playerImg)
        ImageView mPlayerImg;
        @BindView(R.id.playerName)
        TextView mPlayerName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
