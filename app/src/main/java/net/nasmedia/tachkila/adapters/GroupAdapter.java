package net.nasmedia.tachkila.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.fragments.BaseFragment;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.utils.Constants;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Sherif on 8/18/2016.
 */
public class GroupAdapter  extends RecyclerView.Adapter<GroupAdapter.MyViewHolder> {

    ArrayList<Group> mItems;
    BaseFragment mFragment;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageGroup;
        public TextView mTextViewTitleGroup;

        public MyViewHolder(View view) {
            super(view);

            mImageGroup = (ImageView) view.findViewById(R.id.imageViewGroupCardView);
            mTextViewTitleGroup = (TextView) view.findViewById(R.id.textViewGroupCardView);
        }
    }


    public GroupAdapter(ArrayList<Group> items, BaseFragment fragment){
        mItems = items;
        mFragment = fragment;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_group, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Group groupItem = mItems.get(position);

        holder.mTextViewTitleGroup.setText(groupItem.getName());
        Glide.with(mFragment)
                .load(Constants.PIC_GROUPS_URL + groupItem.getAvatar())
                .bitmapTransform(new CropCircleTransformation(Glide.get(mFragment.getActivity()).getBitmapPool()))
                .into(holder.mImageGroup);


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}

