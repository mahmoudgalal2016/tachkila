package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.ActiveGame;
import net.nasmedia.tachkila.realmmodels.test.Finished_game;
import net.nasmedia.tachkila.realmmodels.test.Group;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by mahmoudgalal on 7/28/16.
 */
public class MatchActiveAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ActiveGame> mNotificationList;
    private Context mContext;

    public MatchActiveAdapter(Context context) {
        mContext = context;
        mNotificationList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = createNotificationViewHolder(parent);
        return viewHolder;
    }

    private RecyclerView.ViewHolder createNotificationViewHolder(ViewGroup parent) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_match_active, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        bindTestViewHolder(holder, position);
    }

    private void bindTestViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        ActiveGame notification = mNotificationList.get(position);
        Realm realm = Realm.getDefaultInstance();
        holder.matchLocation.setText(notification.getLocation());
        holder.matchName.setText(realm.where(Group.class).equalTo("id", notification.getGroupId()).findFirst().getName());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date date = simpleDateFormat.parse(notification.getTimeOfGame());
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("EEE, dd MMM yyyy");
            holder.matchDate.setText(simpleDateFormat1.format(date));
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("hh:mm aaa");
            holder.matchTime.setText(simpleDateFormat2.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        holder.message.setText(notification.getText());
//        if (notification.getFriend() != null) {
        loadImage("http://app.tachkila.com/api/game-image/image/" + notification.getId() + "?t=" + Calendar.getInstance().getTimeInMillis(), holder.matchImg);
//        } else {
//            holder.userImg.setVisibility(View.GONE);
//        }
//        holder.time.setText(notification.getCreatedAt());

    }

    private void loadImage(String url, ImageView imageView) {
        Log.d("url", url);
        if (url == null || url.equals("")) {
            Glide.with(mContext)
                    .load(R.drawable.bgk_field)
                    .into(imageView);
        } else {
            Glide.with(mContext)
                    .load(url)
                    .placeholder(R.drawable.bgk_field)
                    .into(imageView);
        }
    }

    public int getGameIdAtPosition(int position) {
        return mNotificationList.get(position).getId();
    }

    private String getPostTime(long postTime) {
        DateTime startTime = new DateTime(postTime);
        DateTime endTime = new DateTime();
        Period p = new Period(startTime, endTime);
        if (p.getDays() == 0 && p.getWeeks() == 0 && p.getMonths() == 0 && p.getYears() == 0) {
            if (p.getHours() == 0 && p.getMinutes() <= 5) {
                return "Now";
            } else if (p.getHours() == 0 && p.getMinutes() > 5) {
                return p.getMinutes() + " m";
            } else if (p.getHours() > 0 && p.getMinutes() == 0) {
                return p.getHours() + " h";
            } else {
                return p.getHours() + " h " + p.getMinutes() + " m";
            }
        } else if (p.getDays() == 1 && p.getWeeks() == 0 && p.getMonths() == 0 && p.getYears() == 0) {
            return "Yesterday";
        } else {
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MMMM dd, yyyy");
            return dtfOut.print(startTime);
        }
    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }


    public void setNotificationList(List<ActiveGame> notifications) {
        this.mNotificationList = notifications;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.matchImg)
        ImageView matchImg;
        @BindView(R.id.matchName)
        TextView matchName;
        @BindView(R.id.matchLocation)
        TextView matchLocation;
        @BindView(R.id.matchDate)
        TextView matchDate;
        @BindView(R.id.matchTime)
        TextView matchTime;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
