package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.test.Notification;
import net.nasmedia.tachkila.utils.Constants;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by mahmoudgalal on 7/28/16.
 */
public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Notification> mNotificationList;
    private Context mContext;

    public NotificationAdapter(Context context) {
        mContext = context;
        mNotificationList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = createNotificationViewHolder(parent);
        return viewHolder;
    }

    private RecyclerView.ViewHolder createNotificationViewHolder(ViewGroup parent) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        bindTestViewHolder(holder, position);
    }

    private void bindTestViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        Notification notification = mNotificationList.get(position);
        holder.mMessage.setText(notification.getText());
        if (notification.getFriend() != null) {
            loadImage(notification.getFriend().getAvatar(), holder.mUserImg);
        } else {
            holder.mUserImg.setImageResource(R.drawable.empty_circl);
        }
        holder.mTime.setText(notification.getCreated_at());

        holder.mBtnRemove.setTag(position);
        holder.mBtnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
                final Notification clickedNotification = mNotificationList.get(position);

                Realm realm =  Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.where(Notification.class).equalTo("id", clickedNotification.getId()).findFirst().deleteFromRealm();
                    }
                });

                realm.close();

            }
        });

    }

    private void loadImage(String url, ImageView imageView) {
        if (url == null || url.equals("")) {
            imageView.setImageResource(R.drawable.empty_circl);
        } else {
            Glide.with(mContext)
                    .load(Constants.PIC_AVATAR_URL + url)
                    .placeholder(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
                    .into(imageView);
        }
    }

    private String getPostTime(long postTime) {
        DateTime startTime = new DateTime(postTime);
        DateTime endTime = new DateTime();
        Period p = new Period(startTime, endTime);
        if (p.getDays() == 0 && p.getWeeks() == 0 && p.getMonths() == 0 && p.getYears() == 0) {
            if (p.getHours() == 0 && p.getMinutes() <= 5) {
                return "Now";
            } else if (p.getHours() == 0 && p.getMinutes() > 5) {
                return p.getMinutes() + " m";
            } else if (p.getHours() > 0 && p.getMinutes() == 0) {
                return p.getHours() + " h";
            } else {
                return p.getHours() + " h " + p.getMinutes() + " m";
            }
        } else if (p.getDays() == 1 && p.getWeeks() == 0 && p.getMonths() == 0 && p.getYears() == 0) {
            return "Yesterday";
        } else {
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MMMM dd, yyyy");
            return dtfOut.print(startTime);
        }
    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }


    public void setNotificationList(List<Notification> notifications) {
        this.mNotificationList = notifications;
        notifyDataSetChanged();
    }

    public Notification getItemAtPosition(int position) {
        return mNotificationList.get(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.userImg)
        ImageView mUserImg;
        @BindView(R.id.message)
        TextView mMessage;
        @BindView(R.id.time)
        TextView mTime;
        @BindView(R.id.btnRemove)
        TextView mBtnRemove;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
