package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.fragments.OtherPlayerCardFragment;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.groups.ViewGroupResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 9/18/2016.
 */
public class RequestsAdapter extends BaseAdapter {

    public RequestsAdapter(Context context) {
        super(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request_full, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        My_current_friend user = mUsersList.get(position);
        holder.userNameTxt.setText(user.getUsername());
        holder.userScoreTxt.setText(user.getWin() + "");
        loadImage(user.getAvatar(), holder.userImg);
        holder.btAccept.setTag(position);
        holder.btAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
                    MaterialDialog.Builder mMaterialDialogBuilder;
                    mMaterialDialogBuilder = new MaterialDialog.Builder(mContext);
                    mMaterialDialogBuilder.content(R.string.Please_wait);
                    mMaterialDialogBuilder.progress(true, 0);
                    final MaterialDialog materialDialog = mMaterialDialogBuilder.show();
                    final int pos = (int) view.getTag();
                    final My_current_friend user1 = mUsersList.get(pos);
                    ApiHandler.getInstance().getServices().makeFriend(Utils.getSavedUserIdInSharedPref(mContext), user1.getId()).enqueue(new Callback<ViewGroupResponse>() {
                        @Override
                        public void onResponse(Call<ViewGroupResponse> call, Response<ViewGroupResponse> response) {
                            materialDialog.dismiss();
                            if (response.isSuccessful() && response.body().getResult().getSuccess() == 1) {
                                notifyItemRemoved(pos);
                                Realm realm = Realm.getDefaultInstance();
                                final User user2 = realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        user2.getMy_current_friends().add(user1);
                                        mUsersList.remove(user1);
                                    }
                                });
                            } else {
                                Toast.makeText(mContext, mContext.getString(R.string.error_happened), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ViewGroupResponse> call, Throwable t) {
                            materialDialog.dismiss();
                            Toast.makeText(mContext, mContext.getString(R.string.error_happened), Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.error_No_Ineternet_connection), Toast.LENGTH_LONG).show();
                }
            }
        });

        holder.itemPlayerContainer.setTag(position);
        holder.itemPlayerContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, mUsersList.get((Integer) view.getTag()).getId());
                intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.REQUEST);
                intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                mContext.startActivity(intent);
            }
        });
    }

    private void loadImage(String url, ImageView imageView) {
        if (url == null || url.equals("")) {
            imageView.setImageResource(R.drawable.empty_circl);
        } else {
            Glide.with(mContext)
                    .load(Constants.PIC_AVATAR_URL + url)
                    .placeholder(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
                    .into(imageView);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.userImg)
        ImageView userImg;
        @BindView(R.id.userNameTxt)
        TextView userNameTxt;
        @BindView(R.id.userScoreTxt)
        TextView userScoreTxt;
        @BindView(R.id.btaccept)
        TextView btAccept;
        @BindView(R.id.smContentView)
        RelativeLayout itemPlayerContainer;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
