package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by mahmoudgalal on 8/23/16.
 */
public class SelectFriendsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<My_current_friend> mFriendsList;
    private List<My_current_friend> mFriendsListCopy;
    private List<My_current_friend> mSelectedFriends = new ArrayList<>();
    private Context mContext;

    public SelectFriendsAdapter(Context context) {
        this.mContext = context;
    }

    public void setFriendsList(List<My_current_friend> friendsList) {
        this.mFriendsList = friendsList;
        mFriendsListCopy = new ArrayList<>();
        mFriendsListCopy.addAll(friendsList);

        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friends_select, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        My_current_friend friend = mFriendsList.get(position);
        viewHolder.mUserNameTxt.setText(friend.getName());
        viewHolder.mUserScoreTxt.setText("" + friend.getPoints());
        loadImage(friend.getAvatar(), viewHolder.mUserImg);
        viewHolder.mSelectChk.setTag(position);
        viewHolder.mSelectChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mSelectedFriends.add(mFriendsList.get((Integer) compoundButton.getTag()));
                } else {
                    mSelectedFriends.remove(mFriendsList.get((Integer) compoundButton.getTag()));
                }
            }
        });

    }

    public List<My_current_friend> getSelectedFriends() {
        return mSelectedFriends;
    }

    private void loadImage(String url, ImageView imageView) {
        if (url == null || url.equals("")) {
            imageView.setImageResource(R.drawable.empty_circl);
        } else {
            Glide.with(mContext)
                    .load(Constants.PIC_AVATAR_URL + url)
                    .placeholder(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
                    .into(imageView);
        }
    }

    @Override
    public int getItemCount() {
        return mFriendsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.userImg)
        ImageView mUserImg;
        @BindView(R.id.userNameTxt)
        TextView mUserNameTxt;
        @BindView(R.id.userScoreTxt)
        TextView mUserScoreTxt;
        @BindView(R.id.selectChk)
        CheckBox mSelectChk;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void filter(String text) {
        mFriendsList.clear();
        if (text.isEmpty()) {
            mFriendsList.addAll(mFriendsListCopy);
        } else {
            for (My_current_friend myCurrentFriend : mFriendsListCopy) {
                if (myCurrentFriend.getName().toLowerCase().contains(text)) {
                    mFriendsList.add(myCurrentFriend);
                }
            }
        }

        notifyDataSetChanged();
    }
}
