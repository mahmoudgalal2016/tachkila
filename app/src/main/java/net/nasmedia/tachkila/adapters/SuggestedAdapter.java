package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.fragments.OtherPlayerCardFragment;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Mahmoud Galal on 9/18/2016.
 */
public class SuggestedAdapter extends BaseAdapter {

    public SuggestedAdapter(Context context) {
        super(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_suggested_full, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        final My_current_friend user = mUsersList.get(position);
        holder.userNameTxt.setText(user.getUsername());
        holder.userScoreTxt.setText(user.getWin() + "");
        loadImage(user.getAvatar(), holder.userImg);
        holder.btShow.setTag(position);
        holder.btShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, mUsersList.get((Integer) view.getTag()).getId());
                intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
                intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                mContext.startActivity(intent);
            }
        });
        holder.itemPlayerContainer.setTag(position);
        holder.itemPlayerContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, mUsersList.get((Integer) view.getTag()).getId());
                intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
                intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                mContext.startActivity(intent);
            }
        });
    }

    private void loadImage(String url, ImageView imageView) {
        if (url == null || url.equals("")) {
            Glide.with(mContext)
                    .load(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
                    .into(imageView);
        } else {
            Glide.with(mContext)
                    .load(Constants.PIC_AVATAR_URL + url)
                    .placeholder(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
                    .into(imageView);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.userImg)
        ImageView userImg;
        @BindView(R.id.userNameTxt)
        TextView userNameTxt;
        @BindView(R.id.userScoreTxt)
        TextView userScoreTxt;
        @BindView(R.id.btshow)
        TextView btShow;
        @BindView(R.id.smContentView)
        RelativeLayout itemPlayerContainer;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
