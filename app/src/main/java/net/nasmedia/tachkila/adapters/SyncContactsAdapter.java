package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.response.searchuser.UserSearch;
import net.nasmedia.tachkila.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Mahmoud Galal on 9/18/2016.
 */
public class SyncContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<User> mUsersList;
    Context mContext;

    public SyncContactsAdapter(Context context, List<User> users) {
        mContext = context;
        mUsersList = users;
    }

    @Override
    public int getItemCount() {
        return mUsersList.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_player, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        User user = mUsersList.get(position);
        holder.userNameTxt.setText(user.getUsername());
        holder.userScoreTxt.setText("Score: " + user.getPoints());
        loadImage(user.getAvatar(), holder.userImg);
    }

    public int getUserIdAtPosition(int position) {
        return mUsersList.get(position).getId();
    }

    private void loadImage(String url, ImageView imageView) {
        if (url == null || url.equals("")) {
            imageView.setImageResource(R.drawable.empty_circl);
        } else {
            Glide.with(mContext)
                    .load(Constants.PIC_AVATAR_URL + url)
                    .placeholder(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
                    .into(imageView);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.userImg)
        ImageView userImg;
        @BindView(R.id.userNameTxt)
        TextView userNameTxt;
        @BindView(R.id.userScoreTxt)
        TextView userScoreTxt;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
