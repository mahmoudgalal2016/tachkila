package net.nasmedia.tachkila.fragments;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import net.nasmedia.tachkila.ContactModel;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.ContactsAdapter;
import net.nasmedia.tachkila.adapters.SyncContactsAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.RecyclerItemClickListener;
import net.nasmedia.tachkila.viewmodels.AddressBookViewModel;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;
import net.nasmedia.tachkila.widgets.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 27/11/2016.
 */

public class AddressBookFragment extends BaseFragment implements OnDataLoaded, NewMatchViewModel.OnError {

    private static final String TAG = AddressBookFragment.class.getSimpleName();

    @BindView(R.id.playersList)
    RecyclerView mPlayersList;
    @BindView(R.id.contactList)
    RecyclerView mContactList;


    AddressBookViewModel mViewModel;

    public static AddressBookFragment getInstance() {
        return new AddressBookFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mViewModel = new AddressBookViewModel(context, this, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address_book, container, false);
        ButterKnife.bind(this, view);


        mContactList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mContactList.addItemDecoration(new DividerItemDecoration(getActivity(), R.dimen.recyclerViewVerticalSpace));
        mContactList.setAdapter(new ContactsAdapter(mViewModel.displayContacts()));

        mContactList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mContactList, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                sendSMS(((ContactsAdapter) mContactList.getAdapter()).getPhoneNumberAtPosition(position));
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        mPlayersList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mPlayersList.addItemDecoration(new DividerItemDecoration(getActivity(), R.dimen.recyclerViewVerticalSpace));

        mPlayersList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mPlayersList, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, ((SyncContactsAdapter) mPlayersList.getAdapter()).getUserIdAtPosition(position));
                intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
                intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        return view;
    }

    public void sendSMS(String phone) {
        Uri uri = Uri.parse("smsto:" + phone);
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", "Please join us to Tachkila application by click on this link : http://www.tachkila.com");
        startActivity(it);
    }

    @Override
    public String getTitle() {
        return "All Contacts";
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        switch (responseStates) {
            case ERROR:

                break;
            case NO_DATA:

                break;
            case NO_NETWORK:

                break;
            case START:

                break;
            case SUCCESS:
                mPlayersList.setAdapter(new SyncContactsAdapter(getActivity(), mViewModel.getUsers()));
                break;
        }
    }

    @Override
    public void showError(String error) {

    }
}
