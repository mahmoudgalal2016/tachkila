package net.nasmedia.tachkila.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by mahmoudgalal on 8/3/16.
 */
public abstract class BaseFragment extends Fragment {

    public abstract String getTitle();
}
