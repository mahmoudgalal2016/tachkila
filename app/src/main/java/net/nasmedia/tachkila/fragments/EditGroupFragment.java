package net.nasmedia.tachkila.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.RealPathUtil;
import net.nasmedia.tachkila.viewmodels.EditGroupViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Sherif on 8/19/2016.
 */
public class EditGroupFragment extends BaseFragment implements OnDataLoaded {

    public final static String ARG_GROUP_ID = "ARG_GROUP_ID";

    String avatarPath = null;
    @BindView(R.id.addGroupPictureInsTxt)
    TextView mAddGroupPictureInsTxt;
    @BindView(R.id.groupPictureImg)
    ImageView mGroupPictureImg;
    @BindView(R.id.editTextGroupName)
    EditText mGroupNameEdt;

    ProgressDialog mProgressDialog;
    EditGroupViewModel mViewModel;

    private int mGroupId;

    public static BaseFragment getInstance(int groupId) {
        EditGroupFragment fragment = new EditGroupFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mGroupId = getArguments().getInt(ARG_GROUP_ID);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mViewModel = new EditGroupViewModel(getActivity(), mGroupId, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_group, container, false);
        ButterKnife.bind(this, view);

        mViewModel.onStart();

        mGroupNameEdt.setText(mViewModel.getGroupName());
        if (mViewModel.getGroupPhoto() != null && !mViewModel.getGroupPhoto().equals("")) {
            mAddGroupPictureInsTxt.setVisibility(View.GONE);
            mGroupPictureImg.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(Constants.PIC_GROUPS_URL + mViewModel.getGroupPhoto()).bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool())).into(mGroupPictureImg);
        }
        return view;
    }

    @Override
    public String getTitle() {
        return "Edit Group";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT < 19) {
                        Intent intent = new Intent();
                        intent.setType("image/jpeg");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(intent, 1);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/jpeg");
                        startActivityForResult(intent, 1);
                    }
                } else {
                    Toast.makeText(getActivity(),"No permission for Gallery", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    public void pickImage() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            if (Build.VERSION.SDK_INT < 19) {
                Intent intent = new Intent();
                intent.setType("image/jpeg");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 1);
            } else {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/jpeg");
                startActivityForResult(intent, 1);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }
// SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                avatarPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getActivity(), data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                avatarPath = RealPathUtil.getRealPathFromURI_API11to18(getActivity(), data.getData());

                // SDK > 19 (Android 4.4)
            else
                avatarPath = RealPathUtil.getRealPathFromURI_API19(getActivity(), data.getData());

            Log.d("Edit group", avatarPath +"");

            mGroupPictureImg.setImageBitmap(null);

            Glide.with(getActivity()).load(avatarPath)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool())).into(mGroupPictureImg);

            mAddGroupPictureInsTxt.setVisibility(View.GONE);
            mGroupPictureImg.setVisibility(View.VISIBLE);
        }
    }


    @OnClick({R.id.addGroupPictureInsTxt, R.id.groupPictureImg})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addGroupPictureInsTxt:
            case R.id.groupPictureImg:
                pickImage();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_group_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                editGroup();
                break;
        }
        return true;

    }

    private void editGroup() {
        if (!TextUtils.isEmpty(mGroupNameEdt.getText().toString())) {
            mViewModel.editGroup(avatarPath, mGroupNameEdt.getText().toString());
        } else {
            Toast.makeText(getActivity(), "Please enter group name", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:
                Toast.makeText(getActivity(), "Error happened", Toast.LENGTH_LONG).show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                Toast.makeText(getActivity(), "No Internet connection", Toast.LENGTH_LONG).show();
                break;
            case START:
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setMessage("Please wait ...");
                mProgressDialog.setTitle(null);
                mProgressDialog.show();
                break;
            case SUCCESS:
                Toast.makeText(getActivity(), "Group edited", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onDestroyView() {
        mViewModel.onStart();
        super.onDestroyView();
    }


}
