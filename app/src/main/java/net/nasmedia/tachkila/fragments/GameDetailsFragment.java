package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.ActiveGamePlayersAdapter;
import net.nasmedia.tachkila.adapters.SelectFriendsAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.GamePlayersWithCaptain;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.viewmodels.GameDetailsViewModel;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;
import net.nasmedia.tachkila.widgets.CustomTextView;
import net.nasmedia.tachkila.widgets.MatchFieldLayout;

import java.text.ParseException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by mahmoudgalal on 11/2/16.
 */

public class GameDetailsFragment extends BaseFragment implements OnDataLoaded, MatchFieldLayout.OnFieldAction, NewMatchViewModel.OnError {

    public static final String ARG_GAME_ID = "ARG_GAME_ID";
    private static final String TAG = GameDetailsFragment.class.getSimpleName();

    GameDetailsViewModel mViewModel;
    int mGameId;

    @BindView(R.id.gamePlayersNumTxt)
    CustomTextView gamePlayersNumTxt;
    @BindView(R.id.groupNameTxt)
    CustomTextView groupNameTxt;
    @BindView(R.id.captainNameTxt)
    CustomTextView captainNameTxt;
    @BindView(R.id.gameDateTxt)
    CustomTextView gameDateTxt;
    @BindView(R.id.placeTxt)
    CustomTextView placeTxt;
    @BindView(R.id.gameTimeTxt)
    CustomTextView gameTimeTxt;
    @BindView(R.id.itemColorA)
    LinearLayout itemColorA;
    @BindView(R.id.teamACheck)
    CheckedTextView teamACheck;
    @BindView(R.id.itemColorB)
    LinearLayout itemColorB;
    @BindView(R.id.teamBCheck)
    CheckedTextView teamBCheck;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.loadingProgress)
    ProgressBar loadingProgress;
    @BindView(R.id.noDataTxt)
    CustomTextView noDataTxt;
    @BindView(R.id.matchField)
    FrameLayout matchField;
    @BindView(R.id.notesEdt)
    EditText notesEdt;
    @BindView(R.id.groupPlayersList)
    RecyclerView groupPlayersList;
    @BindView(R.id.groupPlayersContainer)
    LinearLayout groupPlayersContainer;
    @BindView(R.id.deleteMatchBtn)
    Button deleteMatchBtn;

    MatchFieldLayout mMatchFieldLayout;

    @BindView(R.id.playerImg)
    ImageView myPlayerImg;
    @BindView(R.id.playerName)
    CustomTextView myPlayerName;
    @BindView(R.id.myPlayerContainer)
    LinearLayout myPlayerContainer;
    @BindView(R.id.playersContainer)
    LinearLayout playersContainer;

    private MaterialDialog mProgressDialog;

    public static GameDetailsFragment getInstance(int gameId) {
        GameDetailsFragment gameDetailsFragment = new GameDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GAME_ID, gameId);
        gameDetailsFragment.setArguments(bundle);
        return gameDetailsFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mGameId = getArguments().getInt(ARG_GAME_ID);
        mViewModel = new GameDetailsViewModel(getActivity(), mGameId, this, deleteGameListener, saveChangesListener, this);
        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content("Please wait ...")
                .progress(true, 0)
                .build();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game_details, container, false);
        ButterKnife.bind(this, view);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        switch (responseStates) {
            case ERROR:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_happened))
                        .positiveText("OK").show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_No_Ineternet_connection))
                        .positiveText("OK").show();
                break;
            case START:
                loadingProgress.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.GONE);
                break;
            case SUCCESS:
                mMatchFieldLayout = new MatchFieldLayout(getActivity(), mViewModel.getGamePlayers(), mViewModel.getTeamAColor(), mViewModel.getTeamBColor(), this);
                matchField.addView(mMatchFieldLayout);

                groupNameTxt.setText(mViewModel.getGroupName());
                gamePlayersNumTxt.setText(mViewModel.getGameType());
                try {
                    gameDateTxt.setText(mViewModel.getDate());
                    gameTimeTxt.setText(mViewModel.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                placeTxt.setText(mViewModel.getLocation());
                captainNameTxt.setText(mViewModel.getCaptineName());

                GradientDrawable teamAbackground = (GradientDrawable) itemColorA.getBackground();
                teamAbackground.setColor(mViewModel.getTeamAColor());

                GradientDrawable teamBbackground = (GradientDrawable) itemColorB.getBackground();
                teamBbackground.setColor(mViewModel.getTeamBColor());


                if (!mViewModel.isMyPlayerInField()) {
                    myPlayerName.setText(mViewModel.getMyPlayer().getUserName());
                    if (mViewModel.getMyPlayer().getAvatar() != null && !TextUtils.isEmpty(mViewModel.getMyPlayer().getAvatar())) {
                        Glide.with(getActivity())
                                .load(Constants.PIC_AVATAR_URL + mViewModel.getMyPlayer().getAvatar())
                                .placeholder(R.drawable.empty_circl)
                                .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool()))
                                .into(myPlayerImg);
                    } else {
                        myPlayerImg.setImageResource(R.drawable.empty_circl);
                    }
                    myPlayerContainer.setVisibility(View.VISIBLE);
                } else {
                    myPlayerContainer.setVisibility(View.GONE);
                }

                myPlayerContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mMatchFieldLayout.setNewPlayer(mViewModel.getMyPlayer());
                        myPlayerContainer.setVisibility(View.GONE);
                    }
                });

                notesEdt.setText(mViewModel.getActiveGame().getNotes());

                if (mViewModel.getActiveGame().getCaptainId() == Utils.getSavedUserIdInSharedPref(getActivity())) {
                    notesEdt.setEnabled(true);
                } else {
                    notesEdt.setEnabled(false);
                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                linearLayoutManager.setAutoMeasureEnabled(true);
                groupPlayersList.setLayoutManager(linearLayoutManager);
                ActiveGamePlayersAdapter adapter = new ActiveGamePlayersAdapter(getActivity(), mViewModel.getPlayers());
                groupPlayersList.setAdapter(adapter);
                if (mViewModel.myFriendsList().size() > 0) {
                    playersContainer.setVisibility(View.VISIBLE);
                } else {
                    playersContainer.setVisibility(View.GONE);
                }
                loadingProgress.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void removePlayerFromField(GamePlayersWithCaptain player) {
        myPlayerName.setText(mViewModel.getMyPlayer().getUserName());
        if (mViewModel.getMyPlayer().getAvatar() != null && !TextUtils.isEmpty(mViewModel.getMyPlayer().getAvatar())) {
            Glide.with(getActivity())
                    .load(Constants.PIC_AVATAR_URL + mViewModel.getMyPlayer().getAvatar())
                    .placeholder(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool()))
                    .into(myPlayerImg);
        } else {
            myPlayerImg.setImageResource(R.drawable.empty_circl);
        }
        myPlayerContainer.setVisibility(View.VISIBLE);
        teamACheck.setChecked(false);
        teamBCheck.setChecked(false);
    }

    @Override
    public void chosenTeam(int team) {
        if (team == 1) {
            teamACheck.setChecked(true);
            teamBCheck.setChecked(false);
        } else {
            teamACheck.setChecked(false);
            teamBCheck.setChecked(true);
        }
    }

    OnDataLoaded deleteGameListener = new OnDataLoaded() {
        @Override
        public void downloadComplete(ResponseStates responseStates) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            switch (responseStates) {
                case ERROR:
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content(getString(R.string.error_happened))
                            .positiveText("OK").show();
                    break;
                case NO_DATA:

                    break;
                case NO_NETWORK:
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content(getString(R.string.error_No_Ineternet_connection))
                            .positiveText("OK").show();
                    break;
                case START:
                    if (mProgressDialog != null) {
                        mProgressDialog.show();
                    }
                    break;
                case SUCCESS:
                    String content;
                    if (mViewModel.getActiveGame().getCaptain().getId() == Utils.getSavedUserIdInSharedPref(getActivity())) {
                        content = "Game deleted successfully";
                    } else {
                        content = "Game delete request sent successfully";
                    }
                    new MaterialDialog.Builder(getActivity())
                            .title("Game Delete")
                            .content(content)
                            .positiveText("OK")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (mViewModel.getActiveGame().getCaptain().getId() == Utils.getSavedUserIdInSharedPref(getActivity())) {
                                        GameDetailsFragment.this.getActivity().finish();
                                    }
                                }
                            })
                            .show();
                    break;
            }
        }
    };

    OnDataLoaded saveChangesListener = new OnDataLoaded() {
        @Override
        public void downloadComplete(ResponseStates responseStates) {

            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

            switch (responseStates) {
                case ERROR:

                    break;
                case NO_DATA:

                    break;
                case NO_NETWORK:
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content(getString(R.string.error_No_Ineternet_connection))
                            .positiveText("OK").show();
                    break;
                case START:
                    if (mProgressDialog != null) {
                        mProgressDialog.show();
                    }
                    break;
                case SUCCESS:
                    new MaterialDialog.Builder(getActivity())
                            .title("Save Changes")
                            .content("changes you made are successfully saved")
                            .positiveText("OK")
                            .show();
                    break;
            }
        }
    };


    @Override
    public void showError(String error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        loadingProgress.setVisibility(View.GONE);
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("OK").show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.game_details_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                mViewModel.saveChanges(mMatchFieldLayout.getPositionX(), mMatchFieldLayout.getPositionY(), teamACheck.isChecked() ? 1 : 2);
                break;
        }
        return true;

    }


    @OnClick({R.id.placeTxt, R.id.deleteMatchBtn, R.id.inviteToGAme})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.placeTxt:

                String strUri = "http://maps.google.com/maps?q=loc:" + mViewModel.getActiveGame().getMapLat() + "," + mViewModel.getActiveGame().getMapLng() + " (" + mViewModel.getGroupName() + ")";
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));

                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");

                startActivity(intent);

//                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", mViewModel.getActiveGame().getMapLat(), mViewModel.getActiveGame().getMapLng());
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//                getActivity().startActivity(intent);
                break;
            case R.id.deleteMatchBtn:
                mViewModel.deleteGame();
                break;
            case R.id.inviteToGAme:
                Intent intent2 = new Intent(getActivity(), DetailsActivity.class);
                intent2.putExtra(InvitePlayersGameFragment.ARG_GAME_ID, mGameId);
                intent2.setAction(Constants.ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT);
                startActivity(intent2);
                break;
        }
    }


}
