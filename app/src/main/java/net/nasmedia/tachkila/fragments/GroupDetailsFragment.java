package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.GroupDetailsPlayersAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.groups.ViewGroupResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.CustomFontUtils;
import net.nasmedia.tachkila.utils.RecyclerItemClickListener;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.viewmodels.GroupDetailsViewModel;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 8/30/2016.
 */
public class GroupDetailsFragment extends BaseFragment implements OnDataLoaded, NewMatchViewModel.OnError {


    public static final String ARG_GROUP_ID = "ARG_GROUP_ID";
    private static final String TAG = GroupDetailsFragment.class.getSimpleName();

    @BindView(R.id.newMatchBtn)
    ImageButton mNewMatchBtn;
    @BindView(R.id.matchHistoryBtn)
    Button mMatchHistoryBtn;
    @BindView(R.id.leaveTheGroup)
    Button mLeaveTheGroup;
    @BindView(R.id.addNewPlayer)
    Button mAddNewPlayer;
    @BindView(R.id.playersContainer)
    RecyclerView mPlayersContainer;
    @BindView(R.id.container)
    ScrollView mContainer;
    @BindView(R.id.editGroup)
    Button mEditGroupPlayer;

    int mGroupId;
    GroupDetailsViewModel mViewModel;
    GroupDetailsPlayersAdapter mAdapter;

    MaterialDialog.Builder mDialog;


    public static GroupDetailsFragment getInstance(int groupId) {
        GroupDetailsFragment fragment = new GroupDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mGroupId = getArguments().getInt(ARG_GROUP_ID);
        mViewModel = new GroupDetailsViewModel(context, mGroupId, this, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_details, container, false);
        ButterKnife.bind(this, view);

        CustomFontUtils.applyButtonCustomFont(mMatchHistoryBtn, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyButtonCustomFont(mLeaveTheGroup, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyButtonCustomFont(mAddNewPlayer, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyButtonCustomFont(mEditGroupPlayer, CustomFontUtils.LIGHT, getActivity());

        LinearLayoutManager lLayout = new LinearLayoutManager(this.getActivity());
        mPlayersContainer.setHasFixedSize(true);
        mPlayersContainer.setLayoutManager(lLayout);
//        mPlayersContainer.addItemDecoration(new ItemDecorationAlbumColumns(getActivity(), R.dimen.recyclerViewVerticalSpace));

        mAdapter = new GroupDetailsPlayersAdapter(getActivity());
        mPlayersContainer.setAdapter(mAdapter);

        mPlayersContainer.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mPlayersContainer, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, ((GroupDetailsPlayersAdapter) mPlayersContainer.getAdapter()).getUserIdAtPosition(position));
                intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
                intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                getActivity().startActivity(intent);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }

    @OnClick({R.id.newMatchBtn, R.id.matchHistoryBtn, R.id.leaveTheGroup, R.id.addNewPlayer, R.id.editGroup})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newMatchBtn:
                createNewMatch();
                break;
            case R.id.matchHistoryBtn:
                showMatchHistory();
                break;
            case R.id.leaveTheGroup:
                leaveGroup();
                break;
            case R.id.addNewPlayer:
                addNewPlayer();
                break;
            case R.id.editGroup:
                editGroup();
                break;
        }
    }

    private void addNewPlayer() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(AddPlayersFragment.ARG_GROUP_ID, mGroupId);
        intent.setAction(Constants.ACTION_SHOW_ADD_PLAYERS_FRAGMENT);
        startActivity(intent);
    }

    private void leaveGroup() {
        mDialog = new MaterialDialog.Builder(getActivity());
        mDialog.content("Are you sure you want to leave the group ?");
        mDialog.positiveText("Yes");
        mDialog.negativeText("NO");
        mDialog.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                if (ConectivityUtils.isDeviceConnectedToNetwork(getActivity())) {
                    Log.d("group id", mGroupId + "");
                    ApiHandler.getInstance().getServices().leaveGroup(mGroupId, Utils.getSavedUserIdInSharedPref(getActivity())).enqueue(new Callback<ViewGroupResponse>() {
                        @Override
                        public void onResponse(Call<ViewGroupResponse> call, Response<ViewGroupResponse> response) {
                            if (response.isSuccessful()) {
                                if (response.body().getResult().getSuccess() == 1) {
                                    mViewModel.removeGroup(mGroupId);
                                    getActivity().getSupportFragmentManager().popBackStack();
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.error_happened), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.error_happened), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ViewGroupResponse> call, Throwable t) {

                        }
                    });
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_No_Ineternet_connection), Toast.LENGTH_LONG).show();
                }
            }
        });
        mDialog.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

            }
        });
        mDialog.show();

    }

    private void showMatchHistory() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(MatchHistoryFragment.ARG_GROUP_ID, mGroupId);
        intent.setAction(Constants.ACTION_SHOW_MATCH_HISTORY_FRAGMENT);
        startActivity(intent);
    }

    private void createNewMatch() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra("groupId", mGroupId);
        intent.setAction(Constants.ACTION_SHOW_ADD_NEW_MATCH_FRAGMENT);
        startActivity(intent);
    }

    private void editGroup() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(EditGroupFragment.ARG_GROUP_ID, mGroupId);
        intent.setAction(Constants.ACTION_SHOW_EDIT_GROUP_FRAGMENT);
        startActivity(intent);
    }


    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (isAdded()) {
            switch (responseStates) {
                case ERROR:
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content(getString(R.string.error_happened))
                            .positiveText("OK").show();
                    break;
                case NO_DATA:

                    break;
                case NO_NETWORK:
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content(getString(R.string.error_No_Ineternet_connection))
                            .positiveText("OK").show();
                    break;
                case START:

                    break;
                case SUCCESS:
                    if (mViewModel.getGroup().getUsers() != null && mViewModel.getGroup().getUsers().size() > 0) {
                        Log.d(TAG, mViewModel.getGroup().getUsers().size() + "");
                        mAdapter.setFriendsList(mViewModel.getGroup().getUsers(), mViewModel.getGroup().getAdmin_id());
                        mPlayersContainer.setVisibility(View.VISIBLE);
                    } else {
                        mPlayersContainer.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("OK").show();
    }
}
