package net.nasmedia.tachkila.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.GroupAdapter;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.RecyclerItemClickListener;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.CustomTextView;
import net.nasmedia.tachkila.widgets.ItemDecorationAlbumColumns;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends BaseFragment {

    private static final String TAG = GroupFragment.class.getSimpleName();
    Realm realm;
    @BindView(R.id.gridViewGroups)
    RecyclerView gridViewGroups;
    ArrayList<Group> mGroupItems;
    @BindView(R.id.noDataTxt)
    CustomTextView noDataTxt;
    private GridLayoutManager lLayout;

    public static GroupFragment getInstance() {
        return new GroupFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.groups_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_ADD_NEW_Group_FRAGMENT);
                startActivity(intent);
                break;
        }
        return true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group, container, false);
        ButterKnife.bind(this, view);

        lLayout = new GridLayoutManager(this.getActivity(), 3);
        gridViewGroups.setHasFixedSize(true);
        gridViewGroups.setLayoutManager(lLayout);
        gridViewGroups.addItemDecoration(new ItemDecorationAlbumColumns(getActivity(), R.dimen.groupRecyclerViewVerticalSpace));


        gridViewGroups.addOnItemTouchListener(
                new RecyclerItemClickListener(GroupFragment.this.getActivity(), gridViewGroups, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever to doo
                        Intent intent = new Intent(getActivity(), DetailsActivity.class);
                        intent.putExtra(GroupDetailsFragment.ARG_GROUP_ID, mGroupItems.get(position).getId());
                        intent.putExtra("name", mGroupItems.get(position).getName());
                        intent.setAction(Constants.ACTION_SHOW_GROUP_DETAILS_FRAGMENT);
                        startActivity(intent);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(getActivity())).findFirst();



        mGroupItems = new ArrayList<>();

        for (Group item : user.getGroups()) {
            mGroupItems.add(item);
        }

        if (mGroupItems.size() > 0) {

            GroupAdapter mAdapter = new GroupAdapter(mGroupItems, this);

            gridViewGroups.setAdapter(mAdapter);
            noDataTxt.setVisibility(View.GONE);
            gridViewGroups.setVisibility(View.VISIBLE);
        }else{
            noDataTxt.setVisibility(View.VISIBLE);
            gridViewGroups.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        realm.close();

        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public String getTitle() {
        return "Groups";
    }
}
