package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.MatchActiveAdapter;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.RecyclerItemClickListener;
import net.nasmedia.tachkila.viewmodels.HomeViewModel;
import net.nasmedia.tachkila.widgets.DividerItemDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mahmoudgalal on 8/3/16.
 */
public class HomeFragment extends BaseFragment {


    @BindView(R.id.matchList)
    RecyclerView mRecyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.noDataTxt)
    TextView mNoDataTxt;
    @BindView(R.id.newMatchBtn)
    ImageButton mNewMatchBtn;

    HomeViewModel mViewModel;
    MatchActiveAdapter mAdapter;


    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mViewModel = new HomeViewModel(getActivity());
        mAdapter = new MatchActiveAdapter(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), R.dimen.groupRecyclerViewVerticalSpace);
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx,int dy){
                super.onScrolled(recyclerView, dx, dy);

                if (dy >0) {
                    // Scroll Down
                    if (fab.isShown()) {
                        fab.hide();
                    }
                }
                else if (dy <0) {
                    // Scroll Up
                    if (!fab.isShown()) {
                        fab.show();
                    }
                }
            }
        });

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d("HomeFragment", position + " " + mViewModel.getActiveGames().get(position).toString());
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(GameDetailsFragment.ARG_GAME_ID, mAdapter.getGameIdAtPosition(position));
                intent.putExtra("name", mViewModel.getActiveGames().get(position).getGroup().getName());
                intent.setAction(Constants.ACTION_SHOW_GAME_DETAILS_FRAGMENT);
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_NOTIFICATION_FRAGMENT);
                startActivity(intent);
            }
        });
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();
        if (mViewModel.getActiveGames().size() > 0) {
            mAdapter.setNotificationList(mViewModel.getActiveGames());
            mNoDataTxt.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mNoDataTxt.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }

    @Override
    public String getTitle() {
        return "Home";
    }

    @OnClick(R.id.newMatchBtn)
    public void onClick() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.setAction(Constants.ACTION_SHOW_ADD_NEW_MATCH_FRAGMENT);
        startActivity(intent);
    }
}
