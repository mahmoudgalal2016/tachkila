package net.nasmedia.tachkila.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.CustomFontUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mahmoud Galal on 11/11/2016.
 */

public class InvitePlayersFragment extends BaseFragment {


    @BindView(R.id.addPlayerByUserName)
    Button mAddPlayerByUserName;
    @BindView(R.id.addPlayerByAddressBook)
    Button mAddPlayerByAddressBook;

    public static InvitePlayersFragment getInstance() {
        return new InvitePlayersFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invite_players, container, false);
        ButterKnife.bind(this, view);

        CustomFontUtils.applyButtonCustomFont(mAddPlayerByAddressBook, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyButtonCustomFont(mAddPlayerByUserName, CustomFontUtils.LIGHT, getActivity());

        return view;
    }

    @Override
    public String getTitle() {
        return "Add Players";
    }

    @OnClick({R.id.addPlayerByUserName, R.id.addPlayerByAddressBook})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addPlayerByUserName:
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT);
                startActivity(intent);
                break;
            case R.id.addPlayerByAddressBook:
                askForContactPermission();
                break;
        }

    }


    public void askForContactPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }else{
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_ADDRESS_BOOK_FRAGMENT);
                startActivity(intent);
            }
        }
        else{
            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            intent.setAction(Constants.ACTION_SHOW_ADDRESS_BOOK_FRAGMENT);
            startActivity(intent);
        }
    }

    static final int PERMISSION_REQUEST_CONTACT = 1;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(getActivity(), DetailsActivity.class);
                    intent.setAction(Constants.ACTION_SHOW_ADDRESS_BOOK_FRAGMENT);
                    startActivity(intent);
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(getActivity(),"No permission for contacts", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
