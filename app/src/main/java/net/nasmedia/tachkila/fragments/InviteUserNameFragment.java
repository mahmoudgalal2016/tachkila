package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.AlertDialogWrapper;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.activities.MainActivity;
import net.nasmedia.tachkila.adapters.UserSearchAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.RecyclerItemClickListener;
import net.nasmedia.tachkila.viewmodels.SearchUserViewModel;
import net.nasmedia.tachkila.widgets.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 11/11/2016.
 */

public class InviteUserNameFragment extends BaseFragment implements OnDataLoaded {

    @BindView(R.id.usersList)
    RecyclerView usersList;
    @BindView(R.id.noDataTxt)
    CustomTextView noDataTxt;
    @BindView(R.id.loading)
    ProgressBar loading;

    SearchUserViewModel mViewModel;

    public static InviteUserNameFragment getInstance() {
        return new InviteUserNameFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mViewModel = new SearchUserViewModel(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invite_username, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, view);

        usersList.setLayoutManager(new LinearLayoutManager(getActivity()));
        usersList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), usersList, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, mViewModel.getUserResultList().get(position).getId());
                intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
                intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        return view;
    }

    @Override
    public String getTitle() {
        return "Add by Username";
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        switch (responseStates) {
            case ERROR:

                break;
            case NO_DATA:
                loading.setVisibility(View.GONE);
                usersList.setVisibility(View.GONE);
                noDataTxt.setVisibility(View.VISIBLE);
                break;
            case NO_NETWORK:
                showErrorDialog(getString(R.string.error_No_Ineternet_connection));
                break;
            case START:
                loading.setVisibility(View.VISIBLE);
                usersList.setVisibility(View.GONE);
                noDataTxt.setVisibility(View.GONE);
                break;
            case SUCCESS:
                UserSearchAdapter adapter = new UserSearchAdapter(getActivity());
                adapter.setUsers(mViewModel.getUserResultList());
                usersList.setAdapter(adapter);

                loading.setVisibility(View.GONE);
                usersList.setVisibility(View.VISIBLE);
                noDataTxt.setVisibility(View.GONE);
                break;
        }
    }

    private void showErrorDialog(String message) {
        new AlertDialogWrapper.Builder(getActivity())
                .setTitle(R.string.Error)
                .setMessage(message)
                .setNegativeButton(R.string.OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView sv = new SearchView(((DetailsActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                System.out.println("search query submit");

                mViewModel.startSearch(query);

//                mVideosRecyclerView.setVisibility(View.GONE);
//                noVideosTxt.setVisibility(View.GONE);
//                mProgressBar.setVisibility(View.VISIBLE);
//                searchPage = 1;
//                keyword = query;
//                inSearch = true;
////                mModel.setNewsList((ArrayList<News>) mAdapter.getAllItems());
//                mModel.startSearch(currentCategory, query, searchPage);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                if (isVisible()) {
//                    if (mModel.getNewsList() != null) {
//                        if (TextUtils.isEmpty(newText)) {
//                            inSearch = false;
//                            mAdapter.clear();
//                            if (mModel.getNewsList().size() > 0) {
//                                mAdapter.addAll(mModel.getNewsList());
//                                mAdapter.notifyDataSetChanged();
//                                mVideosRecyclerView.setAdapter(mAdapter);
//                                noVideosTxt.setVisibility(View.GONE);
//                                mVideosRecyclerView.setVisibility(View.VISIBLE);
//                            } else {
//                                noVideosTxt.setVisibility(View.VISIBLE);
//                                mVideosRecyclerView.setVisibility(View.GONE);
//                            }
//                        }
//                    }
//                }
                return false;
            }
        });
//        Utils.changeSearchViewTextColor(sv, getActivity());
    }

}
