package net.nasmedia.tachkila.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.WindowCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.onesignal.OneSignal;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.MainActivity;
import net.nasmedia.tachkila.activities.WelcomeActivity;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.CustomFontUtils;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.viewmodels.LoginViewModel;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mahmoudgalal on 5/22/16.
 */
public class LoginFragment extends Fragment implements OnDataLoaded, GoogleApiClient.OnConnectionFailedListener, NewMatchViewModel.OnError {

//    https://github.com/jaychang0917/SocialLoginManager

    private static final String TAG = LoginFragment.class.getSimpleName();
    private static final int RC_SIGN_IN = 9001;

    @BindView(R.id.userNameEdt)
    EditText mUserNameEdt;
    @BindView(R.id.passwordEdt)
    EditText mPasswordEdt;
    @BindView(R.id.loginBtn)
    Button mLoginBtn;
    @BindView(R.id.facebookBtn)
    LoginButton mFacebookBtn;
    @BindView(R.id.registerBtn)
    Button mRegisterBtn;
    @BindView(R.id.fbBtn)
    Button fbBtn;

    private CallbackManager mCallbackManager;
    private GoogleApiClient mGoogleApiClient;
    private LoginViewModel mModel;
    private MaterialDialog.Builder mMaterialDialogBuilder;
    private MaterialDialog mProgressDialog;

    private String mRegId;


    public static LoginFragment getInstance() {
        return new LoginFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mModel = new LoginViewModel(context, facebookListener, googleLoginListener, this, mResetPassListener, this);
        mMaterialDialogBuilder = new MaterialDialog.Builder(context);
        mMaterialDialogBuilder.content(R.string.Please_wait);
        mMaterialDialogBuilder.progress(true, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        CustomFontUtils.applyButtonCustomFont(mLoginBtn, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyButtonCustomFont(mRegisterBtn, CustomFontUtils.MEDIUM, getActivity());
        CustomFontUtils.applyEditTextCustomFont(mUserNameEdt, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyEditTextCustomFont(mPasswordEdt, CustomFontUtils.LIGHT, getActivity());

        FacebookSdk.sdkInitialize(getActivity());

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                mRegId = userId;
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mCallbackManager = CallbackManager.Factory.create();

        mFacebookBtn.setReadPermissions(Arrays.asList("email", "public_profile"));
        // If using in a fragment
        mFacebookBtn.setFragment(this);
        // Other app specific specialization

        // Callback registration
        mFacebookBtn.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d(TAG, loginResult.getAccessToken().getUserId() + "");
                mModel.loginUsingSocial("facebook", loginResult.getAccessToken().getUserId(), mRegId);
            }


            @Override
            public void onCancel() {
                // App code
                Log.d(TAG, "onCancel");

            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, exception.getMessage() + "");

            }
        });


        return view;
    }

    @Override
    public void onStop() {
        mModel.onStop();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        mModel.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:
                Log.d(TAG, "login error");
                showErrorDialog(getString(R.string.error_Username_and_password_doesnt_match));
                break;
            case NO_DATA:
                Log.d(TAG, "login no data");
                showErrorDialog(getString(R.string.error_Username_and_password_doesnt_match));
                break;
            case NO_NETWORK:
                Log.d(TAG, "login no network");
                showErrorDialog(getString(R.string.error_No_Ineternet_connection));
                break;
            case START:
                Log.d(TAG, "login start");
                if (mProgressDialog == null) {
                    mProgressDialog = mMaterialDialogBuilder.show();
                } else if (!mProgressDialog.isShowing()) {
                    mProgressDialog = mMaterialDialogBuilder.show();
                }
                break;
            case SUCCESS:
                Log.d(TAG, "login success");

                PreferenceHelper.getInstance(getActivity()).setIsUserLogged(true);
                startActivity(new Intent(getActivity(), WelcomeActivity.class));
                getActivity().finish();
                break;
        }
    }

    OnDataLoaded facebookListener = new OnDataLoaded() {
        @Override
        public void downloadComplete(ResponseStates responseStates) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            switch (responseStates) {
                case ERROR:
                    Intent intent = new Intent(Constants.ACTION_SHOW_REGISTER_ACTIVITY);
                    intent.putExtra(RegisterFragment.ARG_REGISTER_TYPE, RegisterFragment.RegisterType.FACEBOOK.ordinal());
                    getActivity().sendBroadcast(intent);
                    break;
                case NO_DATA:
                    break;
                case NO_NETWORK:
                    showErrorDialog(getString(R.string.error_No_Ineternet_connection));
                    break;
                case START:

                    break;
                case SUCCESS:
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                    break;
            }
        }
    };

    OnDataLoaded googleLoginListener = new OnDataLoaded() {
        @Override
        public void downloadComplete(ResponseStates responseStates) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            switch (responseStates) {
                case ERROR:
                    Intent intent = new Intent(Constants.ACTION_SHOW_REGISTER_ACTIVITY);
                    intent.putExtra(RegisterFragment.ARG_REGISTER_TYPE, RegisterFragment.RegisterType.GOOGLE.ordinal());
                    intent.putExtra(RegisterFragment.ARG_GOOGLE_ACCOUNT, googleSignInAccount);
                    getActivity().sendBroadcast(intent);
                    break;
                case NO_DATA:
                    break;
                case NO_NETWORK:
                    showErrorDialog(getString(R.string.error_No_Ineternet_connection));
                    break;
                case START:

                    break;
                case SUCCESS:
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                    break;
            }
        }
    };

    @OnClick({R.id.loginBtn, R.id.registerBtn, R.id.sign_in_button, R.id.forgetPassBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginBtn:
                if (!TextUtils.isEmpty(mUserNameEdt.getText().toString().trim()) && !TextUtils.isEmpty(mPasswordEdt.getText().toString().trim())) {
                    mModel.loginUsingUsername(mUserNameEdt.getText().toString(), mPasswordEdt.getText().toString(), mRegId);
                } else {
                    showErrorDialog(getString(R.string.error_please_complete_require_data));
                }
                break;
            case R.id.registerBtn:
                Intent intent = new Intent(Constants.ACTION_SHOW_REGISTER_ACTIVITY);
                intent.putExtra(RegisterFragment.ARG_REGISTER_TYPE, RegisterFragment.RegisterType.NORMAL.ordinal());
                getActivity().sendBroadcast(intent);
                break;
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.forgetPassBtn:
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_forget_pass);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                window.setAttributes(lp);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#80000000")));
                final EditText emailEdt = (EditText) dialog.findViewById(R.id.emailEdt);
                Button resetPassBtn = (Button) dialog.findViewById(R.id.resetPassBtn);
                ImageButton closeBtn = (ImageButton) dialog.findViewById(R.id.closeBtn);

                closeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                resetPassBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                            mModel.resetPass(emailEdt.getText().toString());
                        } else {
                            showErrorDialog("Please enter your email address");
                        }

                    }
                });

                dialog.show();
                break;
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void showErrorDialog(String message) {
        new AlertDialogWrapper.Builder(getActivity())
                .setTitle(R.string.Error)
                .setMessage(message)
                .setNegativeButton(R.string.OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private GoogleSignInAccount googleSignInAccount;

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            googleSignInAccount = result.getSignInAccount();
            mModel.loginUsingSocial("google", googleSignInAccount.getId(), mRegId);
            // use API

        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }

    @OnClick(R.id.fbBtn)
    public void onClick() {
        mFacebookBtn.performClick();
    }

    @Override
    public void showError(String error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        showErrorDialog(error);
    }

    private OnDataLoaded mResetPassListener = new OnDataLoaded() {
        @Override
        public void downloadComplete(ResponseStates responseStates) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            switch (responseStates) {
                case ERROR:
                    showErrorDialog(getString(R.string.error_happened));
                    break;
                case NO_DATA:
                    break;
                case NO_NETWORK:
                    showErrorDialog(getString(R.string.error_No_Ineternet_connection));
                    break;
                case START:
                    if (mProgressDialog == null) {
                        mProgressDialog = mMaterialDialogBuilder.show();
                    } else if (!mProgressDialog.isShowing()) {
                        mProgressDialog = mMaterialDialogBuilder.show();
                    }
                    break;
                case SUCCESS:
                    new AlertDialogWrapper.Builder(getActivity())
                            .setTitle("Reset password")
                            .setMessage("Please check your email to complete your password resetting")
                            .setNegativeButton(R.string.OK, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                    break;
            }
        }
    };
}
