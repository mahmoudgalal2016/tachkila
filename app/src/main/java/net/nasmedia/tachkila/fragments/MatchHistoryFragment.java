package net.nasmedia.tachkila.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.MatchHistoryAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.RecyclerItemClickListener;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.viewmodels.MatchHistoryViewModel;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;
import net.nasmedia.tachkila.widgets.DividerItemDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 9/19/2016.
 */
public class MatchHistoryFragment extends BaseFragment implements OnDataLoaded, NewMatchViewModel.OnError {


    @BindView(R.id.matchList)
    RecyclerView mRecyclerView;
    @BindView(R.id.noDataTxt)
    TextView mNoDataTxt;

    MatchHistoryViewModel mViewModel;
    MatchHistoryAdapter mAdapter;

    int mGroupId;

    private MaterialDialog mProgressDialog;


    public static final String ARG_GROUP_ID = "ARG_GROUP_ID";

    public static MatchHistoryFragment getInstance(int groupId) {
        MatchHistoryFragment fragment = new MatchHistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mGroupId = getArguments().getInt(ARG_GROUP_ID, 0);
        mViewModel = new MatchHistoryViewModel(getActivity(), mGroupId, this, this);
        mAdapter = new MatchHistoryAdapter(getActivity());

        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content("Please wait ...")
                .progress(true, 0)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_match_history, container, false);
        ButterKnife.bind(this, view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), R.dimen.matchHistoryRecyclerViewVerticalSpace);
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                if (mViewModel.getMatchHistory().get(position).getCaptain_id() == Utils.getSavedUserIdInSharedPref(getActivity()) && mViewModel.getMatchHistory().get(position).getScored() == 0) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_score);

                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = dialog.getWindow();
                    lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                    final MaterialEditText teamAScoreEdt = (MaterialEditText) dialog.findViewById(R.id.teamAScore);
                    teamAScoreEdt.setTextColor(((MatchHistoryAdapter) mRecyclerView.getAdapter()).getTeamAColorAtPosition(position));
                    teamAScoreEdt.setPrimaryColor(((MatchHistoryAdapter) mRecyclerView.getAdapter()).getTeamAColorAtPosition(position));

                    final MaterialEditText teamBScoreEdt = (MaterialEditText) dialog.findViewById(R.id.teamBScore);
                    teamBScoreEdt.setTextColor(((MatchHistoryAdapter) mRecyclerView.getAdapter()).getTeamBColorAtPosition(position));
                    teamBScoreEdt.setPrimaryColor(((MatchHistoryAdapter) mRecyclerView.getAdapter()).getTeamBColorAtPosition(position));

                    Button setScore = (Button) dialog.findViewById(R.id.setScoresBtn);
                    setScore.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {

                                                        if (!TextUtils.isEmpty(teamAScoreEdt.getText().toString().trim()) && !TextUtils.isEmpty(teamBScoreEdt.getText().toString().trim())) {
                                                            int teamAScore = Integer.parseInt(teamAScoreEdt.getText().toString());
                                                            int teamBScore = Integer.parseInt(teamBScoreEdt.getText().toString());

                                                            mViewModel.updateGameScore(((MatchHistoryAdapter) mRecyclerView.getAdapter()).getGameIdAtPosition(position), teamAScore, teamBScore);
                                                            dialog.dismiss();

                                                        }
                                                    }
                                                }
                    );
                    dialog.show();
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();
        if (mViewModel.getMatchHistory().size() > 0) {
            mAdapter.setNotificationList(mViewModel.getMatchHistory());
            mNoDataTxt.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mNoDataTxt.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }

    @Override
    public String getTitle() {
        return "Match History";
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_happened))
                        .positiveText("OK").show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_No_Ineternet_connection))
                        .positiveText("OK").show();
                break;
            case START:
                if (mProgressDialog != null) {
                    mProgressDialog.show();
                }
                break;
            case SUCCESS:
                new MaterialDialog.Builder(getActivity())
                        .title("Save Changes")
                        .content("changes you made are successfully saved")
                        .positiveText("OK")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                mAdapter.notifyDataSetChanged();
                            }
                        })
                        .show();
                break;
        }
    }

    @Override
    public void showError(String error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("OK").show();
    }
}
