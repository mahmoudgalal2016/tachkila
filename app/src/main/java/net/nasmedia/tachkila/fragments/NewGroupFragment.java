package net.nasmedia.tachkila.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.SelectFriendsAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.RealPathUtil;
import net.nasmedia.tachkila.viewmodels.CreateGroupViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Sherif on 8/19/2016.
 */
public class NewGroupFragment extends BaseFragment implements OnDataLoaded {
    String avatarPath = null;
    @BindView(R.id.addGroupPictureInsTxt)
    TextView mAddGroupPictureInsTxt;
    @BindView(R.id.groupPictureImg)
    ImageView mGroupPictureImg;
    @BindView(R.id.editTextGroupName)
    EditText mGroupNameEdt;
    @BindView(R.id.searchEdt)
    EditText mSearchEdt;
    @BindView(R.id.friendsList)
    RecyclerView mFriendsList;
    @BindView(R.id.friendsContainer)
    LinearLayout mFriendsContainer;

    SelectFriendsAdapter mSelectFriendsAdapter;

    ProgressDialog mProgressDialog;
    CreateGroupViewModel mViewModel;

    public static BaseFragment getInstance() {
        return new NewGroupFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mViewModel = new CreateGroupViewModel(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_group, container, false);
        ButterKnife.bind(this, view);

        mSearchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ((SelectFriendsAdapter) mFriendsList.getAdapter()).filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    @Override
    public String getTitle() {
        return "Create Group";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT < 19) {
                        Intent intent = new Intent();
                        intent.setType("image/jpeg");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(intent, 1);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/jpeg");
                        startActivityForResult(intent, 1);
                    }
                } else {
                    Toast.makeText(getActivity(),"No permission for Gallery", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    public void pickImage() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            if (Build.VERSION.SDK_INT < 19) {
                Intent intent = new Intent();
                intent.setType("image/jpeg");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 1);
            } else {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/jpeg");
                startActivityForResult(intent, 1);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                avatarPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getActivity(), data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                avatarPath = RealPathUtil.getRealPathFromURI_API11to18(getActivity(), data.getData());

                // SDK > 19 (Android 4.4)
            else
                avatarPath = RealPathUtil.getRealPathFromURI_API19(getActivity(), data.getData());
            Glide.with(getActivity()).load(avatarPath).bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool())).into(mGroupPictureImg);


            mAddGroupPictureInsTxt.setVisibility(View.GONE);
            mGroupPictureImg.setVisibility(View.VISIBLE);
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        } else return null;
    }

    @OnClick({R.id.addGroupPictureInsTxt, R.id.groupPictureImg})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addGroupPictureInsTxt:
            case R.id.groupPictureImg:
                pickImage();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_group_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                createGroup();
                break;
        }
        return true;

    }

    private void createGroup() {
        if (!TextUtils.isEmpty(mGroupNameEdt.getText().toString())) {
            mViewModel.createGroup(avatarPath, mGroupNameEdt.getText().toString(), mSelectFriendsAdapter != null ? mSelectFriendsAdapter.getSelectedFriends() : null);
        } else {
            Toast.makeText(getActivity(), "Please enter group name", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_happened))
                        .positiveText("OK").show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_No_Ineternet_connection))
                        .positiveText("OK").show();
                break;
            case START:
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setMessage("Please wait ...");
                mProgressDialog.setTitle(null);
                mProgressDialog.show();
                break;
            case SUCCESS:
                new MaterialDialog.Builder(getActivity())
                        .title("Group Created")
                        .content("Group created successfully")
                        .positiveText("OK")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                getActivity().finish();
                            }
                        })
                        .show();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();
        if (mViewModel.getUserFriends() != null && mViewModel.getUserFriends().size() > 0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            mFriendsList.setLayoutManager(linearLayoutManager);
            mSelectFriendsAdapter = new SelectFriendsAdapter(getActivity());
            mSelectFriendsAdapter.setFriendsList(mViewModel.getUserFriends());
            mFriendsList.setAdapter(mSelectFriendsAdapter);
            mFriendsContainer.setVisibility(View.VISIBLE);
        } else {
            mFriendsContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }
}
