package net.nasmedia.tachkila.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.logentries.logger.AndroidLogger;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.ColorsAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.Color;
import net.nasmedia.tachkila.utils.CustomFontUtils;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Mahmoud Galal on 8/13/2016.
 */
public class NewMatchFragment extends BaseFragment implements OnDataLoaded, NewMatchViewModel.OnError {

    @BindView(R.id.groupNamesSpn)
    Spinner mGroupNamesSpn;
    @BindView(R.id.numPlayersSpn)
    Spinner mNumPlayersSpn;
    @BindView(R.id.captineContainer)
    LinearLayout captineContainer;
    @BindView(R.id.dateTxt)
    TextView dateTxt;
    @BindView(R.id.dateContainer)
    LinearLayout dateContainer;
    @BindView(R.id.timeTxt)
    TextView timeTxt;
    @BindView(R.id.timeContainer)
    LinearLayout timeContainer;
    @BindView(R.id.TeamAColorSpn)
    Spinner teamACircle;
    @BindView(R.id.TeamBColorSpn)
    Spinner teamBCircle;
    @BindView(R.id.locationBtn)
    Button locationBtn;
    @BindView(R.id.createMatchBtn)
    Button createMatchBtn;
    @BindView(R.id.captineTxt)
    TextView captineTxt;


    DatePickerDialog datePickerDialog;
    @BindView(R.id.notesEdt)
    EditText notesEdt;
    private MaterialDialog mProgressDialog;
    private NewMatchViewModel mViewModel;

    int PLACE_PICKER_REQUEST = 1;
    private String mLocation = "";
    private double mLat, mLng;

    private int mGroupId;

    private AndroidLogger logger = null;

    public static NewMatchFragment getInstance(int groupId) {
        NewMatchFragment fragment = new NewMatchFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("groupId", groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mGroupId = getArguments().getInt("groupId");
        mViewModel = new NewMatchViewModel(getActivity(), this, this);
        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content("Please wait ...")
                .progress(true, 0)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_match, container, false);
        ButterKnife.bind(this, view);

        try {
            logger = AndroidLogger.createInstance(getApplicationContext(), false, true, false, null, 0, "d6a43a1c-f42c-4053-9090-cb2ac2e3de91", true);
        } catch (IOException e) {
            e.printStackTrace();
        }


        CustomFontUtils.applyButtonCustomFont(locationBtn, R.string.font_light, getActivity());
        CustomFontUtils.applyButtonCustomFont(createMatchBtn, R.string.font_light, getActivity());

        return view;
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }


    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();


        ArrayAdapter<String> groupsNamesAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_new_match_spinner_text, mViewModel.getGroupsNames());
        groupsNamesAdapter.setDropDownViewResource(R.layout.item_spinner_text);
        mGroupNamesSpn.setAdapter(groupsNamesAdapter);
        mGroupNamesSpn.setSelection(mViewModel.getGroupPositionByGroupId(mGroupId));
        ArrayAdapter<String> playerNumsAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_new_match_spinner_text, mViewModel.getNumOfPlayersNames());
        playerNumsAdapter.setDropDownViewResource(R.layout.item_spinner_text);
        mNumPlayersSpn.setAdapter(playerNumsAdapter);

        List<Color> colors = mViewModel.getChosserColors();
        final ColorsAdapter teamAAdapter = new ColorsAdapter(getActivity(), colors);
        teamACircle.setAdapter(teamAAdapter);
        final ColorsAdapter teamBAdapter = new ColorsAdapter(getActivity(), colors);
        teamBCircle.setAdapter(teamBAdapter);

        teamACircle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                if (teamBCircle.getSelectedItemPosition() == i) {
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("You can not set both teams with same color")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (i == 0) {
                                        teamACircle.setSelection(1);
                                    } else {
                                        teamACircle.setSelection(i - 1);
                                    }
                                }
                            })
                            .positiveText("OK").show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        teamBCircle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                if (teamACircle.getSelectedItemPosition() == i) {
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("You can not set both teams with same color")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (i == 0) {
                                        teamBCircle.setSelection(1);
                                    } else {
                                        teamBCircle.setSelection(i - 1);
                                    }
                                }
                            })
                            .positiveText("OK").show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        teamBCircle.setSelection(1);

        captineTxt.setText(mViewModel.getUserName());


    }

    @Override
    public String getTitle() {
        return "New Match";
    }

    @OnClick({R.id.captineContainer, R.id.dateContainer, R.id.timeContainer, R.id.createMatchBtn, R.id.locationBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.captineContainer:
                break;
            case R.id.dateContainer:
                showDatePickerDialog();
                break;
            case R.id.timeContainer:
                final TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, i);
                        datetime.set(Calendar.MINUTE, i1);

                        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                            timeTxt.setText(String.format("%02d", i) + ":" + String.format("%02d", i1) + " AM");
                        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                            timeTxt.setText(String.format("%02d", (i - 12)) + ":" + String.format("%02d", i1) + " PM");
                    }
                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), false);
                timePickerDialog.show();
                break;
            case R.id.createMatchBtn:
                if (mViewModel.getGroupsNames().size() > 0) {
                    if (!mLocation.equals("")) {
                        if (teamACircle.getSelectedItemPosition() != teamBCircle.getSelectedItemPosition()) {
                            if (!TextUtils.isEmpty(dateTxt.getText().toString()) && !TextUtils.isEmpty(timeTxt.getText().toString())) {
                                String time = "";
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM yyyy, dd hh:mm aaa", new Locale("US"));
                                try {
                                    logger.log(dateTxt.getText().toString() + " " + timeTxt.getText().toString());
                                    Log.d("time", dateTxt.getText().toString() + " " + timeTxt.getText().toString());
                                    Date before = simpleDateFormat.parse(dateTxt.getText().toString() + " " + timeTxt.getText().toString());
//                                    Date before = simpleDateFormat.parse("Jan 2017, 5 7:0 PM");
                                    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", new Locale("US"));
                                    time = simpleDateFormat1.format(before);
                                } catch (ParseException e) {
                                    logger.log(e.getMessage());
                                    e.printStackTrace();
                                }
                                mViewModel.createMatch(time, mViewModel.getGameTypeId(mNumPlayersSpn.getSelectedItemPosition()), mViewModel.getGroupId(mGroupNamesSpn.getSelectedItemPosition()),
                                        mViewModel.getChossenColorId(teamACircle.getSelectedItemPosition()), mViewModel.getChossenColorId(teamBCircle.getSelectedItemPosition()), mLocation, mLng, mLat, notesEdt.getText().toString());
                            } else {
                                new MaterialDialog.Builder(getActivity())
                                        .title("Error")
                                        .content("You must choose date and time")
                                        .positiveText("OK").show();
                            }
                        } else {
                            new MaterialDialog.Builder(getActivity())
                                    .title("Error")
                                    .content("Both teams can not have same colors")
                                    .positiveText("OK").show();
                        }
                    } else {
                        new MaterialDialog.Builder(getActivity())
                                .title("Error")
                                .content("Please select location first")
                                .positiveText("OK").show();
                    }


                } else {
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Please create or join any group first")
                            .positiveText("OK").show();
                }
                break;
            case R.id.locationBtn:
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getActivity());
                mLocation = place.getAddress().toString();
                mLat = place.getLatLng().latitude;
                mLng = place.getLatLng().longitude;
                locationBtn.setText(mLocation);
            }
        }
    }

    private String getMonthName(int month) {
        switch (month) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";

            default:
                return "";
        }
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                dateTxt.setText(getMonthName(i1) + " " + i + ", " + String.format("%02d", i2));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_happened))
                        .positiveText("OK").show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_No_Ineternet_connection))
                        .positiveText("OK").show();
                break;
            case START:

                if (mProgressDialog != null) {
                    mProgressDialog.show();
                }
                break;
            case SUCCESS:
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                new MaterialDialog.Builder(getActivity())
                        .title("Game Created")
                        .content("Game created successfully, you can add or remove players by click on your Game name.")
                        .positiveText("OK")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                getActivity().finish();
                            }
                        })
                        .show();
                break;
        }
    }

    @Override
    public void showError(String error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("OK").show();
    }
}
