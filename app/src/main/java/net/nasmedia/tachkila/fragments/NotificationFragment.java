package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tubb.smrv.SwipeMenuRecyclerView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.NotificationAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.Notification;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.RecyclerItemClickListener;
import net.nasmedia.tachkila.viewmodels.NotificationViewModel;
import net.nasmedia.tachkila.widgets.DividerItemDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 7/16/2016.
 */
public class NotificationFragment extends BaseFragment implements OnDataLoaded {

    @BindView(R.id.loadingProgress)
    ProgressBar mLoadingProgress;
    @BindView(R.id.notificationRecyclerView)
    SwipeMenuRecyclerView mRecyclerView;
    @BindView(R.id.noData)
    TextView mNoDataTxt;

    private NotificationViewModel mModel;
    private NotificationAdapter mAdapter;

    public static NotificationFragment getInstance() {
        return new NotificationFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mModel = new NotificationViewModel(context, this);
        mAdapter = new NotificationAdapter(context);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        ButterKnife.bind(this, view);


        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), R.dimen.notifictionrecyclerViewVerticalSpace);
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Notification notification = ((NotificationAdapter)mRecyclerView.getAdapter()).getItemAtPosition(position);
                if (notification.getFriend_id() != 0) {
                    Intent playerIntent = new Intent(getActivity(), DetailsActivity.class);
                    playerIntent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, notification.getFriend_id());
                    playerIntent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
                    playerIntent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                    startActivity(playerIntent);
                }else if (notification.getGame_id() != 0) {
                    Intent gameDetails = new Intent(getActivity(), DetailsActivity.class);
                    gameDetails.putExtra(GameDetailsFragment.ARG_GAME_ID, notification.getGame_id());
                    gameDetails.setAction(Constants.ACTION_SHOW_GAME_DETAILS_FRAGMENT);
                    startActivity(gameDetails);
                }else if (notification.getGroup_id() != 0) {
                    Intent groupIntent = new Intent(getActivity(), DetailsActivity.class);
                    groupIntent.putExtra(GroupDetailsFragment.ARG_GROUP_ID, notification.getGroup_id());
                    groupIntent.setAction(Constants.ACTION_SHOW_GROUP_DETAILS_FRAGMENT);
                    startActivity(groupIntent);
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mModel.onStart();
    }

    @Override
    public void onStop() {
        mModel.onStop();
        super.onStop();
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        switch (responseStates) {
            case ERROR:

                break;
            case NO_DATA:
                mLoadingProgress.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.GONE);
                mNoDataTxt.setVisibility(View.VISIBLE);
                break;
            case NO_NETWORK:
                mLoadingProgress.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.GONE);
                mNoDataTxt.setVisibility(View.GONE);

                Toast.makeText(getActivity(), "No Internet connection", Toast.LENGTH_LONG).show();

                break;
            case START:
                mLoadingProgress.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                mNoDataTxt.setVisibility(View.GONE);
                break;
            case SUCCESS:
                mAdapter.setNotificationList(mModel.getNotifications());
                mLoadingProgress.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                mNoDataTxt.setVisibility(View.GONE);

                break;
        }
    }

    @Override
    public String getTitle() {
        return "Notification";
    }
}
