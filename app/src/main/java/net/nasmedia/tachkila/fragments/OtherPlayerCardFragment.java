package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;
import net.nasmedia.tachkila.viewmodels.PlayerCardViewModel;
import net.nasmedia.tachkila.widgets.CircularProgressBar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by mahmoudgalal on 7/29/16.
 */
public class OtherPlayerCardFragment extends BaseFragment implements OnDataLoaded, NewMatchViewModel.OnError {


    @BindView(R.id.playerImg)
    ImageView playerImg;
    @BindView(R.id.playerNameTxt)
    TextView playerNameTxt;
    @BindView(R.id.playerNickNameTxt)
    TextView playerNickNameTxt;
    @BindView(R.id.ageTxt)
    TextView ageTxt;
    @BindView(R.id.countryTxt)
    TextView countryTxt;
    @BindView(R.id.positionTxt)
    TextView positionTxt;
    @BindView(R.id.heightTxt)
    TextView heightTxt;
    @BindView(R.id.footTxt)
    TextView footTxt;
    @BindView(R.id.tshirtNumTxt)
    TextView tshirtNumTxt;
    @BindView(R.id.scoreTxt)
    TextView scoreTxt;
    @BindView(R.id.winProgress)
    CircularProgressBar winProgress;

    public static final String ARG_PLAYER_ID = "ARG_PLAYER_ID";
    public static final String ARG_TYPE = "ARG_TYPE";
    @BindView(R.id.addPlayerBtn)
    ImageButton addPlayerBtn;
    @BindView(R.id.noImgTxt)
    ImageView mNoImgTxt;
    @BindView(R.id.imgContainer)
    FrameLayout imgContainer;
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.loadingProgress)
    ProgressBar loadingProgress;

    private int mPlayerId;
    PlayerCardViewModel mViewModel;

    PlayerCardViewModel.FriendState mFriendState;

    private MaterialDialog mProgressDialog;
    private MaterialDialog.Builder mMaterialDialogBuilder;

    String mType;

    public static final String PLAYER = "PLAYER";
    public static final String REQUEST = "REQUEST";
    public static final String SUGGEST = "SUGGEST";

    public static OtherPlayerCardFragment getInstance(int playerId, String type) {
        OtherPlayerCardFragment fragment = new OtherPlayerCardFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_PLAYER_ID, playerId);
        bundle.putString(ARG_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPlayerId = getArguments().getInt(ARG_PLAYER_ID);
        mType = getArguments().getString(ARG_TYPE);
        mViewModel = new PlayerCardViewModel(getActivity(), mPlayerId, this, addFriendFinished, removeFriendFinished, this);
        mMaterialDialogBuilder = new MaterialDialog.Builder(context);
        mMaterialDialogBuilder.content(R.string.Please_wait);
        mMaterialDialogBuilder.progress(true, 0);
    }

    public int getAge(int _year, int _month, int _day) {

        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        a = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        if (a < 0)
            a = 0;
        return a;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player_card, container, false);
        ButterKnife.bind(this, view);

        switch (mType) {
            case PLAYER:
            case SUGGEST:
                addPlayerBtn.setVisibility(View.VISIBLE);
                break;
            case REQUEST:
                addPlayerBtn.setVisibility(View.GONE);
                break;
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();
        mViewModel.viewPlayer(mPlayerId);
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }

    private void loadImage(String url, ImageView imageView) {
        Glide.with(this)
                .load(url)
                .placeholder(R.drawable.empty_circl)
                .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool()))
                .into(imageView);

    }

    @Override
    public String getTitle() {
        return "Player Card";
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        switch (responseStates) {
            case ERROR:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_happened))
                        .positiveText("OK").show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_No_Ineternet_connection))
                        .positiveText("OK").show();

                loadingProgress.setVisibility(View.GONE);
                container.setVisibility(View.GONE);
                break;
            case START:
                loadingProgress.setVisibility(View.VISIBLE);
                container.setVisibility(View.GONE);
                break;
            case SUCCESS:
                My_current_friend user = mViewModel.getMyCurrentFriend();

                playerNameTxt.setText(user.getName());
                playerNickNameTxt.setText("@" + user.getUsername());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    if (user.getBirthday() != null) {
                        Calendar calendar = Calendar.getInstance();

                        Date birthdayDate = simpleDateFormat.parse(user.getBirthday());
                        calendar.setTime(birthdayDate);
                        ageTxt.setText(getAge(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)) + "");
                    } else {
                        ageTxt.setText("0");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                    ageTxt.setText("0");
                }
                positionTxt.setText(mViewModel.getPostionName(user.getPosition_id()));
                countryTxt.setText(mViewModel.getCountryName(user.getCountry_id()));
                footTxt.setText(user.getPerfect_foot());
                tshirtNumTxt.setText(user.getFavorite_number() + "");
                scoreTxt.setText(user.getPoints() + "");
                winProgress.setMax(user.getWin() + user.getLosses());
                winProgress.setProgress(user.getWin());
                winProgress.setTitle("Win " + user.getWin());
                winProgress.setLoss("Lose " + user.getLosses());
                winProgress.setSubTitle("Total " + (user.getWin() + user.getLosses()));
                heightTxt.setText(user.getHeight() + " CM");


                mFriendState = mViewModel.isUserInFriends(user.getId());
                if (mFriendState == PlayerCardViewModel.FriendState.FRIEND) {
                    addPlayerBtn.setImageResource(R.drawable.ic_remove_friend);
                } else if (mFriendState == PlayerCardViewModel.FriendState.NOT_FRIEND) {
                    addPlayerBtn.setImageResource(R.drawable.ic_add_player_btn);
                } else {
                    addPlayerBtn.setImageResource(R.drawable.ic_pending_request_btn);
                }
                if (user.getAvatar() != null && !user.getAvatar().isEmpty()) {
                    mNoImgTxt.setVisibility(View.GONE);
                    playerImg.setVisibility(View.VISIBLE);
                    loadImage(Constants.PIC_AVATAR_URL + user.getAvatar(), playerImg);
                } else {
                    mNoImgTxt.setVisibility(View.VISIBLE);
                    playerImg.setVisibility(View.GONE);
                }

                loadingProgress.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                break;
        }


    }

    @OnClick(R.id.addPlayerBtn)
    public void onClick() {
        if (mFriendState == PlayerCardViewModel.FriendState.FRIEND) {
            mViewModel.removeFriend();
        } else if (mFriendState == PlayerCardViewModel.FriendState.NOT_FRIEND) {
            mViewModel.addFriend();
        }
    }

    OnDataLoaded addFriendFinished = new OnDataLoaded() {
        @Override
        public void downloadComplete(ResponseStates responseStates) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            switch (responseStates) {
                case ERROR:
                    showErrorDialog(getString(R.string.error_happened));
                    break;

                case SUCCESS:
                    mFriendState = PlayerCardViewModel.FriendState.PENDING;
                    addPlayerBtn.setImageResource(R.drawable.ic_pending_request_btn);
                    break;

                case NO_DATA:

                    break;
                case NO_NETWORK:
                    showErrorDialog(getString(R.string.error_No_Ineternet_connection));
                    break;
                case START:
                    if (mProgressDialog == null) {
                        mProgressDialog = mMaterialDialogBuilder.show();
                    } else if (!mProgressDialog.isShowing()) {
                        mProgressDialog = mMaterialDialogBuilder.show();
                    }
                    break;
            }

        }
    };

    private void showErrorDialog(String message) {
        new AlertDialogWrapper.Builder(getActivity())
                .setTitle(R.string.Error)
                .setMessage(message)
                .setNegativeButton(R.string.OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    OnDataLoaded removeFriendFinished = new OnDataLoaded() {
        @Override
        public void downloadComplete(ResponseStates responseStates) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            switch (responseStates) {
                case ERROR:
                    showErrorDialog(getString(R.string.error_happened));
                    break;

                case SUCCESS:
                    mFriendState = PlayerCardViewModel.FriendState.NOT_FRIEND;
                    addPlayerBtn.setImageResource(R.drawable.add_player);
                    break;

                case NO_DATA:

                    break;
                case NO_NETWORK:
                    showErrorDialog(getString(R.string.error_No_Ineternet_connection));
                    break;
                case START:
                    if (mProgressDialog == null) {
                        mProgressDialog = mMaterialDialogBuilder.show();
                    } else if (!mProgressDialog.isShowing()) {
                        mProgressDialog = mMaterialDialogBuilder.show();
                    }
                    break;
            }
        }
    };

    @Override
    public void showError(String error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("OK").show();
    }
}
