package net.nasmedia.tachkila.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.Position;
import net.nasmedia.tachkila.realmmodels.test.Country;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.CircularProgressBar;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by mahmoudgalal on 7/29/16.
 */
public class PlayerCardFragment extends BaseFragment {

    private static final String TAG = PlayerCardFragment.class.getSimpleName();


    @BindView(R.id.playerImg)
    ImageView playerImg;
    @BindView(R.id.playerNameTxt)
    TextView playerNameTxt;
    @BindView(R.id.playerNickNameTxt)
    TextView playerNickNameTxt;
    @BindView(R.id.ageTxt)
    TextView ageTxt;
    @BindView(R.id.countryTxt)
    TextView countryTxt;
    @BindView(R.id.positionTxt)
    TextView positionTxt;
    @BindView(R.id.heightTxt)
    TextView heightTxt;
    @BindView(R.id.footTxt)
    TextView footTxt;
    @BindView(R.id.tshirtNumTxt)
    TextView tshirtNumTxt;
    @BindView(R.id.scoreTxt)
    TextView scoreTxt;
    @BindView(R.id.winProgress)
    CircularProgressBar winProgress;
    @BindView(R.id.noImgTxt)
    ImageView mNoImgTxt;

    public static PlayerCardFragment getInstance() {
        return new PlayerCardFragment();
    }

    public int getAge(int _year, int _month, int _day) {

        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        a = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        if (a < 0)
            a= 0;
        return a;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player_card, container, false);
        ButterKnife.bind(this, view);

        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(getActivity())).findFirst();

        playerNameTxt.setText(user.getName());
        playerNickNameTxt.setText("@" + user.getUsername());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            DateTime birthdayDate = new DateTime(simpleDateFormat.parse(user.getBirthday()));


            ageTxt.setText(getAge(birthdayDate.getYear(), birthdayDate.getMonthOfYear(), birthdayDate.getDayOfMonth()) + "");
        } catch (ParseException e) {
            e.printStackTrace();
            ageTxt.setText("0");
        }
        positionTxt.setText(realm.where(Position.class).equalTo("id", user.getPosition_id()).findFirst().getName());
        countryTxt.setText(realm.where(Country.class).equalTo("id", user.getCountry_id()).findFirst().getTitleEn());
        footTxt.setText(user.getPerfect_foot());
        tshirtNumTxt.setText(user.getFavorite_number() + "");
        scoreTxt.setText(user.getPoints() + "");
        winProgress.setMax(user.getWin() + user.getLosses());
        winProgress.setProgress(user.getWin());
        winProgress.setTitle("Win " + user.getWin());
        winProgress.setLoss("Lose " + user.getLosses());
        winProgress.setSubTitle("Total " + (user.getWin() + user.getLosses()));
        heightTxt.setText(user.getHeight() + " CM");

        if (user.getAvatar() != null && !user.getAvatar().isEmpty()){
            mNoImgTxt.setVisibility(View.GONE);
            playerImg.setVisibility(View.VISIBLE);
            loadImage(Constants.PIC_AVATAR_URL + user.getAvatar(), playerImg);
        }else{
            mNoImgTxt.setVisibility(View.VISIBLE);
            playerImg.setVisibility(View.GONE);
        }


        realm.close();

        return view;
    }

    private void loadImage(String url, ImageView imageView) {
        if (url == null || url.equals("")) {
            imageView.setImageResource(R.drawable.empty_circl);
        } else {
            Glide.with(this)
                    .load(url)
                    .placeholder(R.drawable.empty_circl)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool()))
                    .into(imageView);
        }
    }

    @Override
    public String getTitle() {
        return "Player Card";
    }
}
