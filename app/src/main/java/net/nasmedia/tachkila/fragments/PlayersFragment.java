package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.BaseAdapter;
import net.nasmedia.tachkila.adapters.PlayersAdapter;
import net.nasmedia.tachkila.adapters.RequestsAdapter;
import net.nasmedia.tachkila.adapters.SuggestedAdapter;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.RecyclerItemClickListener;
import net.nasmedia.tachkila.viewmodels.PlayersViewModel;
import net.nasmedia.tachkila.widgets.DividerItemDecoration;
import com.tubb.smrv.SwipeMenuRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 9/17/2016.
 */
public class PlayersFragment extends Fragment {

    public static final String ARG_TYPE = "ARG_TYPE";
    public static final String TYPE_PLAYERS = "TYPE_PLAYERS";
    public static final String TYPE_REQUESTES = "TYPE_REQUESTES";
    public static final String TYPE_SUGGESTED = "TYPE_SUGGESTED";

    @BindView(R.id.recyclerView)
    SwipeMenuRecyclerView mRecyclerView;
    @BindView(R.id.noDataTxt)
    TextView mNoDataTxt;

    private String mType;
    private PlayersViewModel mViewModel;
    BaseAdapter mAdapter;

    public static PlayersFragment getInstance(String type) {
        PlayersFragment playersFragment = new PlayersFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TYPE, type);
        playersFragment.setArguments(bundle);
        return playersFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mType = getArguments().getString(ARG_TYPE);
        mViewModel = new PlayersViewModel(getActivity(), mType);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players, container, false);
        ButterKnife.bind(this, view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), R.dimen.recyclerViewVerticalSpace));
        switch (mType) {
            case TYPE_PLAYERS:
                mAdapter = new PlayersAdapter(getActivity());
                break;
            case TYPE_REQUESTES:
                mAdapter = new RequestsAdapter(getActivity());
                break;
            case TYPE_SUGGESTED:
                mAdapter = new SuggestedAdapter(getActivity());
                break;
        }

        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();

        List<My_current_friend> users = mViewModel.getList();
        if (users != null && users.size() > 0) {
            mAdapter.setUsers(users);
            mNoDataTxt.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {

            mNoDataTxt.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }

}
