package net.nasmedia.tachkila.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.SuperscriptSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.RegisterWelcomeActivity;
import net.nasmedia.tachkila.adapters.CountriesSpinnerAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.CustomFontUtils;
import net.nasmedia.tachkila.utils.RealPathUtil;
import net.nasmedia.tachkila.viewmodels.RegisterViewModel;
import net.nasmedia.tachkila.widgets.WhiteCircularTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by mahmoudgalal on 5/21/16.
 */
public class RegisterFragment extends Fragment implements OnDataLoaded, RegisterViewModel.OnError {

    public static final String ARG_REGISTER_TYPE = "ARG_REGISTER_TYPE";
    public static final String ARG_GOOGLE_ACCOUNT = "ARG_GOOGLE_ACCOUNT";

    @BindView(R.id.uploadBtn)
    Button uploadBtn;
    @BindView(R.id.nameEdt)
    EditText nameEdt;
    @BindView(R.id.userNameEdt)
    EditText userNameEdt;
    @BindView(R.id.emailEdt)
    EditText emailEdt;
    @BindView(R.id.passwordEdt)
    EditText passwordEdt;
    @BindView(R.id.phoneEdt)
    EditText phoneEdt;
    @BindView(R.id.heightEdt)
    EditText heightEdt;
    @BindView(R.id.tshirtNumEdt)
    EditText frontNumEdt;
    @BindView(R.id.countriesSpn)
    Spinner countriesSpn;
    @BindView(R.id.rightFootCheck)
    CheckedTextView rightCheck;
    @BindView(R.id.leftFootCheck)
    CheckedTextView leftCheck;
    @BindView(R.id.maleCheck)
    CheckedTextView maleCheck;
    @BindView(R.id.femaleCheck)
    CheckedTextView femaleCheck;
    @BindView(R.id.profilePictureInsTxt)
    WhiteCircularTextView mProfilePictureInsTxt;
    @BindView(R.id.profilePictureImg)
    ImageView mProfilePictureImg;

    String avatarPath;
    String favFoot;
    String sex;
    String countryId;
    RegisterViewModel mModel;
    RegisterType mRegisterType;
    @BindView(R.id.birthDayTxt)
    TextView mBirthDayTxt;
    @BindView(R.id.dayTxt)
    TextView mDayTxt;
    @BindView(R.id.yearTxt)
    TextView mYearTxt;
    @BindView(R.id.birthDayContainer)
    RelativeLayout mBirthDayContainer;
    @BindView(R.id.monthTxt)
    TextView mMonthTxt;
    @BindView(R.id.positionSpn)
    Spinner mPositionSpn;

    private MaterialDialog mProgressDialog;
    DatePickerDialog datePickerDialog;
    private GoogleSignInAccount mGoogleSignInAccount;
    private String mSocialTokenId;

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:

                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_happened))
                        .positiveText("OK").show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content(getString(R.string.error_No_Ineternet_connection))
                        .positiveText("OK").show();
                break;
            case START:
                mProgressDialog.show();
                break;
            case SUCCESS:
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                new MaterialDialog.Builder(getActivity())
                        .title("Success")
                        .content("Thank you for registration")
                        .positiveText("OK")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                startActivity(new Intent(getActivity(), RegisterWelcomeActivity.class));
                                getActivity().finish();
                            }
                        })
                        .show();
                break;
        }
    }

    @Override
    public void showError(String error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("OK").show();
    }


    public enum RegisterType {
        NORMAL,
        GOOGLE,
        FACEBOOK
    }

    public static RegisterFragment getInstance(RegisterType registerType, GoogleSignInAccount googleSignInAccount) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_REGISTER_TYPE, registerType.ordinal());
        if (googleSignInAccount != null) {
            bundle.putParcelable(ARG_GOOGLE_ACCOUNT, googleSignInAccount);
        }
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mModel = new RegisterViewModel(getActivity(), this, this);
        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content("Please wait ...")
                .progress(true, 0)
                .build();

        mRegisterType = RegisterType.values()[getArguments().getInt(ARG_REGISTER_TYPE)];
        if (getArguments().containsKey(ARG_GOOGLE_ACCOUNT)) {
            mGoogleSignInAccount = getArguments().getParcelable(ARG_GOOGLE_ACCOUNT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, null);
        ButterKnife.bind(this, view);

        CustomFontUtils.applyEditTextCustomFont(nameEdt, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyEditTextCustomFont(userNameEdt, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyEditTextCustomFont(emailEdt, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyEditTextCustomFont(passwordEdt, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyEditTextCustomFont(phoneEdt, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyEditTextCustomFont(heightEdt, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyEditTextCustomFont(frontNumEdt, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyCheckBoxCustomFont(leftCheck, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyCheckBoxCustomFont(femaleCheck, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyCheckBoxCustomFont(maleCheck, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyCheckBoxCustomFont(rightCheck, CustomFontUtils.LIGHT, getActivity());
        CustomFontUtils.applyButtonCustomFont(uploadBtn, CustomFontUtils.LIGHT, getActivity());


        SpannableStringBuilder cs = new SpannableStringBuilder(getString(R.string.hint_name));
        cs.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        nameEdt.setHint(cs);

        SpannableStringBuilder cs1 = new SpannableStringBuilder(getString(R.string.hint_username));
        cs1.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        userNameEdt.setHint(cs1);

        SpannableStringBuilder cs2 = new SpannableStringBuilder(getString(R.string.hint_mail));
        cs2.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        emailEdt.setHint(cs2);

        SpannableStringBuilder cs3 = new SpannableStringBuilder(getString(R.string.hint_reg_password));
        cs3.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        passwordEdt.setHint(cs3);

        SpannableStringBuilder cs4 = new SpannableStringBuilder(getString(R.string.hint_phone));
        cs4.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        phoneEdt.setHint(cs4);

        SpannableStringBuilder cs5 = new SpannableStringBuilder(getString(R.string.star_Birthday));
        cs5.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBirthDayTxt.setText(cs5);

        maleCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!maleCheck.isChecked()) {
                    maleCheck.setChecked(true);
                    femaleCheck.setChecked(false);
                }
            }
        });

        femaleCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!femaleCheck.isChecked()) {
                    maleCheck.setChecked(false);
                    femaleCheck.setChecked(true);
                }
            }
        });

        rightCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!rightCheck.isChecked()) {
                    rightCheck.setChecked(true);
                    leftCheck.setChecked(false);
                }
            }
        });

        leftCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!leftCheck.isChecked()) {
                    rightCheck.setChecked(false);
                    leftCheck.setChecked(true);
                }
            }
        });

        switch (mRegisterType) {
            case FACEBOOK:
                getDataFromFaceBook();
                break;
            case GOOGLE:
                getDataFromGoogle();
                break;
            case NORMAL:

                break;
        }

        return view;
    }

    private void getDataFromGoogle() {
        nameEdt.setText(mGoogleSignInAccount.getDisplayName());
        emailEdt.setText(mGoogleSignInAccount.getEmail());
        mSocialTokenId = mGoogleSignInAccount.getId();
        if (mGoogleSignInAccount.getPhotoUrl() != null && !TextUtils.isEmpty(mGoogleSignInAccount.getPhotoUrl().toString()))
            Glide.with(getActivity())
                    .load(mGoogleSignInAccount.getPhotoUrl())
                    .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool()))
                    .into(mProfilePictureImg);
    }


    private void getDataFromFaceBook() {
        nameEdt.setText(String.format("%s %s", Profile.getCurrentProfile().getFirstName(), Profile.getCurrentProfile().getLastName()));
        mSocialTokenId = AccessToken.getCurrentAccessToken().getUserId();
//        Profile.getCurrentProfile().getProfilePictureUri()
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Log.v("LoginActivity Response ", response.toString());

                        try {
                            emailEdt.setText(object.getString("email"));
//                            birthDayEdt.setText(object.getString("birthday"));
                            Log.d("gender", object.getString("gender"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onResume() {
        super.onResume();
        mModel.onStart();

        CountriesSpinnerAdapter adapter1 = new CountriesSpinnerAdapter(getActivity(), R.layout.item_spinner_text, mModel.getCountries());
        countriesSpn.setAdapter(adapter1);
        countriesSpn.setSelection(mModel.getKuwiatPosition());

        ArrayAdapter<String> positionAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_spinner_text, mModel.getPositionsNames());
        positionAdapter.setDropDownViewResource(R.layout.item_spinner_text);
        mPositionSpn.setAdapter(positionAdapter);
    }

    @Override
    public void onPause() {
        mModel.onStop();
        super.onPause();
    }

    @OnClick({R.id.profilePictureImg, R.id.profilePictureInsTxt, R.id.uploadBtn, R.id.birthDayContainer})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profilePictureImg:
            case R.id.profilePictureInsTxt:
                pickImage();
                break;
            case R.id.uploadBtn:
                uploadData();
                break;
            case R.id.birthDayContainer:
                showDatePickerDialog();
                break;
        }
    }

    private void showDatePickerDialog() {

        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                mDayTxt.setText(i + "");
                mYearTxt.setText(i2 + "");
                mMonthTxt.setText(getMonthName(i1));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }

    private String getMonthName(int month) {
        switch (month) {
            case 0:
                return "january";
            case 1:
                return "february";
            case 2:
                return "march";
            case 3:
                return "april";
            case 4:
                return "may";
            case 5:
                return "june";
            case 6:
                return "july";
            case 7:
                return "august";
            case 8:
                return "september";
            case 9:
                return "october";
            case 10:
                return "november";
            case 11:
                return "december";

            default:
                return "";
        }
    }

    private int getMonthNumber(String monthName) {
        switch (monthName) {
            case "january":
                return 1;
            case "february":
                return 2;
            case "march":
                return 3;
            case "april":
                return 4;
            case "may":
                return 5;
            case "june":
                return 6;
            case "july":
                return 7;
            case "august":
                return 8;
            case "september":
                return 9;
            case "october":
                return 10;
            case "november":
                return 11;
            case "december":
                return 12;

            default:
                return 1;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT < 19) {
                        Intent intent = new Intent();
                        intent.setType("image/jpeg");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(intent, 1);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/jpeg");
                        startActivityForResult(intent, 1);
                    }
                } else {
                    Toast.makeText(getActivity(),"No permission for Gallery", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    public void pickImage() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            if (Build.VERSION.SDK_INT < 19) {
                Intent intent = new Intent();
                intent.setType("image/jpeg");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 1);
            } else {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/jpeg");
                startActivityForResult(intent, 1);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }


            // SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                avatarPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getActivity(), data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                avatarPath = RealPathUtil.getRealPathFromURI_API11to18(getActivity(), data.getData());

                // SDK > 19 (Android 4.4)
            else
                avatarPath = RealPathUtil.getRealPathFromURI_API19(getActivity(), data.getData());

//            avatarPath = getPath(data.getData());

            Glide.with(getActivity())
                    .load(avatarPath)
                    .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool()))
                    .into(mProfilePictureImg);
            mProfilePictureImg.setVisibility(View.VISIBLE);
            mProfilePictureInsTxt.setVisibility(View.GONE);
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        } else return null;
    }

    private void uploadData() {
        boolean allValuesValid = true;

        if (TextUtils.isEmpty(nameEdt.getText().toString().trim())) {
            nameEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }

        if (TextUtils.isEmpty(userNameEdt.getText().toString().trim())) {
            userNameEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }

        if (TextUtils.isEmpty(emailEdt.getText().toString().trim())) {
            emailEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }

        if (!TextUtils.isEmpty(emailEdt.getText().toString().trim()) && !mModel.isEmailValid(emailEdt.getText().toString().trim())) {
            emailEdt.setError(getString(R.string.edittext_error_emailformat));
            allValuesValid = false;
        }

        if (TextUtils.isEmpty(passwordEdt.getText().toString().trim())) {
            passwordEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }

        if (!TextUtils.isEmpty(passwordEdt.getText().toString().trim()) && passwordEdt.getText().toString().trim().length() < 6) {
            passwordEdt.setError(getString(R.string.edittext_error_passwordlength));
            allValuesValid = false;
        }

        if (TextUtils.isEmpty(phoneEdt.getText().toString().trim())) {
            phoneEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }


        if (allValuesValid) {

            mModel.performRegistration(avatarPath,
                    userNameEdt.getText().toString().trim(),
                    passwordEdt.getText().toString().trim(),
                    emailEdt.getText().toString().trim(),
                    mDayTxt.getText().toString()  + "-" + getMonthNumber(mMonthTxt.getText().toString()) + "-" + mYearTxt.getText().toString(),
                    frontNumEdt.getText().toString().trim(),
                    leftCheck.isChecked() ? "left" : "right",
                    nameEdt.getText().toString().trim(),
                    phoneEdt.getText().toString().trim(),
                    femaleCheck.isChecked() ? "female" : "male",
                    heightEdt.getText().toString().trim(),
                    "516516516516516",
                    mModel.geCountryId(countriesSpn.getSelectedItemPosition()),
                    mRegisterType.name(),
                    mSocialTokenId == null ? "" : mSocialTokenId,
                    mModel.gePositionId(mPositionSpn.getSelectedItemPosition()));
        } else {
            new MaterialDialog.Builder(getActivity())
                    .title("Error")
                    .content("Please fill all required field")
                    .positiveText("OK").show();
        }


    }


}
