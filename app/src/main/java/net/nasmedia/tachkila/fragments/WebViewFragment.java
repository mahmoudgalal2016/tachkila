package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.afollestad.materialdialogs.MaterialDialog;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.utils.ConectivityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 6/26/2016.
 */
public class WebViewFragment extends BaseFragment {


    String mUrl;
    @BindView(R.id.webview)
    WebView mWebview;

    private MaterialDialog mProgressDialog;

    public static WebViewFragment getInstance(String url) {
        WebViewFragment webViewFragment = new WebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        webViewFragment.setArguments(bundle);
        return webViewFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mUrl = getArguments().getString("url");
        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content("Please wait ...")
                .progress(true, 0)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_webview, container, false);
        ButterKnife.bind(this, view);
        if (ConectivityUtils.isDeviceConnectedToNetwork(getActivity())) {

            mProgressDialog.show();

            mWebview.getSettings().setJavaScriptEnabled(true);
            mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
            mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
            mWebview.getSettings().setJavaScriptEnabled(true);
            mWebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
            mWebview.getSettings().setSupportMultipleWindows(false);
            mWebview.getSettings().setSupportZoom(false);
            mWebview.setVerticalScrollBarEnabled(false);
            mWebview.setHorizontalScrollBarEnabled(false);
            mWebview.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                }
            });
            mWebview.loadUrl(mUrl);

        } else {
            new MaterialDialog.Builder(getActivity())
                    .title("Error")
                    .content("Please check internet connection")
                    .positiveText("OK").show();
        }

        return view;
    }


    @Override
    public String getTitle() {
        return null;
    }
}
