
package net.nasmedia.tachkila.realmmodels;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.nasmedia.tachkila.realmmodels.test.Captain;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.Player;
import net.nasmedia.tachkila.realmmodels.test.Type;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ActiveGame extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("time_of_game")
    @Expose
    private String timeOfGame;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("map_lat")
    @Expose
    private double mapLat;
    @SerializedName("map_lng")
    @Expose
    private double mapLng;
    @SerializedName("group_id")
    @Expose
    private int groupId;
    @SerializedName("captain_id")
    @Expose
    private int captainId;
    @SerializedName("type_id")
    @Expose
    private int typeId;
    @SerializedName("team1_color_id")
    @Expose
    private int team1ColorId;
    @SerializedName("team2_color_id")
    @Expose
    private int team2ColorId;
    @SerializedName("completed")
    @Expose
    private int completed;
    @SerializedName("finished")
    @Expose
    private int finished;
    @SerializedName("team1_score")
    @Expose
    private int team1Score;
    @SerializedName("team2_score")
    @Expose
    private int team2Score;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("captain")
    @Expose
    private Captain captain;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("team1_color")
    @Expose
    private TeamColor teamColor;
    @SerializedName("team2_color")
    @Expose
    private TeamColor team2Color;
    @SerializedName("players")
    @Expose
    private RealmList<Player> players = new RealmList<>();
    @SerializedName("group")
    @Expose
    private Group group;

    @SerializedName("notes")
    @Expose
    private String notes;


    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The timeOfGame
     */
    public String getTimeOfGame() {
        return timeOfGame;
    }

    /**
     * @param timeOfGame The time_of_game
     */
    public void setTimeOfGame(String timeOfGame) {
        this.timeOfGame = timeOfGame;
    }

    /**
     * @return The location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return The mapLat
     */
    public double getMapLat() {
        return mapLat;
    }

    /**
     * @param mapLat The map_lat
     */
    public void setMapLat(double mapLat) {
        this.mapLat = mapLat;
    }

    /**
     * @return The mapLng
     */
    public double getMapLng() {
        return mapLng;
    }

    /**
     * @param mapLng The map_lng
     */
    public void setMapLng(double mapLng) {
        this.mapLng = mapLng;
    }

    /**
     * @return The groupId
     */
    public int getGroupId() {
        return groupId;
    }

    /**
     * @param groupId The group_id
     */
    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    /**
     * @return The captainId
     */
    public int getCaptainId() {
        return captainId;
    }

    /**
     * @param captainId The captain_id
     */
    public void setCaptainId(int captainId) {
        this.captainId = captainId;
    }

    /**
     * @return The typeId
     */
    public int getTypeId() {
        return typeId;
    }

    /**
     * @param typeId The type_id
     */
    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    /**
     * @return The team1ColorId
     */
    public int getTeam1ColorId() {
        return team1ColorId;
    }

    /**
     * @param team1ColorId The team1_color_id
     */
    public void setTeam1ColorId(int team1ColorId) {
        this.team1ColorId = team1ColorId;
    }

    /**
     * @return The team2ColorId
     */
    public int getTeam2ColorId() {
        return team2ColorId;
    }

    /**
     * @param team2ColorId The team2_color_id
     */
    public void setTeam2ColorId(int team2ColorId) {
        this.team2ColorId = team2ColorId;
    }

    /**
     * @return The completed
     */
    public int getCompleted() {
        return completed;
    }

    /**
     * @param completed The completed
     */
    public void setCompleted(int completed) {
        this.completed = completed;
    }

    /**
     * @return The finished
     */
    public int getFinished() {
        return finished;
    }

    /**
     * @param finished The finished
     */
    public void setFinished(int finished) {
        this.finished = finished;
    }

    /**
     * @return The team1Score
     */
    public int getTeam1Score() {
        return team1Score;
    }

    /**
     * @param team1Score The team1_score
     */
    public void setTeam1Score(int team1Score) {
        this.team1Score = team1Score;
    }

    /**
     * @return The team2Score
     */
    public int getTeam2Score() {
        return team2Score;
    }

    /**
     * @param team2Score The team2_score
     */
    public void setTeam2Score(int team2Score) {
        this.team2Score = team2Score;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The deletedAt
     */
    public String getDeletedAt() {
        return deletedAt;
    }

    /**
     * @param deletedAt The deleted_at
     */
    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * @return The captain
     */
    public Captain getCaptain() {
        return captain;
    }

    /**
     * @param captain The captain
     */
    public void setCaptain(Captain captain) {
        this.captain = captain;
    }

    /**
     * @return The type
     */
    public Type getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * @return The teamColor
     */
    public TeamColor getTeamColor() {
        return teamColor;
    }

    /**
     * @param teamColor The team1_color
     */
    public void setTeamColor(TeamColor teamColor) {
        this.teamColor = teamColor;
    }

    /**
     * @return The team2Color
     */
    public TeamColor getTeam2Color() {
        return team2Color;
    }

    /**
     * @param team2Color The team2_color
     */
    public void setTeam2Color(TeamColor team2Color) {
        this.team2Color = team2Color;
    }

    /**
     * @return The players
     */
    public List<Player> getPlayers() {
        return players;
    }

    /**
     * @param players The players
     */
    public void setPlayers(RealmList<Player> players) {
        this.players = players;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
