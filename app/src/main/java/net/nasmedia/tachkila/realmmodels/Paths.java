
package net.nasmedia.tachkila.realmmodels;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Paths extends RealmObject {


    @PrimaryKey
    private int primaryKey;
    @SerializedName("avatars")
    @Expose
    private String avatars;
    @SerializedName("groups")
    @Expose
    private String groups;
    @SerializedName("resize")
    @Expose
    private String resize;

    /**
     * 
     * @return
     *     The avatars
     */
    public String getAvatars() {
        return avatars;
    }

    /**
     * 
     * @param avatars
     *     The avatars
     */
    public void setAvatars(String avatars) {
        this.avatars = avatars;
    }

    /**
     * 
     * @return
     *     The groups
     */
    public String getGroups() {
        return groups;
    }

    /**
     * 
     * @param groups
     *     The groups
     */
    public void setGroups(String groups) {
        this.groups = groups;
    }

    /**
     * 
     * @return
     *     The resize
     */
    public String getResize() {
        return resize;
    }

    /**
     * 
     * @param resize
     *     The resize
     */
    public void setResize(String resize) {
        this.resize = resize;
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }
}
