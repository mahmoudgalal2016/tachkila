
package net.nasmedia.tachkila.realmmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Settings extends RealmObject{

    @PrimaryKey
    private int primaryKet;
    @SerializedName("siteNameAr")
    @Expose
    private String siteNameAr;
    @SerializedName("siteNameEn")
    @Expose
    private String siteNameEn;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("adminEmail")
    @Expose
    private String adminEmail;
    @SerializedName("socialInstagram")
    @Expose
    private String socialInstagram;
    @SerializedName("socialFacebook")
    @Expose
    private String socialFacebook;
    @SerializedName("socialTwitter")
    @Expose
    private String socialTwitter;
    @SerializedName("socialGoogle")
    @Expose
    private String socialGoogle;
    @SerializedName("socialLinkedin")
    @Expose
    private String socialLinkedin;
    @SerializedName("socialYoutube")
    @Expose
    private String socialYoutube;
    @SerializedName("facebookApiAndroid")
    @Expose
    private String facebookApiAndroid;
    @SerializedName("facebookApiIphone")
    @Expose
    private String facebookApiIphone;
    @SerializedName("googleApiAndroid")
    @Expose
    private String googleApiAndroid;
    @SerializedName("googleApiIphone")
    @Expose
    private String googleApiIphone;
    @SerializedName("firstWelcomeMessage")
    @Expose
    private String firstWelcomeMessage;
    @SerializedName("welcomeMessage")
    @Expose
    private String welcomeMessage;

    @SerializedName("contactUrl")
    @Expose
    private String contactUrl;

    @SerializedName("faqUrl")
    @Expose
    private String faqUrl;

    /**
     * 
     * @return
     *     The siteNameAr
     */
    public String getSiteNameAr() {
        return siteNameAr;
    }

    /**
     * 
     * @param siteNameAr
     *     The siteNameAr
     */
    public void setSiteNameAr(String siteNameAr) {
        this.siteNameAr = siteNameAr;
    }

    /**
     * 
     * @return
     *     The siteNameEn
     */
    public String getSiteNameEn() {
        return siteNameEn;
    }

    /**
     * 
     * @param siteNameEn
     *     The siteNameEn
     */
    public void setSiteNameEn(String siteNameEn) {
        this.siteNameEn = siteNameEn;
    }

    /**
     * 
     * @return
     *     The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 
     * @param phone
     *     The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 
     * @return
     *     The mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 
     * @param mobile
     *     The mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 
     * @return
     *     The adminEmail
     */
    public String getAdminEmail() {
        return adminEmail;
    }

    /**
     * 
     * @param adminEmail
     *     The adminEmail
     */
    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    /**
     * 
     * @return
     *     The socialInstagram
     */
    public String getSocialInstagram() {
        return socialInstagram;
    }

    /**
     * 
     * @param socialInstagram
     *     The socialInstagram
     */
    public void setSocialInstagram(String socialInstagram) {
        this.socialInstagram = socialInstagram;
    }

    /**
     * 
     * @return
     *     The socialFacebook
     */
    public String getSocialFacebook() {
        return socialFacebook;
    }

    /**
     * 
     * @param socialFacebook
     *     The socialFacebook
     */
    public void setSocialFacebook(String socialFacebook) {
        this.socialFacebook = socialFacebook;
    }

    /**
     * 
     * @return
     *     The socialTwitter
     */
    public String getSocialTwitter() {
        return socialTwitter;
    }

    /**
     * 
     * @param socialTwitter
     *     The socialTwitter
     */
    public void setSocialTwitter(String socialTwitter) {
        this.socialTwitter = socialTwitter;
    }

    /**
     * 
     * @return
     *     The socialGoogle
     */
    public String getSocialGoogle() {
        return socialGoogle;
    }

    /**
     * 
     * @param socialGoogle
     *     The socialGoogle
     */
    public void setSocialGoogle(String socialGoogle) {
        this.socialGoogle = socialGoogle;
    }

    /**
     * 
     * @return
     *     The socialLinkedin
     */
    public String getSocialLinkedin() {
        return socialLinkedin;
    }

    /**
     * 
     * @param socialLinkedin
     *     The socialLinkedin
     */
    public void setSocialLinkedin(String socialLinkedin) {
        this.socialLinkedin = socialLinkedin;
    }

    /**
     * 
     * @return
     *     The socialYoutube
     */
    public String getSocialYoutube() {
        return socialYoutube;
    }

    /**
     * 
     * @param socialYoutube
     *     The socialYoutube
     */
    public void setSocialYoutube(String socialYoutube) {
        this.socialYoutube = socialYoutube;
    }

    /**
     * 
     * @return
     *     The facebookApiAndroid
     */
    public String getFacebookApiAndroid() {
        return facebookApiAndroid;
    }

    /**
     * 
     * @param facebookApiAndroid
     *     The facebookApiAndroid
     */
    public void setFacebookApiAndroid(String facebookApiAndroid) {
        this.facebookApiAndroid = facebookApiAndroid;
    }

    /**
     * 
     * @return
     *     The facebookApiIphone
     */
    public String getFacebookApiIphone() {
        return facebookApiIphone;
    }

    /**
     * 
     * @param facebookApiIphone
     *     The facebookApiIphone
     */
    public void setFacebookApiIphone(String facebookApiIphone) {
        this.facebookApiIphone = facebookApiIphone;
    }

    /**
     * 
     * @return
     *     The googleApiAndroid
     */
    public String getGoogleApiAndroid() {
        return googleApiAndroid;
    }

    /**
     * 
     * @param googleApiAndroid
     *     The googleApiAndroid
     */
    public void setGoogleApiAndroid(String googleApiAndroid) {
        this.googleApiAndroid = googleApiAndroid;
    }

    /**
     * 
     * @return
     *     The googleApiIphone
     */
    public String getGoogleApiIphone() {
        return googleApiIphone;
    }

    /**
     * 
     * @param googleApiIphone
     *     The googleApiIphone
     */
    public void setGoogleApiIphone(String googleApiIphone) {
        this.googleApiIphone = googleApiIphone;
    }

    /**
     * 
     * @return
     *     The firstWelcomeMessage
     */
    public String getFirstWelcomeMessage() {
        return firstWelcomeMessage;
    }

    /**
     * 
     * @param firstWelcomeMessage
     *     The firstWelcomeMessage
     */
    public void setFirstWelcomeMessage(String firstWelcomeMessage) {
        this.firstWelcomeMessage = firstWelcomeMessage;
    }

    /**
     * 
     * @return
     *     The welcomeMessage
     */
    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    /**
     * 
     * @param welcomeMessage
     *     The welcomeMessage
     */
    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public int getPrimaryKet() {
        return primaryKet;
    }

    public void setPrimaryKet(int primaryKet) {
        this.primaryKet = primaryKet;
    }

    public String getFaqUrl() {
        return faqUrl;
    }

    public void setFaqUrl(String faqUrl) {
        this.faqUrl = faqUrl;
    }

    public String getContactUrl() {
        return contactUrl;
    }

    public void setContactUrl(String contactUrl) {
        this.contactUrl = contactUrl;
    }
}
