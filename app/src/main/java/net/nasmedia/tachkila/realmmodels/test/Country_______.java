
package net.nasmedia.tachkila.realmmodels.test;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Country_______ extends RealmObject{
@PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("titleAr")
    @Expose
    private String titleAr;
    @SerializedName("titleEn")
    @Expose
    private String titleEn;
    @SerializedName("alpha_2")
    @Expose
    private String alpha_2;
    @SerializedName("numcode")
    @Expose
    private int numcode;
    @SerializedName("phonecode")
    @Expose
    private int phonecode;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The titleAr
     */
    public String getTitleAr() {
        return titleAr;
    }

    /**
     * 
     * @param titleAr
     *     The titleAr
     */
    public void setTitleAr(String titleAr) {
        this.titleAr = titleAr;
    }

    /**
     * 
     * @return
     *     The titleEn
     */
    public String getTitleEn() {
        return titleEn;
    }

    /**
     * 
     * @param titleEn
     *     The titleEn
     */
    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    /**
     * 
     * @return
     *     The alpha_2
     */
    public String getAlpha_2() {
        return alpha_2;
    }

    /**
     * 
     * @param alpha_2
     *     The alpha_2
     */
    public void setAlpha_2(String alpha_2) {
        this.alpha_2 = alpha_2;
    }

    /**
     * 
     * @return
     *     The numcode
     */
    public int getNumcode() {
        return numcode;
    }

    /**
     * 
     * @param numcode
     *     The numcode
     */
    public void setNumcode(int numcode) {
        this.numcode = numcode;
    }

    /**
     * 
     * @return
     *     The phonecode
     */
    public int getPhonecode() {
        return phonecode;
    }

    /**
     * 
     * @param phonecode
     *     The phonecode
     */
    public void setPhonecode(int phonecode) {
        this.phonecode = phonecode;
    }

}
