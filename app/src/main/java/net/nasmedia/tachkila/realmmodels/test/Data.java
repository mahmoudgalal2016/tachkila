
package net.nasmedia.tachkila.realmmodels.test;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Data {

    @SerializedName("signed_in")
    @Expose
    private boolean signed_in;
    @SerializedName("user")
    @Expose
    private User user;

    /**
     * 
     * @return
     *     The signed_in
     */
    public boolean isSigned_in() {
        return signed_in;
    }

    /**
     * 
     * @param signed_in
     *     The signed_in
     */
    public void setSigned_in(boolean signed_in) {
        this.signed_in = signed_in;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

}
