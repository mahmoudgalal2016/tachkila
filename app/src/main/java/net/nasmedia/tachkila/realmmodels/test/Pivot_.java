
package net.nasmedia.tachkila.realmmodels.test;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Pivot_ {

    @SerializedName("group_id")
    @Expose
    private int group_id;
    @SerializedName("user_id")
    @Expose
    private int user_id;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("updated_at")
    @Expose
    private String updated_at;

    /**
     * 
     * @return
     *     The group_id
     */
    public int getGroup_id() {
        return group_id;
    }

    /**
     * 
     * @param group_id
     *     The group_id
     */
    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    /**
     * 
     * @return
     *     The user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * 
     * @param user_id
     *     The user_id
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * 
     * @return
     *     The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     * 
     * @param created_at
     *     The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     * 
     * @return
     *     The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * 
     * @param updated_at
     *     The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

}
