
package net.nasmedia.tachkila.realmmodels.test;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Pivot__ extends RealmObject{
@PrimaryKey
    @SerializedName("user_id")
    @Expose
    private int user_id;
    @SerializedName("friend_id")
    @Expose
    private int friend_id;
    @SerializedName("blocked")
    @Expose
    private int blocked;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("updated_at")
    @Expose
    private String updated_at;

    /**
     * 
     * @return
     *     The user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * 
     * @param user_id
     *     The user_id
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * 
     * @return
     *     The friend_id
     */
    public int getFriend_id() {
        return friend_id;
    }

    /**
     * 
     * @param friend_id
     *     The friend_id
     */
    public void setFriend_id(int friend_id) {
        this.friend_id = friend_id;
    }

    /**
     * 
     * @return
     *     The blocked
     */
    public int getBlocked() {
        return blocked;
    }

    /**
     * 
     * @param blocked
     *     The blocked
     */
    public void setBlocked(int blocked) {
        this.blocked = blocked;
    }

    /**
     * 
     * @return
     *     The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     * 
     * @param created_at
     *     The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     * 
     * @return
     *     The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * 
     * @param updated_at
     *     The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

}
