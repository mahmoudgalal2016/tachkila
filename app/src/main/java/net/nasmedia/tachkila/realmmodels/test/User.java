
package net.nasmedia.tachkila.realmmodels.test;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.nasmedia.tachkila.realmmodels.CurrentRequest;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class User extends RealmObject {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("country_id")
    @Expose
    private int country_id;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("losses")
    @Expose
    private int losses;
    @SerializedName("win")
    @Expose
    private int win;
    @SerializedName("points")
    @Expose
    private int points;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("position_id")
    @Expose
    private int position_id;
    @SerializedName("favorite_number")
    @Expose
    private int favorite_number;
    @SerializedName("perfect_foot")
    @Expose
    private String perfect_foot;
    @SerializedName("isAdmin")
    @Expose
    private int isAdmin;
    @SerializedName("role_id")
    @Expose
    private int role_id;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("deviceToken")
    @Expose
    private String deviceToken;
    @SerializedName("remember_token")
    @Expose
    private String remember_token;
    @SerializedName("active")
    @Expose
    private int active;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("updated_at")
    @Expose
    private String updated_at;
    @SerializedName("people_you_may_now")
    @Expose
    private RealmList<My_current_friend> people_you_may_now = new RealmList<My_current_friend>();
    @SerializedName("groups")
    @Expose
    private RealmList<Group> groups = new RealmList<Group>();
    @SerializedName("notifications")
    @Expose
    private RealmList<Notification> notifications = new RealmList<Notification>();
    @SerializedName("my_current_friends")
    @Expose
    private RealmList<My_current_friend> my_current_friends = new RealmList<My_current_friend>();
    @SerializedName("my_blocked_friends")
    @Expose
    private RealmList<My_current_friend> my_blocked_friends = new RealmList<My_current_friend>();
    @SerializedName("current_requests")
    @Expose
    private RealmList<My_current_friend> current_requests = new RealmList<My_current_friend>();
    @SerializedName("country")
    @Expose
    private Country________ country;

    @SerializedName("country_code")
    @Expose
    private String countryCode;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday The birthday
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar The avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * @return The country_id
     */
    public int getCountry_id() {
        return country_id;
    }

    /**
     * @param country_id The country_id
     */
    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    /**
     * @return The sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * @param sex The sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * @return The losses
     */
    public int getLosses() {
        return losses;
    }

    /**
     * @param losses The losses
     */
    public void setLosses(int losses) {
        this.losses = losses;
    }

    /**
     * @return The win
     */
    public int getWin() {
        return win;
    }

    /**
     * @param win The win
     */
    public void setWin(int win) {
        this.win = win;
    }

    /**
     * @return The points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points The points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * @return The height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height The height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return The position_id
     */
    public int getPosition_id() {
        return position_id;
    }

    /**
     * @param position_id The position_id
     */
    public void setPosition_id(int position_id) {
        this.position_id = position_id;
    }

    /**
     * @return The favorite_number
     */
    public int getFavorite_number() {
        return favorite_number;
    }

    /**
     * @param favorite_number The favorite_number
     */
    public void setFavorite_number(int favorite_number) {
        this.favorite_number = favorite_number;
    }

    /**
     * @return The perfect_foot
     */
    public String getPerfect_foot() {
        return perfect_foot;
    }

    /**
     * @param perfect_foot The perfect_foot
     */
    public void setPerfect_foot(String perfect_foot) {
        this.perfect_foot = perfect_foot;
    }

    /**
     * @return The isAdmin
     */
    public int getIsAdmin() {
        return isAdmin;
    }

    /**
     * @param isAdmin The isAdmin
     */
    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * @return The role_id
     */
    public int getRole_id() {
        return role_id;
    }

    /**
     * @param role_id The role_id
     */
    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    /**
     * @return The deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType The deviceType
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return The deviceToken
     */
    public String getDeviceToken() {
        return deviceToken;
    }

    /**
     * @param deviceToken The deviceToken
     */
    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    /**
     * @return The remember_token
     */
    public String getRemember_token() {
        return remember_token;
    }

    /**
     * @param remember_token The remember_token
     */
    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    /**
     * @return The active
     */
    public int getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * @return The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     * @param created_at The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     * @return The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * @param updated_at The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     * @return The people_you_may_now
     */
    public List<My_current_friend> getPeople_you_may_now() {
        return people_you_may_now;
    }

    /**
     * @param people_you_may_now The people_you_may_now
     */
    public void setPeople_you_may_now(RealmList<My_current_friend> people_you_may_now) {
        this.people_you_may_now = people_you_may_now;
    }

    /**
     * @return The groups
     */
    public List<Group> getGroups() {
        return groups;
    }

    /**
     * @param groups The groups
     */
    public void setGroups(RealmList<Group> groups) {
        this.groups = groups;
    }

    /**
     * @return The notifications
     */
    public List<Notification> getNotifications() {
        return notifications;
    }

    /**
     * @param notifications The notifications
     */
    public void setNotifications(RealmList<Notification> notifications) {
        this.notifications = notifications;
    }

    /**
     * @return The my_current_friends
     */
    public List<My_current_friend> getMy_current_friends() {
        return my_current_friends;
    }

    /**
     * @param my_current_friends The my_current_friends
     */
    public void setMy_current_friends(RealmList<My_current_friend> my_current_friends) {
        this.my_current_friends = my_current_friends;
    }

    /**
     * @return The my_blocked_friends
     */
    public List<My_current_friend> getMy_blocked_friends() {
        return my_blocked_friends;
    }

    /**
     * @param my_blocked_friends The my_blocked_friends
     */
    public void setMy_blocked_friends(RealmList<My_current_friend> my_blocked_friends) {
        this.my_blocked_friends = my_blocked_friends;
    }

    /**
     * @return The current_requests
     */
    public List<My_current_friend> getCurrent_requests() {
        return current_requests;
    }

    /**
     * @param current_requests The current_requests
     */
    public void setCurrent_requests(RealmList<My_current_friend> current_requests) {
        this.current_requests = current_requests;
    }

    /**
     * @return The country
     */
    public Country________ getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    public void setCountry(Country________ country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
