package net.nasmedia.tachkila.services;

import net.nasmedia.tachkila.GameInviteModel;
import net.nasmedia.tachkila.SyncContactsModel;
import net.nasmedia.tachkila.TestModel;
import net.nasmedia.tachkila.services.response.FinishedGameResponse;
import net.nasmedia.tachkila.services.response.SyncContactsResponse;
import net.nasmedia.tachkila.services.response.creategames.CreateGameResponse;
import net.nasmedia.tachkila.services.response.delete_game.DeleteGameResponse;
import net.nasmedia.tachkila.services.response.groups.CreateGroupResponse;
import net.nasmedia.tachkila.services.response.groups.ViewGroupResponse;
import net.nasmedia.tachkila.services.response.login.LoginResponse;
import net.nasmedia.tachkila.services.response.notifications.NotificationResponse;
import net.nasmedia.tachkila.services.response.register.RegisterResponse;
import net.nasmedia.tachkila.services.response.searchuser.SearchUserNameResponse;
import net.nasmedia.tachkila.services.response.settings.SettingsResponse;
import net.nasmedia.tachkila.services.response.viewgame.ViewGameResponse;
import net.nasmedia.tachkila.services.response.viewplayer.ViewPlayerResponse;
import net.nasmedia.tachkila.utils.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by mahmoudgalal on 5/18/16.
 */
public class ApiHandler {

    private static final String TAG = ApiHandler.class.getSimpleName();
    private static ApiHandler mInstance;
    private Services mServices;
    private Retrofit mRetrofit;

    private ApiHandler() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mServices = mRetrofit.create(Services.class);
    }

    public static ApiHandler getInstance() {
        if (mInstance == null) {
            mInstance = new ApiHandler();
        }

        return mInstance;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public Services getServices() {
        return mServices;
    }

    public interface Services {

        @GET("settings")
        Call<SettingsResponse> getSettings();


        @GET("settings")
        Call<SettingsResponse> getSettingsByUserId(@Query("userID") int userId);

        @Multipart
        @POST("users/register")
        Call<RegisterResponse> registerUser(@Part("avatar\"; filename=\"image.jpg") RequestBody avatar,
                                            @Part("username") RequestBody userName,
                                            @Part("password") RequestBody password,
                                            @Part("name") RequestBody name,
                                            @Part("email") RequestBody email,
                                            @Part("phone") RequestBody phone,
                                            @Part("country_id") RequestBody countryId,
                                            @Part("sex") RequestBody sex,
                                            @Part("perfect_foot") RequestBody perfectFoot,
                                            @Part("favorite_number") RequestBody favorite_number,
                                            @Part("position_id") RequestBody positionId,
                                            @Part("height") RequestBody height,
                                            @Part("birthday") RequestBody birthday,
                                            @Part("deviceType") RequestBody deviceType,
                                            @Part("deviceToken") RequestBody deviceToken,
                                            @Part("social_type") RequestBody socialType,
                                            @Part("social_key") RequestBody socialKey);

        @Multipart
        @POST("users/update")
        Call<RegisterResponse> updateUser(@Part("avatar\"; filename=\"image.jpg") RequestBody avatar,
                                          @Part("username") RequestBody userName,
                                          @Part("password") RequestBody password,
                                          @Part("name") RequestBody name,
                                          @Part("email") RequestBody email,
                                          @Part("phone") RequestBody phone,
                                          @Part("country_id") RequestBody countryId,
                                          @Part("sex") RequestBody sex,
                                          @Part("perfect_foot") RequestBody perfectFoot,
                                          @Part("favorite_number") RequestBody favorite_number,
                                          @Part("position_id") RequestBody positionId,
                                          @Part("height") RequestBody height,
                                          @Part("birthday") RequestBody birthday,
                                          @Part("deviceType") RequestBody deviceType,
                                          @Part("userID") RequestBody userId);


        @POST("users/login")
        @FormUrlEncoded
        Call<LoginResponse> loginBySocial(@Field("social_type") String socialType, @Field("social_key") String socialKey, @Field("deviceType") int deviceType, @Field("deviceToken") String deviceToken);

        @POST("users/login")
        @FormUrlEncoded
        Call<LoginResponse> loginByUsername(@Field("username") String username, @Field("password") String password, @Field("deviceType") int deviceType, @Field("deviceToken") String deviceToken);

        @GET("users/notifications/{id}")
        Call<NotificationResponse> getNotificationByUser(@Path("id") int id);

        @Multipart
        @POST("groups/create")
        Call<CreateGroupResponse> createGroup(@Part("avatar\"; filename=\"image.jpg") RequestBody avatar,
                                              @Part("name") RequestBody groupName,
                                              @Part("admin_id") RequestBody userId);

        @POST("games/join")
        @FormUrlEncoded
        Call<ViewGameResponse> joinGame(@Field("team") int team,
                                        @Field("gameID") int gameId,
                                        @Field("playerID") int userId,
                                        @Field("position_x") int positionX,
                                        @Field("position_y") int positionY);

        @POST("games/invite")
        Call<ViewGameResponse> gameInvite(@Body GameInviteModel model);

        @POST("users/sync-contacts")
        Call<SyncContactsResponse> syncContact(@Body SyncContactsModel model);

        @POST("groups/invite")
        Call<CreateGroupResponse> inviteUsersToGroup(@Body TestModel model);

        @POST("games/create")
        @FormUrlEncoded
        Call<CreateGameResponse> createGame(@Field("time_of_game") String timeOfGame, @Field("location") String locationName,
                                            @Field("map_lat") double lat, @Field("map_lng") double lng,
                                            @Field("group_id") int groupId, @Field("captain_id") int captainId,
                                            @Field("type_id") int typeId, @Field("team1_color_id") int teamAColorId,
                                            @Field("team2_color_id") int teamBColorId, @Field("notes") String notes);

        @POST("users/search")
        @FormUrlEncoded
        Call<SearchUserNameResponse> searchByUsername(@Field("username") String username, @Field("userID") String userId);

        @GET("groups/view/{groupId}")
        Call<ViewGroupResponse> viewGroup(@Path("groupId") int groupId);

        @GET("users/un-friend/{currentId}/{userId}")
        Call<ViewGroupResponse> unFriend(@Path("currentId") int currentId, @Path("userId") int userId);

        @GET("users/block-friend/{currentId}/{userId}")
        Call<ViewGroupResponse> block(@Path("currentId") int currentId, @Path("userId") int userId);

        @GET("users/make-friend/{currentId}/{userId}")
        Call<ViewGroupResponse> makeFriend(@Path("currentId") int currentId, @Path("userId") int userId);

        @GET("games/view/{gameId}")
        Call<ViewGameResponse> viewGame(@Path("gameId") int gameId);

        @GET("users/view/{playerId}")
        Call<ViewPlayerResponse> viewPlayer(@Path("playerId") int playerId, @Query("userId") int userId);

        @GET("games/not-real-delete/{gameId}")
        Call<DeleteGameResponse> deleteGame(@Path("gameId") int gameId);

        @POST("groups/leave")
        @FormUrlEncoded
        Call<ViewGroupResponse> leaveGroup(@Field("groupID") int groupId, @Field("userID") int userId);

        @POST("games/un-join")
        @FormUrlEncoded
        Call<ViewGameResponse> leaveGame(@Field("gameID") int gameId, @Field("playerID") int userId);

        @POST("games/update")
        @FormUrlEncoded
        Call<FinishedGameResponse> updateGameScore(@Field("finished") int finished, @Field("gameID") int gameId, @Field("team1_score") int team1Score, @Field("team2_score") int team2Score);

        @POST("users/reset")
        @FormUrlEncoded
        Call<FinishedGameResponse> resetPass(@Field("email") String email);

        @Multipart
        @POST("groups/edit")
        Call<CreateGroupResponse> editGroup(@Part("avatar\"; filename=\"image.jpg") RequestBody avatar,
                                              @Part("name") RequestBody groupName, @Part("id") RequestBody groupid);


    }
}
