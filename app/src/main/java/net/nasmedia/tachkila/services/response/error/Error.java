
package net.nasmedia.tachkila.services.response.error;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Error {

    @SerializedName("success")
    @Expose
    private int success;
    @SerializedName("errorType")
    @Expose
    private int errorType;
    @SerializedName("error")
    @Expose
    private String error;

    /**
     * 
     * @return
     *     The success
     */
    public int getSuccess() {
        return success;
    }

    /**
     * 
     * @param success
     *     The success
     */
    public void setSuccess(int success) {
        this.success = success;
    }

    /**
     * 
     * @return
     *     The errorType
     */
    public int getErrorType() {
        return errorType;
    }

    /**
     * 
     * @param errorType
     *     The errorType
     */
    public void setErrorType(int errorType) {
        this.errorType = errorType;
    }

    /**
     * 
     * @return
     *     The error
     */
    public String getError() {
        return error;
    }

    /**
     * 
     * @param error
     *     The error
     */
    public void setError(String error) {
        this.error = error;
    }

}
