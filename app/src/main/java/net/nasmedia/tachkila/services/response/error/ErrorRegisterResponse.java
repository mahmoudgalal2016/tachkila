
package net.nasmedia.tachkila.services.response.error;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorRegisterResponse {

    @SerializedName("result")
    @Expose
    private ErrorRegister result;

    /**
     * 
     * @return
     *     The result
     */
    public ErrorRegister getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(ErrorRegister result) {
        this.result = result;
    }

}
