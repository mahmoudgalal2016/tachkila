package net.nasmedia.tachkila.services.response.groups;


import net.nasmedia.tachkila.realmmodels.test.Group;

/**
 * Created by Sherif on 8/19/2016.
 */
public class CreateGroupResult {
    int success;
    Group data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public Group getData() {
        return data;
    }

    public void setData(Group data) {
        this.data = data;
    }
}
