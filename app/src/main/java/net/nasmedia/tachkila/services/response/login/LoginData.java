package net.nasmedia.tachkila.services.response.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.nasmedia.tachkila.realmmodels.test.User;

/**
 * Created by Mahmoud Galal on 7/1/2016.
 */
public class LoginData {

    @SerializedName("signed_in")
    @Expose
    boolean signedIn;

    @SerializedName("user")
    @Expose
    User user;

    public boolean isSignedIn() {
        return signedIn;
    }

    public void setSignedIn(boolean signedIn) {
        this.signedIn = signedIn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
