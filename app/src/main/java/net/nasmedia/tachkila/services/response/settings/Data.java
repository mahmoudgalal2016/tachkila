
package net.nasmedia.tachkila.services.response.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.nasmedia.tachkila.realmmodels.ActiveGame;
import net.nasmedia.tachkila.realmmodels.Color;
import net.nasmedia.tachkila.realmmodels.GameType;
import net.nasmedia.tachkila.realmmodels.Paths;
import net.nasmedia.tachkila.realmmodels.Position;
import net.nasmedia.tachkila.realmmodels.Settings;
import net.nasmedia.tachkila.realmmodels.test.Country;
import net.nasmedia.tachkila.realmmodels.test.User;

import java.util.ArrayList;
import java.util.List;


public class Data {

    @SerializedName("settings")
    @Expose
    private Settings settings;
    @SerializedName("paths")
    @Expose
    private Paths paths;
    @SerializedName("positions")
    @Expose
    private List<Position> positions = new ArrayList<Position>();
    @SerializedName("colors")
    @Expose
    private List<Color> colors = new ArrayList<Color>();
    @SerializedName("game_types")
    @Expose
    private List<GameType> gameTypes = new ArrayList<GameType>();
    @SerializedName("countries")
    @Expose
    private List<Country> countries = new ArrayList<Country>();
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("active_games")
    @Expose
    private List<ActiveGame> activeGames = new ArrayList<ActiveGame>();

    /**
     * 
     * @return
     *     The settings
     */
    public Settings getSettings() {
        return settings;
    }

    /**
     * 
     * @param settings
     *     The settings
     */
    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * 
     * @return
     *     The paths
     */
    public Paths getPaths() {
        return paths;
    }

    /**
     * 
     * @param paths
     *     The paths
     */
    public void setPaths(Paths paths) {
        this.paths = paths;
    }

    /**
     * 
     * @return
     *     The positions
     */
    public List<Position> getPositions() {
        return positions;
    }

    /**
     * 
     * @param positions
     *     The positions
     */
    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    /**
     * 
     * @return
     *     The colors
     */
    public List<Color> getColors() {
        return colors;
    }

    /**
     * 
     * @param colors
     *     The colors
     */
    public void setColors(List<Color> colors) {
        this.colors = colors;
    }

    /**
     * 
     * @return
     *     The gameTypes
     */
    public List<GameType> getGameTypes() {
        return gameTypes;
    }

    /**
     * 
     * @param gameTypes
     *     The game_types
     */
    public void setGameTypes(List<GameType> gameTypes) {
        this.gameTypes = gameTypes;
    }

    /**
     * 
     * @return
     *     The countries
     */
    public List<Country> getCountries() {
        return countries;
    }

    /**
     * 
     * @param countries
     *     The countries
     */
    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }



    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * 
     * @return
     *     The activeGames
     */
    public List<ActiveGame> getActiveGames() {
        return activeGames;
    }

    /**
     * 
     * @param activeGames
     *     The active_games
     */
    public void setActiveGames(List<ActiveGame> activeGames) {
        this.activeGames = activeGames;
    }

}
