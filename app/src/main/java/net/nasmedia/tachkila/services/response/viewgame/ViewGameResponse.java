package net.nasmedia.tachkila.services.response.viewgame;

/**
 * Created by mahmoudgalal on 11/4/16.
 */

public class ViewGameResponse {

    ViewGameData result;

    public ViewGameData getResult() {
        return result;
    }

    public void setResult(ViewGameData result) {
        this.result = result;
    }
}
