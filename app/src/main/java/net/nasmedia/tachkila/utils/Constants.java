package net.nasmedia.tachkila.utils;


import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 * Created by Mahmoud Galal on 4/3/2016.
 */
public class Constants {

    public static final String BASE_URL = "http://app.tachkila.com/api/";

    public static final String PIC_GROUPS_URL = "http://app.tachkila.com/" + "media/groups/";
    public static final String PIC_AVATAR_URL = "http://app.tachkila.com/" + "media/avatars/";


    public static final String ACTION_SHOW_REGISTER_ACTIVITY = "ACTION_SHOW_REGISTER_ACTIVITY";
    public static final String APP_PREF_NAME = "pref_tachkela";
    public static final String USER_ID_PREF_KEY = "pref_userId";
    public static final String PASSED_LOGIN_KEY = "pref_login";


//    public static final String ACTION_SHOW_LOGIN_ACTIVITY = "ACTION_SHOW_LOGIN_ACTIVITY";
public static final String ACTION_SHOW_ADD_NEW_MATCH_FRAGMENT = "ACTION_SHOW_ADD_NEW_MATCH";
    public static final String ACTION_SHOW_ADD_NEW_Group_FRAGMENT = "ACTION_SHOW_ADD_NEW_Group";
    public static final String ACTION_SHOW_GROUP_DETAILS_FRAGMENT = "ACTION_SHOW_GROUP_DETAILS_FRAGMENT";
    public static final String ACTION_SHOW_ADD_PLAYERS_FRAGMENT = "ACTION_SHOW_ADD_PLAYERS_FRAGMENT";
    public static final String ACTION_SHOW_MATCH_HISTORY_FRAGMENT = "ACTION_SHOW_MATCH_HISTORY_FRAGMENT";
    public static final String ACTION_SHOW_OTHER_PLAYER_FRAGMENT = "ACTION_SHOW_OTHER_PLAYER_FRAGMENT";
    public static final String ACTION_SHOW_NOTIFICATION_FRAGMENT = "ACTION_SHOW_NOTIFICATION_FRAGMENT";
    public static final String ACTION_SHOW_WEBVIEW_FRAGMENT = "ACTION_SHOW_WEBVIEW_FRAGMENT";
    public static final String ACTION_SHOW_GAME_DETAILS_FRAGMENT = "ACTION_SHOW_GAME_DETAILS_FRAGMENT";
    public static final String ACTION_SHOW_INVITE_PLAYERS_FRAGMENT = "ACTION_SHOW_INVITE_PLAYERS_FRAGMENT";
    public static final String ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT = "ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT";
    public static final String ACTION_SHOW_ADDRESS_BOOK_FRAGMENT = "ACTION_SHOW_ADDRESS_BOOK_FRAGMENT";
    public static final String ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT = "ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT";
    public static final String ACTION_SHOW_SETTINGS_FRAGMENT = "ACTION_SHOW_SETTINGS_FRAGMENT";
    public static final String ACTION_SHOW_EDIT_GROUP_FRAGMENT = "ACTION_SHOW_EDIT_GROUP_FRAGMENT";


    public static class FontCache {

        private static Hashtable<String, Typeface> fontCache = new Hashtable<>();

        public static Typeface get(String name, Context context) {
            Typeface tf = fontCache.get(name);
            if (tf == null) {
                try {
                    tf = Typeface.createFromAsset(context.getAssets(), name);
                } catch (Exception e) {
                    return null;
                }
                fontCache.put(name, tf);
            }
            return tf;
        }
    }

}
