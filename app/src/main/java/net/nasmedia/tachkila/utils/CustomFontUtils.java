package net.nasmedia.tachkila.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;

import net.nasmedia.tachkila.R;


/**
 * Created by Mahmoud Galal on 2/19/2016.
 */
public class CustomFontUtils {

    // region Constants
    public static final int LIGHT = 0;
    public static final int MEDIUM = 1;
    // endregion

    public static void applyButtonCustomFont(Button button,int font, Context context) {
        Typeface customFont = getTypeface(context, font);
        button.setTypeface(customFont);
    }

    public static void applyEditTextCustomFont(EditText editText, int font,  Context context) {
        Typeface customFont = getTypeface(context, font);
        editText.setTypeface(customFont);
    }

    public static void applyCheckBoxCustomFont(CheckedTextView checkBox, int font, Context context) {
        Typeface customFont = getTypeface(context, font);
        checkBox.setTypeface(customFont);
    }

    private static Typeface getTypeface(Context context, int font) {
        switch (font) {
            case LIGHT:
                return Constants.FontCache.get("fonts/"+context.getString(R.string.font_light), context);
            case MEDIUM:
                return Constants.FontCache.get("fonts/"+context.getString(R.string.font_medium), context);
            default:
                // no matching font found
                // return null so Android just uses the standard font (Roboto)
                return null;
        }
    }

}
