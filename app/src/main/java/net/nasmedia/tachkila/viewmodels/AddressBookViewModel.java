package net.nasmedia.tachkila.viewmodels;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import net.nasmedia.tachkila.ContactModel;
import net.nasmedia.tachkila.SyncContactsModel;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.SyncContactsResponse;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 27/11/2016.
 */

public class AddressBookViewModel {

    Context mContext;
    ApiHandler mApiHandler;
    OnDataLoaded mListener;
    NewMatchViewModel.OnError mError;
    List<User> mUsers;

    public AddressBookViewModel(Context context, OnDataLoaded listener, NewMatchViewModel.OnError error) {
        mContext = context;
        mListener = listener;
        mError = error;
        mApiHandler = ApiHandler.getInstance();
    }

    public List<ContactModel> displayContacts() {

        List<ContactModel> contactsList = new ArrayList<>();

        ContentResolver cr = mContext.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        ContactModel contactModel = new ContactModel();
                        contactModel.setName(name);
                        contactModel.setPhone(phoneNo);
                        contactsList.add(contactModel);
                    }
                    pCur.close();
                }
            }
        }
        syncContact(contactsList);
        return contactsList;
    }

    private void syncContact(List<ContactModel> contactModelList) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            SyncContactsModel syncContactsModel = new SyncContactsModel();
            syncContactsModel.setUserID(Utils.getSavedUserIdInSharedPref(mContext));
            ArrayList<String> phones = new ArrayList<>();
            for (ContactModel model : contactModelList) {
                phones.add(model.getPhone().replace(" ", ""));
            }
            syncContactsModel.setPhones(phones);
            mApiHandler.getServices().syncContact(syncContactsModel).enqueue(new Callback<SyncContactsResponse>() {
                @Override
                public void onResponse(Call<SyncContactsResponse> call, Response<SyncContactsResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getResult().getData().size() > 0) {
                            mUsers = response.body().getResult().getData();
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        } else {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_DATA);
                        }
                    } else {
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mError.showError(errorContent);
                        } else {
                            mError.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }
                }

                @Override
                public void onFailure(Call<SyncContactsResponse> call, Throwable t) {
                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public List<User> getUsers(){
        return mUsers;
    }

}
