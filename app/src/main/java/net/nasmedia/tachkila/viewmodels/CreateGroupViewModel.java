package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.TestModel;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.groups.CreateGroupResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 9/11/2016.
 */
public class CreateGroupViewModel {

    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    ApiHandler mApiHandler;
    User mUser;

    public CreateGroupViewModel(Context context, OnDataLoaded listener) {
        this.mContext = context;
        this.mListener = listener;
        mApiHandler = ApiHandler.getInstance();
    }


    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        mUser = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
    }

    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    public void createGroup(String avatarPath, String groupName, final List<My_current_friend> selectedFriends) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);

            RequestBody requestFile = null;
            File avatarFile = null;
            if (avatarPath != null) {
                avatarFile = new File(avatarPath);
            }
            if (avatarFile != null) {
                requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
            }
            RequestBody groupNameBody = RequestBody.create(MediaType.parse("text/plain"), groupName);
            RequestBody adminIdBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mUser.getId()));
            Call<CreateGroupResponse> createGroupResponseCall = mApiHandler.getServices().createGroup(requestFile, groupNameBody, adminIdBody);


            createGroupResponseCall.enqueue(new Callback<CreateGroupResponse>() {
                @Override
                public void onResponse(Call<CreateGroupResponse> call, final Response<CreateGroupResponse> response) {
                    if (response.isSuccessful()) {
                        if (selectedFriends != null && selectedFriends.size() > 0) {
                            TestModel model = new TestModel();
                            model.setGroupId(response.body().getResult().getData().getId());
                            ArrayList<Integer> userIds = new ArrayList<Integer>();
                            for (My_current_friend user : selectedFriends) {
                                userIds.add(user.getId());
                            }
                            model.setUserIds(userIds);
                            model.setMyID(Utils.getSavedUserIdInSharedPref(mContext));
                            Call<CreateGroupResponse> createGroupResponseCall1 = mApiHandler.getServices().inviteUsersToGroup(model);
                            createGroupResponseCall1.enqueue(new Callback<CreateGroupResponse>() {
                                @Override
                                public void onResponse(Call<CreateGroupResponse> call, final Response<CreateGroupResponse> response) {
                                    if (response.isSuccessful()) {
                                        mRealm.executeTransaction(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                mUser.getGroups().add(response.body().getResult().getData());
                                                mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                                            }
                                        });
                                    }
                                }
                                @Override
                                public void onFailure(Call<CreateGroupResponse> call, Throwable t) {
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                                }
                            });
                        } else {
                            mRealm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    Group group = realm.copyToRealmOrUpdate(response.body().getResult().getData());
                                    mUser.getGroups().add(group);
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                                }
                            });
                        }
                    } else {
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                    }
                }

                @Override
                public void onFailure(Call<CreateGroupResponse> call, Throwable t) {
                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public List<My_current_friend> getUserFriends() {
        return mRealm.copyFromRealm(mUser.getMy_current_friends());
    }
}
