package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.TestModel;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.groups.CreateGroupResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 9/11/2016.
 */
public class EditGroupViewModel {

    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    ApiHandler mApiHandler;
    Group mGroup;

    int mGroupId;

    public EditGroupViewModel(Context context, int groupId, OnDataLoaded listener) {
        this.mContext = context;
        this.mListener = listener;
        mGroupId = groupId;
        mApiHandler = ApiHandler.getInstance();
    }


    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        mGroup = mRealm.where(Group.class).equalTo("id", mGroupId).findFirst();
    }

    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    public String getGroupName() {
        return mGroup.getName();
    }

    public String getGroupPhoto() {
        return mGroup.getAvatar();
    }

    public void editGroup(String avatarPath, String groupName) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);

            RequestBody requestFile = null;
            File avatarFile = null;
            if (avatarPath != null) {
                avatarFile = new File(avatarPath);
            }
            if (avatarFile != null) {
                requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
            }
            RequestBody groupNameBody = RequestBody.create(MediaType.parse("text/plain"), groupName);
            RequestBody groupIdBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mGroupId));
            Call<CreateGroupResponse> createGroupResponseCall = mApiHandler.getServices().editGroup(requestFile, groupNameBody, groupIdBody);

            createGroupResponseCall.enqueue(new Callback<CreateGroupResponse>() {
                @Override
                public void onResponse(Call<CreateGroupResponse> call, final Response<CreateGroupResponse> response) {
                    if (response.isSuccessful()) {
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                Group group = realm.copyToRealmOrUpdate(response.body().getResult().getData());
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                            }
                        });
                    } else {
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                    }
                }

                @Override
                public void onFailure(Call<CreateGroupResponse> call, Throwable t) {
                    t.printStackTrace();
                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

}
