package net.nasmedia.tachkila.viewmodels;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import net.nasmedia.tachkila.GameInviteModel;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.ActiveGame;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.Player;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.delete_game.DeleteGameResponse;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.viewgame.ViewGameResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.GamePlayersWithCaptain;
import net.nasmedia.tachkila.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoudgalal on 11/2/16.
 */

public class GameDetailsViewModel {

    private Context mContext;
    private OnDataLoaded mListener;
    private OnDataLoaded mDeleteGameListener;
    private OnDataLoaded mSaveChangesListener;
    private ApiHandler mApiHandler;
    private Call<ViewGameResponse> mViewGameResponseCall;
    private int mGameId;
    private ActiveGame mActiveGame;
    private NewMatchViewModel.OnError mErrorListener;

    public GameDetailsViewModel(Context context, int gameId, OnDataLoaded listener, OnDataLoaded deleteGameListener, OnDataLoaded saveChangesListener, NewMatchViewModel.OnError errorListener) {
        this.mContext = context;
        this.mGameId = gameId;
        this.mListener = listener;
        this.mApiHandler = ApiHandler.getInstance();
        this.mDeleteGameListener = deleteGameListener;
        this.mSaveChangesListener = saveChangesListener;
        this.mErrorListener = errorListener;
    }

    public void onStart() {
        Log.d("Log", "GameId= " + mGameId + " UserId= " + Utils.getSavedUserIdInSharedPref(mContext));
        mViewGameResponseCall = mApiHandler.getServices().viewGame(mGameId);
        mViewGameResponseCall.enqueue(new Callback<ViewGameResponse>() {
            @Override
            public void onResponse(Call<ViewGameResponse> call, Response<ViewGameResponse> response) {
                if (response.isSuccessful()) {
                    mActiveGame = response.body().getResult().getData();
                    mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                } else {
                    ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                    if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                        String errorContent = "";
                        for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                            errorContent += string + "\n";
                        }
                        mErrorListener.showError(errorContent);
                    } else {
                        mErrorListener.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                    }
                }
            }

            @Override
            public void onFailure(Call<ViewGameResponse> call, Throwable t) {
                t.printStackTrace();
                mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
            }
        });
    }

    public void onStop() {
        if (mViewGameResponseCall != null)
            mViewGameResponseCall.cancel();
    }

    public ActiveGame getActiveGame() {
        return mActiveGame;
    }

    public String getGroupName() {
        return mActiveGame.getGroup().getName();
    }

    public String getGameType() {
        return mActiveGame.getType().getName();
    }

    public String getCaptineName() {
        return "Captain: " + mActiveGame.getCaptain().getName();
    }

    public String getDate() throws ParseException {
        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat toFormat = new SimpleDateFormat("EEE, MMM d, yyyy");

        return toFormat.format(fromFormat.parse(mActiveGame.getTimeOfGame()));
    }

    public String getTime() throws ParseException {
        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat toFormat = new SimpleDateFormat("hh:mm a");

        return toFormat.format(fromFormat.parse(mActiveGame.getTimeOfGame()));
    }

    public String getLocation() {
        return mActiveGame.getLocation();
    }

    public int getTeamAColor() {
        return Color.parseColor(mActiveGame.getTeamColor().getCode());
    }

    public int getTeamBColor() {
        return Color.parseColor(mActiveGame.getTeam2Color().getCode());
    }

    public List<GamePlayersWithCaptain> getGamePlayers() {
        List<GamePlayersWithCaptain> gamePlayersWithCaptainList = new ArrayList<>();

        for (Player player : mActiveGame.getPlayers()) {
            GamePlayersWithCaptain gamePlayersWithCaptain1 = new GamePlayersWithCaptain();
            gamePlayersWithCaptain1.setAvatar(player.getUser().getAvatar());
            gamePlayersWithCaptain1.setUserName(player.getUser().getUsername());
            gamePlayersWithCaptain1.setId(player.getUser().getId());
            gamePlayersWithCaptain1.setPositionX(player.getPosition_x());
            gamePlayersWithCaptain1.setPositionY(player.getPosition_y());
            gamePlayersWithCaptainList.add(gamePlayersWithCaptain1);
        }

        return gamePlayersWithCaptainList;
    }

    public boolean isMyPlayerInField() {
        for (Player player : mActiveGame.getPlayers()) {
            if (player.getUser().getId() == Utils.getSavedUserIdInSharedPref(mContext)) {
                return true;
            }
        }

        return false;
    }

    public GamePlayersWithCaptain getMyPlayer() {
        GamePlayersWithCaptain gamePlayersWithCaptain = new GamePlayersWithCaptain();
        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();

        gamePlayersWithCaptain.setAvatar(user.getAvatar());
        gamePlayersWithCaptain.setUserName(user.getUsername());
        gamePlayersWithCaptain.setId(user.getId());
        realm.close();
        return gamePlayersWithCaptain;
    }

    public List<My_current_friend> myFriendsList() {
        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
        List<My_current_friend> my_current_friends = realm.copyFromRealm(user.getMy_current_friends());
        realm.close();
        return my_current_friends;
    }

    public List<Player> getPlayers() {
        return mActiveGame.getPlayers();
    }

    public void invitePlayers(List<My_current_friend> players) {
        if (players != null && players.size() > 0) {
            ArrayList<Integer> playersIds = new ArrayList<>();
            for (My_current_friend player : players) {
                playersIds.add(player.getId());
            }
            GameInviteModel gameInviteModel = new GameInviteModel();
            gameInviteModel.setGameID(mGameId);
            gameInviteModel.setMyID(Utils.getSavedUserIdInSharedPref(mContext));
            gameInviteModel.setUserIds(playersIds);

            mApiHandler.getServices().gameInvite(gameInviteModel).enqueue(new Callback<ViewGameResponse>() {
                @Override
                public void onResponse(Call<ViewGameResponse> call, Response<ViewGameResponse> response) {

                }

                @Override
                public void onFailure(Call<ViewGameResponse> call, Throwable t) {

                }
            });
        }
    }

    public void joinGame(int positionX, int positionY, int team) {
        mApiHandler.getServices().joinGame(team, mGameId, Utils.getSavedUserIdInSharedPref(mContext), positionX, positionY).enqueue(new Callback<ViewGameResponse>() {
            @Override
            public void onResponse(Call<ViewGameResponse> call, Response<ViewGameResponse> response) {
                if (response.isSuccessful()) {

                    mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);

                } else {
                    ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                    if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                        String errorContent = "";
                        for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                            errorContent += string + "\n";
                        }
                        mErrorListener.showError(errorContent);
//                        mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                    }
                }
            }

            @Override
            public void onFailure(Call<ViewGameResponse> call, Throwable t) {
                mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
            }
        });
    }

    public void saveChanges(int positionX, int positionY, int team) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.START);

//            invitePlayers(players);
            if (positionX != 0 && positionY != 0) {
                joinGame(positionX, positionY, team);
            } else {
                leaveGame();
            }

        } else {
            mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public void deleteGame() {

        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.START);
            mApiHandler.getServices().deleteGame(mGameId).enqueue(new Callback<DeleteGameResponse>() {
                @Override
                public void onResponse(Call<DeleteGameResponse> call, Response<DeleteGameResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getResult().getSuccess() == 1) {
                            if (mActiveGame.getCaptain().getId() == Utils.getSavedUserIdInSharedPref(mContext)) {
                                Realm realm = Realm.getDefaultInstance();
                                final ActiveGame activeGame = realm.where(ActiveGame.class).equalTo("id", mActiveGame.getId()).findFirst();
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        activeGame.deleteFromRealm();
                                    }
                                });
                                realm.close();
                            }
                            mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        } else {
                            mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    } else {
                        mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);

                    }
                }

                @Override
                public void onFailure(Call<DeleteGameResponse> call, Throwable t) {
                    mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public void leaveGame() {
        mApiHandler.getServices().leaveGame(mGameId, Utils.getSavedUserIdInSharedPref(mContext)).enqueue(new Callback<ViewGameResponse>() {
            @Override
            public void onResponse(Call<ViewGameResponse> call, Response<ViewGameResponse> response) {
                if (response.isSuccessful()) {
                    mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);

                } else {

                    ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                    if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                        String errorContent = "";
                        for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                            errorContent += string + "\n";
                        }
                        mErrorListener.showError(errorContent);
                    }
                }
            }

            @Override
            public void onFailure(Call<ViewGameResponse> call, Throwable t) {
                    mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);

            }
        });
    }

}
