package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.groups.ViewGroupResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 9/13/2016.
 */
public class GroupDetailsViewModel implements RealmChangeListener<Group> {

    ApiHandler mApiHandler;
    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    NewMatchViewModel.OnError mErrorListener;
    int mGroupId;
    Group mGroup;

    public GroupDetailsViewModel(Context context, int groupId, OnDataLoaded listener, NewMatchViewModel.OnError errorListener) {
        mContext = context;
        mListener = listener;
        mGroupId = groupId;
        mErrorListener = errorListener;
    }

    public void onStart() {
        mApiHandler = ApiHandler.getInstance();
        mRealm = Realm.getDefaultInstance();

        Group group = mRealm.where(Group.class).equalTo("id", mGroupId).findFirstAsync();
        group.addChangeListener(this);

        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mApiHandler.getServices().viewGroup(mGroupId).enqueue(new Callback<ViewGroupResponse>() {
                @Override
                public void onResponse(Call<ViewGroupResponse> call, final Response<ViewGroupResponse> response) {
                    if (response.isSuccessful()) {
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(response.body().getResult().getData());
                            }
                        });
                    }else{
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mErrorListener.showError(errorContent);
                        } else {
                            mErrorListener.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ViewGroupResponse> call, Throwable t) {
                    t.printStackTrace();
                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        }
    }

    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    @Override
    public void onChange(Group element) {
        if (element.isLoaded() && element.isValid()) {
            mGroup = element;
            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
        }
    }

    public Group getGroup() {
        return mGroup;
    }

    public void removeGroup(int groupId){
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                mGroup.deleteFromRealm();
            }
        });

    }
}
