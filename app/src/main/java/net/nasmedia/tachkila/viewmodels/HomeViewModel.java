package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.realmmodels.ActiveGame;

import java.util.List;

import io.realm.Realm;
import io.realm.Sort;

/**
 * Created by Mahmoud Galal on 9/24/2016.
 */

public class HomeViewModel {

    Context mContext;
    Realm mRealm;

    public HomeViewModel(Context context) {
        this.mContext = context;
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
    }

    public void onStop() {
        mRealm.close();
    }

    public List<ActiveGame> getActiveGames() {
        return mRealm.where(ActiveGame.class).findAllSorted("timeOfGame", Sort.ASCENDING);
    }

}
