package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.GameInviteModel;
import net.nasmedia.tachkila.TestModel;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.groups.CreateGroupResponse;
import net.nasmedia.tachkila.services.response.viewgame.ViewGameResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 9/11/2016.
 */
public class InvitePlayersGameViewModel {

    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    ApiHandler mApiHandler;
    NewMatchViewModel.OnError mErrorListener;
    User mUser;
    int mGameId;

    public InvitePlayersGameViewModel(Context context, int gameId, OnDataLoaded listener, NewMatchViewModel.OnError errorListener) {
        this.mContext = context;
        this.mListener = listener;
        mApiHandler = ApiHandler.getInstance();
        mGameId = gameId;
        mErrorListener = errorListener;
    }


    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        mUser = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
    }

    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    public void invitePlayers(List<My_current_friend> players) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            if (players != null && players.size() > 0) {
                mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
                ArrayList<Integer> playersIds = new ArrayList<>();
                for (My_current_friend player : players) {
                    playersIds.add(player.getId());
                }
                GameInviteModel gameInviteModel = new GameInviteModel();
                gameInviteModel.setGameID(mGameId);
                gameInviteModel.setMyID(Utils.getSavedUserIdInSharedPref(mContext));
                gameInviteModel.setUserIds(playersIds);

                mApiHandler.getServices().gameInvite(gameInviteModel).enqueue(new Callback<ViewGameResponse>() {
                    @Override
                    public void onResponse(Call<ViewGameResponse> call, Response<ViewGameResponse> response) {
                        if (response.isSuccessful()) {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        } else {
                            ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                            if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                                String errorContent = "";
                                for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                    errorContent += string + "\n";
                                }
                                mErrorListener.showError(errorContent);
                            } else {
                                mErrorListener.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ViewGameResponse> call, Throwable t) {
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                    }
                });
            }
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public List<My_current_friend> getUserFriends() {
        return mRealm.copyFromRealm(mUser.getMy_current_friends());
    }
}
