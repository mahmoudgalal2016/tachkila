package net.nasmedia.tachkila.viewmodels;

import android.content.Context;
import android.util.Log;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.FinishedGameResponse;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.login.LoginResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.utils.Utils;

import java.lang.annotation.Annotation;

import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by mahmoudgalal on 5/24/16.
 */
public class LoginViewModel {
    private static final String TAG = LoginViewModel.class.getSimpleName();
    private Realm mRealm;
    private OnDataLoaded mListener;
    private OnDataLoaded mFacebookListener;
    private OnDataLoaded mGoogleLoginListener;
    private OnDataLoaded mResetPassListener;
    private NewMatchViewModel.OnError mErrorListener;

    private Context mContext;
    ApiHandler mApiHandler;

    public LoginViewModel(Context context, OnDataLoaded facebookListener, OnDataLoaded googleLoginListener, OnDataLoaded listener, OnDataLoaded resetPassListener, NewMatchViewModel.OnError errorListener) {
        mListener = listener;
        mContext = context;
        this.mFacebookListener = facebookListener;
        this.mGoogleLoginListener = googleLoginListener;
        this.mResetPassListener = resetPassListener;
        this.mErrorListener = errorListener;
    }


    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        mApiHandler = ApiHandler.getInstance();
    }


    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    public void loginUsingSocial(final String socialType, String socialKey, String deviceToken) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            postToListener(OnDataLoaded.ResponseStates.START);
            Call<LoginResponse> registerResponseCall = mApiHandler.getServices().loginBySocial(socialType, socialKey, 2, deviceToken);
            registerResponseCall.enqueue(new Callback<LoginResponse>() {

                @Override
                public void onResponse(Call<LoginResponse> call, final Response<LoginResponse> response) {
                    if (response.isSuccessful()) {
                        Log.d("response", response.body().getResult().getData().getUser().getUsername() + "");
                        Utils.saveUserIdToSharedPref(mContext, response.body().getResult().getData().getUser().getId());
                        PreferenceHelper.getInstance(mContext).setIsUserLogged(true);
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(response.body().getResult().getData().getUser());
                                if (socialType.equals("facebook")) {
                                    mFacebookListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                                } else {
                                    mGoogleLoginListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                                }
                            }
                        });

                    } else {
                        Converter<ResponseBody, ErrorResponse> converter = ApiHandler.getInstance().getRetrofit().responseBodyConverter(ErrorResponse.class, new Annotation[0]);

                        try {
                            ErrorResponse errors = converter.convert(response.errorBody());
                            Log.d(TAG, errors.getResult().getError());
                            if (socialType.equals("facebook")) {
                                mFacebookListener.downloadComplete(OnDataLoaded.ResponseStates.NO_DATA);
                            } else {
                                mGoogleLoginListener.downloadComplete(OnDataLoaded.ResponseStates.NO_DATA);
                            }
                        } catch (Exception e) {
                            Log.d("server error", "server error");
                            if (socialType.equals("facebook")) {
                                mFacebookListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            } else {
                                mGoogleLoginListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        }


                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    t.printStackTrace();
                    mFacebookListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            mFacebookListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public void loginUsingUsername(String username, String password, String deviceToken) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            postToListener(OnDataLoaded.ResponseStates.START);
            Call<LoginResponse> loginResponseCall = mApiHandler.getServices().loginByUsername(username, password, 2, deviceToken);
            loginResponseCall.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, final Response<LoginResponse> response) {
                    if (response.isSuccessful()) {
                        Log.d("response", response.body().getResult().getData().getUser().getUsername() + "");
                        Utils.saveUserIdToSharedPref(mContext, response.body().getResult().getData().getUser().getId());
                        PreferenceHelper.getInstance(mContext).setIsUserLogged(true);
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(response.body().getResult().getData().getUser());
                            }
                        });
                        postToListener(OnDataLoaded.ResponseStates.SUCCESS);
                    } else {
                        Converter<ResponseBody, ErrorResponse> converter = ApiHandler.getInstance().getRetrofit().responseBodyConverter(ErrorResponse.class, new Annotation[0]);

                        try {
                            ErrorResponse errors = converter.convert(response.errorBody());
                            Log.d(TAG, errors.getResult().getError());
                            postToListener(OnDataLoaded.ResponseStates.NO_DATA);
                        } catch (Exception e) {
                            Log.d("server error", "server error");
                            postToListener(OnDataLoaded.ResponseStates.ERROR);
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    t.printStackTrace();
                    postToListener(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            postToListener(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    protected void postToListener(OnDataLoaded.ResponseStates responseStates) {
        if (mListener != null) {
            mListener.downloadComplete(responseStates);
        }
    }

    public void resetPass(String email) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mResetPassListener.downloadComplete(OnDataLoaded.ResponseStates.START);
            mApiHandler.getServices().resetPass(email).enqueue(new Callback<FinishedGameResponse>() {
                @Override
                public void onResponse(Call<FinishedGameResponse> call, Response<FinishedGameResponse> response) {
                    if (response.isSuccessful()) {
                        mResetPassListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    } else {
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mErrorListener.showError(errorContent);
                        } else {
                            mErrorListener.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }
                }

                @Override
                public void onFailure(Call<FinishedGameResponse> call, Throwable t) {
                    mResetPassListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            mResetPassListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

}
