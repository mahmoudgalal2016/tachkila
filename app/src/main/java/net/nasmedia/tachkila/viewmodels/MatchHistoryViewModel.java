package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.Finished_game;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.FinishedGameResponse;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.viewgame.ViewGameResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 9/19/2016.
 */
public class MatchHistoryViewModel {

    Realm mRealm;
    Context mContext;
    int mGroupId;
    ApiHandler mApiHandler;
    OnDataLoaded mListener;
    NewMatchViewModel.OnError mErrorListener;

    public MatchHistoryViewModel(Context context, int groupId, OnDataLoaded listener, NewMatchViewModel.OnError errorListener) {
        mContext = context;
        mGroupId = groupId;
        mListener = listener;
        mErrorListener = errorListener;
        mApiHandler = ApiHandler.getInstance();
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
    }

    public void onStop() {
        mRealm.close();
    }

    public List<Finished_game> getMatchHistory() {
        List<Finished_game> games = new ArrayList<>();
        if (mGroupId == 0) {
            List<Group> groups = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getGroups();
            if (groups != null && groups.size() > 0) {
                for (Group group : groups) {
                    if (group.getFinished_games() != null && group.getFinished_games().size() > 0) {
                        for (Finished_game game : group.getFinished_games()) {
                            games.add(game);
                        }
                    }
                }
            }
        }else{
            Group group = mRealm.where(Group.class).equalTo("id", mGroupId).findFirst();
            if (group.getFinished_games() != null && group.getFinished_games().size() > 0) {
                for (Finished_game game : group.getFinished_games()) {
                    games.add(game);
                }
            }
        }
        return games;
    }

    public void updateGameScore(int gameId, int teamAScore, int teamBScore) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
            mApiHandler.getServices().updateGameScore(1, gameId, teamAScore, teamBScore).enqueue(new Callback<FinishedGameResponse>() {
                @Override
                public void onResponse(Call<FinishedGameResponse> call, final Response<FinishedGameResponse> response) {
                    if (response.isSuccessful()) {
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(response.body().getResult().getData());
                            }
                        });
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    } else {
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mErrorListener.showError(errorContent);
                        } else {
                            mErrorListener.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }
                }

                @Override
                public void onFailure(Call<FinishedGameResponse> call, Throwable t) {
                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

}
