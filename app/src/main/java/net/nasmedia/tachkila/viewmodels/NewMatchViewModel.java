package net.nasmedia.tachkila.viewmodels;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.Color;
import net.nasmedia.tachkila.realmmodels.GameType;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.creategames.CreateGameResponse;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.Utils;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import static net.nasmedia.tachkila.R.drawable.group;
import static net.nasmedia.tachkila.R.drawable.user;

/**
 * Created by mahmoudgalal on 10/20/16.
 */

public class NewMatchViewModel {

    Context mContext;
    Realm mRealm;
    OnDataLoaded mListener;
    OnError mErrorListener;


    public NewMatchViewModel(Context context, OnDataLoaded listener, OnError errorListener) {
        mContext = context;
        mListener = listener;
        mErrorListener = errorListener;
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
    }

    public void onStop() {
        mRealm.close();
    }

    public void createMatch(String dateTime, int gameTypeId, final int groupId, int teamAColorId, int teamBColorId, String location, double lng, double lat, String notes) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
            Call<CreateGameResponse> call = ApiHandler.getInstance().getServices().createGame(dateTime,
                    location, lat, lng, groupId, Utils.getSavedUserIdInSharedPref(mContext), gameTypeId, teamAColorId, teamBColorId, notes);
            call.enqueue(new Callback<CreateGameResponse>() {
                @Override
                public void onResponse(Call<CreateGameResponse> call, final Response<CreateGameResponse> response) {
                    if (response.isSuccessful()) {
                        Realm realm = Realm.getDefaultInstance();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.where(Group.class).equalTo("id", groupId).findFirst().getActive_games().add(response.body().getResult().getData());
                            }
                        });
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    } else {
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mErrorListener.showError(errorContent);
                        } else {
                            mErrorListener.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }
                }

                @Override
                public void onFailure(Call<CreateGameResponse> call, Throwable t) {
                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public List<Color> getChosserColors() {
        return mRealm.copyFromRealm(mRealm.where(Color.class).findAll());
    }

    public int getChossenColorId(int position) {
        return mRealm.copyFromRealm(mRealm.where(Color.class).findAll()).get(position).getId();
    }

    public int getGameTypeId(int position) {
        return mRealm.copyFromRealm(mRealm.where(GameType.class).findAll()).get(position).getId();
    }

    public ArrayList<String> getNumOfPlayersNames() {
        ArrayList<String> numOfPlayersNamesList = new ArrayList<>();
        List<GameType> gameTypes = mRealm.copyFromRealm(mRealm.where(GameType.class).findAll());
        for (GameType gameType : gameTypes) {
            numOfPlayersNamesList.add(gameType.getName());
        }
        return numOfPlayersNamesList;
    }

    public ArrayList<String> getGroupsNames() {
        ArrayList<String> groupsNameList = new ArrayList<>();
        List<Group> currentUserGroups = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getGroups();
        for (Group group : currentUserGroups) {
            groupsNameList.add(group.getName());
        }
        return groupsNameList;
    }

    public int getGroupId(int position) {
        return mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getGroups().get(position).getId();
    }

    public int getGroupPositionByGroupId(int groupId) {
        ArrayList<String> groupsNameList = new ArrayList<>();
        List<Group> currentUserGroups = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getGroups();

        for (int i = 0; i < currentUserGroups.size(); i++) {
            if (currentUserGroups.get(i).getId() == groupId) {
                return i;
            }
        }
        return 0;
    }

    public String getUserName() {
        return mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getName();
    }

    public interface OnError {
        void showError(String error);
    }

}
