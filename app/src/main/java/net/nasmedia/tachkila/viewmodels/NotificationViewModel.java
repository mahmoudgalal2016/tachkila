package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.Notification;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.notifications.NotificationResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.Utils;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 7/14/2016.
 */
public class NotificationViewModel implements RealmChangeListener<RealmResults<Notification>> {

    private Realm mRealm;
    private OnDataLoaded mListener;
    private Context mContext;
    List<Notification> mNotifications;

    public NotificationViewModel(Context context, OnDataLoaded listener) {
        mListener = listener;
        mContext = context;
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        RealmResults<Notification> notificationRealmResults = mRealm.where(Notification.class).findAllSortedAsync("id", Sort.DESCENDING);
        notificationRealmResults.addChangeListener(this);
        downloadNotifications(mRealm.where(Notification.class).count() == 0);
    }


    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    private void downloadNotifications(final boolean isDataEmpty) {
        if (isDataEmpty) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
        }
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            ApiHandler apiHandler = ApiHandler.getInstance();
            Call<NotificationResponse> call = apiHandler.getServices().getNotificationByUser(Utils.getSavedUserIdInSharedPref(mContext));
            call.enqueue(new Callback<NotificationResponse>() {
                @Override
                public void onResponse(Call<NotificationResponse> call, final Response<NotificationResponse> response) {
                    if (response.isSuccessful()) {

                        if (response.body().getResult().getData().getNotifications() != null) {
                            if (response.body().getResult().getData().getNotifications().size() > 0) {
                                mRealm.executeTransactionAsync(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(response.body().getResult().getData().getNotifications());
                                    }
                                });
                            } else {
                                mRealm.delete(Notification.class);
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_DATA);
                            }
                        } else {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    } else {
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                    }
                }

                @Override
                public void onFailure(Call<NotificationResponse> call, Throwable t) {
                    t.printStackTrace();
                    if (isDataEmpty)
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            if (isDataEmpty)
                mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public List<Notification> getNotifications() {
        return mNotifications;
    }

    @Override
    public void onChange(RealmResults<Notification> notifications) {
        if (notifications.size() > 0) {
            mNotifications = mRealm.copyFromRealm(notifications);
            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
        }
    }
}
