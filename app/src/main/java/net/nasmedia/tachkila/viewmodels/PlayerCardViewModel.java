package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.Position;
import net.nasmedia.tachkila.realmmodels.test.Country;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.PendingRequest;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.groups.ViewGroupResponse;
import net.nasmedia.tachkila.services.response.viewplayer.ViewPlayerResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.Utils;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 9/27/2016.
 */

public class PlayerCardViewModel {

    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    My_current_friend mMyCurrentFriend;
    int mPlayerId;
    ApiHandler mApiHandler;
    OnDataLoaded removeFriend;
    OnDataLoaded addFriend;
    NewMatchViewModel.OnError mErrorListener;


    public PlayerCardViewModel(Context context, int playerId, OnDataLoaded listener, OnDataLoaded addFriendLisnere, OnDataLoaded removeFriendLisnere, NewMatchViewModel.OnError errorListener) {
        mContext = context;
        mListener = listener;
        this.mPlayerId = playerId;
        mApiHandler = ApiHandler.getInstance();
        removeFriend = removeFriendLisnere;
        addFriend = addFriendLisnere;
        mErrorListener = errorListener;
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
//        My_current_friend myCurrentFriend = mRealm.where(My_current_friend.class).equalTo("id", mPlayerId).findFirst();
//        mMyCurrentFriend = myCurrentFriend;
//        mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
    }

    public void onStop() {
        mRealm.close();
    }


    public My_current_friend getMyCurrentFriend() {
        return mMyCurrentFriend;
    }

    public String getPostionName(int positionId) {
        Position position = mRealm.where(Position.class).equalTo("id", positionId).findFirst();
        if (position != null) {
            return position.getName();
        } else {
            return "";
        }
    }

    public String getCountryName(int countryId) {
        return mRealm.where(Country.class).equalTo("id", countryId).findFirst().getTitleEn();
    }

    public FriendState isUserInFriends(int id) {
        FriendState friendState = FriendState.NOT_FRIEND;
        PendingRequest pendingRequest = mRealm.where(PendingRequest.class).equalTo("id", mMyCurrentFriend.getId()).findFirst();
        if (pendingRequest != null) {
            friendState = FriendState.PENDING;
        }
        User myCurrentFriend = mRealm.where(User.class).equalTo("my_current_friends.id", id).findFirst();
        if (myCurrentFriend != null) {
            friendState = FriendState.FRIEND;
        } else if (pendingRequest == null) {
            friendState = FriendState.NOT_FRIEND;
        }
        return friendState;
    }

    public void addFriend() {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            addFriend.downloadComplete(OnDataLoaded.ResponseStates.START);
            mApiHandler.getServices().makeFriend(Utils.getSavedUserIdInSharedPref(mContext), mMyCurrentFriend.getId()).enqueue(new Callback<ViewGroupResponse>() {
                @Override
                public void onResponse(Call<ViewGroupResponse> call, Response<ViewGroupResponse> response) {
                    if (response.isSuccessful()) {
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                PendingRequest pendingRequest = new PendingRequest();
                                pendingRequest.setId(mMyCurrentFriend.getId());
                                realm.copyToRealmOrUpdate(pendingRequest);
                            }
                        });
                        addFriend.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    } else {
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mErrorListener.showError(errorContent);
                        } else {
                            mErrorListener.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ViewGroupResponse> call, Throwable t) {
                    addFriend.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            addFriend.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public void removeFriend() {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            addFriend.downloadComplete(OnDataLoaded.ResponseStates.START);
            mApiHandler.getServices().unFriend(Utils.getSavedUserIdInSharedPref(mContext), mMyCurrentFriend.getId()).enqueue(new Callback<ViewGroupResponse>() {
                @Override
                public void onResponse(Call<ViewGroupResponse> call, Response<ViewGroupResponse> response) {
                    if (response.isSuccessful()) {
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getMy_current_friends().remove(mMyCurrentFriend);
                            }
                        });
                        removeFriend.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    } else {
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mErrorListener.showError(errorContent);
                        } else {
                            mErrorListener.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ViewGroupResponse> call, Throwable t) {
                    removeFriend.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            addFriend.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }


    public void viewPlayer(int playerId) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
            mApiHandler.getServices().viewPlayer(playerId, Utils.getSavedUserIdInSharedPref(mContext)).enqueue(new Callback<ViewPlayerResponse>() {
                @Override
                public void onResponse(Call<ViewPlayerResponse> call, Response<ViewPlayerResponse> response) {
                    if (response.isSuccessful()) {
                        mMyCurrentFriend = response.body().getResult().getData();
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    } else {
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mErrorListener.showError(errorContent);
                        } else {
                            mErrorListener.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ViewPlayerResponse> call, Throwable t) {
                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                }
            });
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }


    public static enum FriendState {
        PENDING,
        NOT_FRIEND,
        FRIEND
    }
}
