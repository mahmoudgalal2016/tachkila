package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.fragments.PlayersFragment;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.utils.Utils;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Mahmoud Galal on 9/17/2016.
 */
public class PlayersViewModel {

    Realm mRealm;
    Context mContext;
    String mType;
    User mUser;

    public PlayersViewModel(Context context, String type) {
        this.mContext = context;
        this.mType = type;
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
    }

    public void onStop() {
        mRealm.close();
    }

    public List<My_current_friend> getList() {
        mUser = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
        if (mUser != null) {
            switch (mType) {
                case PlayersFragment.TYPE_PLAYERS:
                    return getCurrentFriends();
                case PlayersFragment.TYPE_REQUESTES:
                    return getCurrentRequests();
                case PlayersFragment.TYPE_SUGGESTED:
                    return getCurrentSuggested();
                default:
                    return null;
            }
        } else {
            return null;
        }

    }

    public List<My_current_friend> getCurrentFriends() {
        return mUser.getMy_current_friends();
    }

    public List<My_current_friend> getCurrentRequests() {
        return mUser.getCurrent_requests();
    }

    public List<My_current_friend> getCurrentSuggested() {
        return mUser.getPeople_you_may_now();
    }
}
