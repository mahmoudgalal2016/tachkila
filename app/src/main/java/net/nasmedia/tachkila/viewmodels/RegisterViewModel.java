package net.nasmedia.tachkila.viewmodels;

import android.content.Context;
import android.util.Log;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.Position;
import net.nasmedia.tachkila.realmmodels.test.Country;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.register.RegisterResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoudgalal on 5/21/16.
 */
public class RegisterViewModel {

    private static final String TAG = RegisterViewModel.class.getSimpleName();
    private Realm mRealm;
    private OnDataLoaded mListener;
    private Context mContext;
    private OnError mOnError;

    public RegisterViewModel(Context context, OnDataLoaded listener, OnError onError) {
        mContext = context;
        this.mListener = listener;
        mOnError = onError;
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
    }

    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }


    public void performRegistration(String avatarPath, String userName, String password, String email, String birthDate,
                                    String favNum, String favFoot, String name, String phone, String sex,
                                    String height, String deviceToken, String countryId, String socialType, String socialId, String positionId) {

        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);

            File avatarFile = null;
            if (avatarPath != null) {
                avatarFile = new File(avatarPath);
            }


            RequestBody requestFile = null;
            if (avatarFile != null) {
                requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
            }
            RequestBody userNameBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), userName);
            RequestBody emailBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), email);
            RequestBody birthDateBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), birthDate != null ? birthDate : "");
            RequestBody favNumBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), favNum != null ? favNum : "");
            RequestBody favFootBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), favFoot != null ? favFoot : "");
            RequestBody nameBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), name != null ? name : "");
            RequestBody phoneBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), phone != null ? phone : "");
            RequestBody sexBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), sex != null ? sex : "");
            RequestBody deviceType =
                    RequestBody.create(
                            MediaType.parse("text/plain"), "2");
            RequestBody heightBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), height != null ? height : "");
            RequestBody deviceTokenBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), deviceToken != null ? deviceToken : "");
            RequestBody countryIdBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), countryId);
            RequestBody socialTypeBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), socialType != null ? socialType : "");
            RequestBody socialKeyBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), socialId != null ? socialId : "");
            RequestBody passwordBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), password);
            RequestBody positionIdBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), positionId);

            Call<RegisterResponse> registerResponseCall = ApiHandler.getInstance().getServices().
                    registerUser(requestFile, userNameBody, passwordBody, nameBody, emailBody,
                            phoneBody, countryIdBody, sexBody,
                            favFootBody, favNumBody, positionIdBody, heightBody, birthDateBody,
                            deviceType, deviceTokenBody, socialTypeBody, socialKeyBody);


            registerResponseCall.enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(Call<RegisterResponse> call, final Response<RegisterResponse> response) {
                    if (response.isSuccessful()) {
                        Log.d("response", response.body().getResult().getData().getAvatar() + "");
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(response.body().getResult().getData());
                            }
                        });
                        Utils.saveUserIdToSharedPref(mContext, response.body().getResult().getData().getId());
                        PreferenceHelper.getInstance(mContext).setIsUserLogged(true);
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    } else {
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mOnError.showError(errorContent);
                        } else {
                            mOnError.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }
                }

                @Override
                public void onFailure(Call<RegisterResponse> call, Throwable t) {
                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                    Log.d("responseError", t.getMessage());
                }
            });
        }else{
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public ArrayList<String> getCountriesNames() {
        ArrayList<String> countriesNames = new ArrayList<>();
        RealmResults<Country> countries = mRealm.where(Country.class).findAll();
        for (Country country : countries) {
            countriesNames.add(country.getAlpha_2() + " +" + country.getPhonecode());
        }
        return countriesNames;
    }

    public List<Country> getCountries(){
        return mRealm.where(Country.class).findAll();
    }

    public int getKuwiatPosition(){
        RealmResults<Country> countries = mRealm.where(Country.class).findAll();
        for (Country country : countries) {
            if (country.getAlpha_2().equalsIgnoreCase("kw")) {
                return countries.indexOf(country);
            }
        }
        return 0;
    }

    public String geCountryId(int countryPosition) {
        RealmResults<Country> countries = mRealm.where(Country.class).findAll();

        return String.valueOf(countries.get(countryPosition).getId());
    }

    public ArrayList<String> getPositionsNames() {
        ArrayList<String> positionsNames = new ArrayList<>();
        RealmResults<Position> positions = mRealm.where(Position.class).findAll();
        for (Position country : positions) {
            positionsNames.add(country.getName());
        }
        return positionsNames;
    }

    public String gePositionId(int positionPosition) {
        RealmResults<Position> positions = mRealm.where(Position.class).findAll();

        return String.valueOf(positions.get(positionPosition).getId());
    }


    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public interface OnError {
        void showError(String error);
    }
}
