package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.searchuser.SearchUserNameResponse;
import net.nasmedia.tachkila.services.response.searchuser.UserSearch;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 11/11/2016.
 */

public class SearchUserViewModel {

    Context mContext;
    ApiHandler mApiHandler;
    OnDataLoaded mListener;
    Call<SearchUserNameResponse> mCall;
    List<UserSearch> mUserResultList;

    public SearchUserViewModel(Context context, OnDataLoaded listener) {
        this.mContext = context;
        this.mApiHandler = ApiHandler.getInstance();
        this.mListener = listener;
    }

    public void onStart() {

    }

    public void onStop() {
        if (mCall != null) {
            mCall.cancel();
        }
    }

    public void startSearch(String userName) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
            mCall = mApiHandler.getServices().searchByUsername(userName, String.valueOf(Utils.getSavedUserIdInSharedPref(mContext)));
            mCall.enqueue(new Callback<SearchUserNameResponse>() {
                @Override
                public void onResponse(Call<SearchUserNameResponse> call, Response<SearchUserNameResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getResult().getData().size() > 0) {
                            mUserResultList = response.body().getResult().getData();
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        } else {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_DATA);
                        }
                    } else {
//                        ErrorAbstract errorAbstract = ErrorUtils.parseError(response);
//                        errorAbstract.getErrorRegisterResponse();
                    }
                }

                @Override
                public void onFailure(Call<SearchUserNameResponse> call, Throwable t) {

                }
            });
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public List<UserSearch> getUserResultList() {
        return mUserResultList;
    }


}
