package net.nasmedia.tachkila.viewmodels;

import android.content.Context;
import android.util.Log;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.Paths;
import net.nasmedia.tachkila.realmmodels.Settings;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.settings.SettingsResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.utils.Utils;

import java.lang.annotation.Annotation;

import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by mahmoudgalal on 5/21/16.
 */
public class SplashViewModel {

    private static final String TAG = SplashViewModel.class.getSimpleName();

    private Realm mRealm;
    private OnDataLoaded mListener;
    private Context mContext;
    private Call<SettingsResponse> mCall;
    NewMatchViewModel.OnError mOnError;

    public SplashViewModel(Context context, OnDataLoaded listener, NewMatchViewModel.OnError errorListener) {
        mContext = context;
        mListener = listener;
        mOnError = errorListener;
    }

    public void onStart() {
        postToListener(OnDataLoaded.ResponseStates.START);
        mRealm = Realm.getDefaultInstance();
        if (!PreferenceHelper.getInstance(mContext).isUserLogged()) {
            mCall = ApiHandler.getInstance().getServices().getSettings();
        }else{
            mCall = ApiHandler.getInstance().getServices().getSettingsByUserId(Utils.getSavedUserIdInSharedPref(mContext));
        }
        loadSettingsData();
    }

    public void onStop() {
        if (mCall != null) {
            mCall.cancel();
        }

        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }

    }

    protected void postToListener(OnDataLoaded.ResponseStates responseStates) {
        if (mListener != null) {
            mListener.downloadComplete(responseStates);
        }
    }


    private void loadSettingsData() {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mCall.enqueue(new Callback<SettingsResponse>() {
                @Override
                public void onResponse(Call<SettingsResponse> call, final Response<SettingsResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            mRealm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    Settings settings = response.body().getSettingsResult().getData().getSettings();
                                    settings.setPrimaryKet(1);
                                    realm.copyToRealmOrUpdate(settings);
                                    Paths paths = response.body().getSettingsResult().getData().getPaths();
                                    paths.setPrimaryKey(1);
                                    realm.copyToRealmOrUpdate(paths);
                                    if (response.body().getSettingsResult().getData().getUser() != null)
                                        realm.copyToRealmOrUpdate(response.body().getSettingsResult().getData().getUser());
                                    if (response.body().getSettingsResult().getData().getColors() != null)
                                        realm.copyToRealmOrUpdate(response.body().getSettingsResult().getData().getColors());
                                    if (response.body().getSettingsResult().getData().getActiveGames() != null)
                                        realm.copyToRealmOrUpdate(response.body().getSettingsResult().getData().getActiveGames());
                                    if (response.body().getSettingsResult().getData().getCountries() != null)
                                        realm.copyToRealmOrUpdate(response.body().getSettingsResult().getData().getCountries());
                                    if (response.body().getSettingsResult().getData().getGameTypes() != null)
                                        realm.copyToRealmOrUpdate(response.body().getSettingsResult().getData().getGameTypes());
                                    if (response.body().getSettingsResult().getData().getPositions() != null)
                                        realm.copyToRealmOrUpdate(response.body().getSettingsResult().getData().getPositions());

                                    postToListener(OnDataLoaded.ResponseStates.SUCCESS);
                                }
                            });

                        } else {
                            postToListener(OnDataLoaded.ResponseStates.NO_DATA);
                        }
                    } else {
                        ErrorAbstract errorRegisterResponse = ErrorUtils.parseError(response);

                        if (errorRegisterResponse.getErrorRegisterResponse() != null) {
                            String errorContent = "";
                            for (String string : errorRegisterResponse.getErrorRegisterResponse().getResult().getError()) {
                                errorContent += string + "\n";
                            }
                            mOnError.showError(errorContent);
                        } else {
                            mOnError.showError(errorRegisterResponse.getErrorResponse().getResult().getError());
                        }
                    }

                }

                @Override
                public void onFailure(Call<SettingsResponse> call, Throwable t) {
                    if (call.isCanceled()) {
                        Log.d(TAG, "request is cancelled");
                    } else {
                        postToListener(OnDataLoaded.ResponseStates.ERROR);
                    }
                }
            });
        } else {
            if (mRealm.where(Settings.class).count() > 0) {
                postToListener(OnDataLoaded.ResponseStates.SUCCESS);
            } else {
                postToListener(OnDataLoaded.ResponseStates.NO_NETWORK);
            }
        }
    }


}
