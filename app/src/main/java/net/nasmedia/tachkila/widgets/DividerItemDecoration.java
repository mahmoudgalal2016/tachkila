package net.nasmedia.tachkila.widgets;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private int mItemOffset;
    private boolean mIsFirstItemPadding = true;

    public DividerItemDecoration(int itemOffset) {
        mItemOffset = itemOffset;
    }

    public DividerItemDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
    }

    public DividerItemDecoration(@NonNull Context context, @DimenRes int itemOffsetId, boolean firstItemPadding) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
        mIsFirstItemPadding = firstItemPadding;
    }
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        if (mIsFirstItemPadding && parent.getChildAdapterPosition(view) == 0) {
            outRect.set(0, mItemOffset, 0, mItemOffset);
        } else {
            outRect.set(0, 0, 0, mItemOffset);
        }
    }
}