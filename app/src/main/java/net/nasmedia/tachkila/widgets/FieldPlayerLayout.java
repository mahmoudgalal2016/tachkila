package net.nasmedia.tachkila.widgets;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.GamePlayersWithCaptain;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Mahmoud Galal on 20/11/2016.
 */

public class FieldPlayerLayout extends LinearLayout {

    Context mContext;
    GamePlayersWithCaptain mPlayer;
    @BindView(R.id.userImage)
    ImageView userImage;
    @BindView(R.id.userName)
    CustomTextView userName;
    @BindView(R.id.userContainer)
    LinearLayout userContainer;
    @BindView(R.id.imageContainer)
    FrameLayout imageContainer;

    public FieldPlayerLayout(Context context) {
        super(context);
    }

    public FieldPlayerLayout(Context context, GamePlayersWithCaptain player) {
        super(context);
        mContext = context;
        mPlayer = player;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.layout_field_player, this);
        ButterKnife.bind(this, v);
        GradientDrawable teamAbackground = (GradientDrawable) imageContainer.getBackground();
        teamAbackground.setStroke(2, mContext.getResources().getColor(android.R.color.transparent));

        initialize();
    }

    private void initialize() {
        userName.setText(mPlayer.getUserName());
        Glide.with(mContext)
                .load(Constants.PIC_AVATAR_URL + mPlayer.getAvatar())
                .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
                .into(userImage);
    }

    public void changeStroke(int color) {
        GradientDrawable teamAbackground = (GradientDrawable) imageContainer.getBackground();
        teamAbackground.setStroke(2, color);
    }
}
