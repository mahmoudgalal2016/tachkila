package net.nasmedia.tachkila.widgets;

import android.content.ClipData;
import android.content.Context;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.utils.GamePlayersWithCaptain;
import net.nasmedia.tachkila.utils.Utils;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 19/11/2016.
 */

public class MatchFieldLayout extends LinearLayout {


    MatchFieldLayout mField;
    Context mContext;
    List<GamePlayersWithCaptain> mGamePlayersList;
    OnFieldAction mListener;


    @BindView(R.id.teamALeftTop)
    LinearLayout teamALeftTop;
    @BindView(R.id.teamALeftCenter)
    LinearLayout teamALeftCenter;
    @BindView(R.id.teamALeftBottom)
    LinearLayout teamALeftBottom;
    @BindView(R.id.teamACenterTop)
    LinearLayout teamACenterTop;
    @BindView(R.id.teamACenterCenter)
    LinearLayout teamACenterCenter;
    @BindView(R.id.teamACenterBottom)
    LinearLayout teamACenterBottom;
    @BindView(R.id.teamARightTop)
    LinearLayout teamARightTop;
    @BindView(R.id.teamARightCenter)
    LinearLayout teamARightCenter;
    @BindView(R.id.teamARightBottom)
    LinearLayout teamARightBottom;
    @BindView(R.id.teamBLeftTop)
    LinearLayout teamBLeftTop;
    @BindView(R.id.teamBLeftCenter)
    LinearLayout teamBLeftCenter;
    @BindView(R.id.teamBLeftBottom)
    LinearLayout teamBLeftBottom;
    @BindView(R.id.teamBCenterTop)
    LinearLayout teamBCenterTop;
    @BindView(R.id.teamBCenterCenter)
    LinearLayout teamBCenterCenter;
    @BindView(R.id.teamBCenterBottom)
    LinearLayout teamBCenterBottom;
    @BindView(R.id.teamBRightTop)
    LinearLayout teamBRightTop;
    @BindView(R.id.teamBRightCenter)
    LinearLayout teamBRightCenter;
    @BindView(R.id.teamBRightBottom)
    LinearLayout teamBRightBottom;
    @BindView(R.id.container)
    LinearLayout container;

    GamePlayersWithCaptain mPlayer;
    @BindView(R.id.centerCenter)
    LinearLayout centerCenter;

    HashMap<Integer, int[]> fieldMap = new HashMap<>();
    private int positionX;
    private int positionY;
    private int teamAColor;
    private int teamBColor;

    public MatchFieldLayout(Context context) {
        super(context);
        mContext = context;
        mField = this;
    }

    public MatchFieldLayout(Context context, List<GamePlayersWithCaptain> gamePlayers, int teamAColor, int teamBColor, OnFieldAction listener) {
        super(context);
        mContext = context;
        mField = this;
        mGamePlayersList = gamePlayers;
        mListener = listener;
        this.teamAColor = teamAColor;
        this.teamBColor = teamBColor;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.layout_match_field, this);
        ButterKnife.bind(this, v);


        initialize();
        initializeMap();
    }

    private void initializeMap() {

        fieldMap.put(R.id.teamALeftTop, new int[]{1, 1});
        fieldMap.put(R.id.teamALeftCenter, new int[]{1, 2});
        fieldMap.put(R.id.teamALeftBottom, new int[]{1, 3});

        fieldMap.put(R.id.teamACenterTop, new int[]{2, 1});
        fieldMap.put(R.id.teamACenterCenter, new int[]{2, 2});
        fieldMap.put(R.id.teamACenterBottom, new int[]{2, 3});

        fieldMap.put(R.id.teamARightTop, new int[]{3, 1});
        fieldMap.put(R.id.teamARightCenter, new int[]{3, 2});
        fieldMap.put(R.id.teamARightBottom, new int[]{3, 3});

        fieldMap.put(R.id.teamBLeftTop, new int[]{4, 1});
        fieldMap.put(R.id.teamBLeftCenter, new int[]{4, 2});
        fieldMap.put(R.id.teamBLeftBottom, new int[]{4, 3});

        fieldMap.put(R.id.teamBCenterTop, new int[]{5, 1});
        fieldMap.put(R.id.teamBCenterCenter, new int[]{5, 2});
        fieldMap.put(R.id.teamBCenterBottom, new int[]{5, 3});

        fieldMap.put(R.id.teamBRightTop, new int[]{6, 1});
        fieldMap.put(R.id.teamBRightCenter, new int[]{6, 2});
        fieldMap.put(R.id.teamBRightBottom, new int[]{6, 3});


        for (GamePlayersWithCaptain gamePlayersWithCaptain : mGamePlayersList) {
            int positionX = gamePlayersWithCaptain.getPositionX();
            int positionY = gamePlayersWithCaptain.getPositionY();

            FieldPlayerLayout view = new FieldPlayerLayout(mContext, gamePlayersWithCaptain);
            boolean isMyPlayer = false;
            if (gamePlayersWithCaptain.getId() == Utils.getSavedUserIdInSharedPref(mContext)) {
                view.setOnTouchListener(new MyTouchListener());
                isMyPlayer = true;

                this.positionX = gamePlayersWithCaptain.getPositionX();
                this.positionY = gamePlayersWithCaptain.getPositionY();
            }
            //team A

            if (isMyPlayer && (positionX == 1 || positionX == 2 || positionX == 3)) {
                mListener.chosenTeam(1);
                view.changeStroke(teamAColor);
            } else if (isMyPlayer && (positionX == 4 || positionX == 5 || positionX == 6)) {
                mListener.chosenTeam(2);
                view.changeStroke(teamBColor);
            }

            if (positionX == 1 || positionX == 2 || positionX == 3) {
                view.changeStroke(teamAColor);
            } else if (positionX == 4 || positionX == 5 || positionX == 6) {
                view.changeStroke(teamBColor);
            }

            if (positionX == 1 && positionY == 1) {
                teamALeftTop.addView(view);
            } else if (positionX == 1 && positionY == 2) {
                teamALeftCenter.addView(view);
            } else if (positionX == 1 && positionY == 3) {
                teamALeftBottom.addView(view);
            } else if (positionX == 2 && positionY == 1) {
                teamACenterTop.addView(view);
            } else if (positionX == 2 && positionY == 2) {
                teamACenterCenter.addView(view);
            } else if (positionX == 2 && positionY == 3) {
                teamACenterBottom.addView(view);
            } else if (positionX == 3 && positionY == 1) {
                teamARightTop.addView(view);
            } else if (positionX == 3 && positionY == 2) {
                teamARightCenter.addView(view);
            } else if (positionX == 3 && positionY == 3) {
                teamARightBottom.addView(view);
            }


            // team B
            else if (positionX == 4 && positionY == 1) {
                teamBLeftTop.addView(view);
            } else if (positionX == 4 && positionY == 2) {
                teamBLeftCenter.addView(view);
            } else if (positionX == 4 && positionY == 3) {
                teamBLeftBottom.addView(view);
            } else if (positionX == 5 && positionY == 1) {
                teamBCenterTop.addView(view);
            } else if (positionX == 5 && positionY == 2) {
                teamBCenterCenter.addView(view);
            } else if (positionX == 5 && positionY == 3) {
                teamBCenterBottom.addView(view);
            } else if (positionX == 6 && positionY == 1) {
                teamBRightTop.addView(view);
            } else if (positionX == 6 && positionY == 2) {
                teamBRightCenter.addView(view);
            } else if (positionX == 6 && positionY == 3) {
                teamBRightBottom.addView(view);
            }
        }
    }


    private void initialize() {

        container.setOnDragListener(new MyDragListener());
        teamALeftBottom.setOnDragListener(new MyDragListener());
        teamALeftCenter.setOnDragListener(new MyDragListener());
        teamALeftTop.setOnDragListener(new MyDragListener());
        teamACenterBottom.setOnDragListener(new MyDragListener());
        teamACenterTop.setOnDragListener(new MyDragListener());
        teamACenterCenter.setOnDragListener(new MyDragListener());
        teamARightBottom.setOnDragListener(new MyDragListener());
        teamARightTop.setOnDragListener(new MyDragListener());
        teamARightCenter.setOnDragListener(new MyDragListener());
        teamBLeftBottom.setOnDragListener(new MyDragListener());
        teamBLeftTop.setOnDragListener(new MyDragListener());
        teamBLeftCenter.setOnDragListener(new MyDragListener());
        teamBCenterBottom.setOnDragListener(new MyDragListener());
        teamBCenterTop.setOnDragListener(new MyDragListener());
        teamBCenterCenter.setOnDragListener(new MyDragListener());
        teamBRightBottom.setOnDragListener(new MyDragListener());
        teamBRightCenter.setOnDragListener(new MyDragListener());
        teamBRightTop.setOnDragListener(new MyDragListener());


    }

    public MatchFieldLayout getView() {
        return mField;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    private final class MyTouchListener implements OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                DragShadowBuilder shadowBuilder = new DragShadowBuilder(
                        view);
                view.startDrag(data, shadowBuilder, view, 0);
                view.setVisibility(View.INVISIBLE);
                return true;
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                Log.d("Action", "up");
                return false;
            } else {
                return false;
            }
        }
    }

    class MyDragListener implements OnDragListener {
//        Drawable enterShape = getResources().getDrawable(
//                R.drawable.shape_droptarget);
//        Drawable normalShape = getResources().getDrawable(R.drawable.shape);

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
//                    v.setBackgroundDrawable(enterShape);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
//                    v.setBackgroundDrawable(normalShape);
                    if (v.getId() == R.id.container) {
                        Log.d("container", "exit");
                        View view = (View) event.getLocalState();
                        ViewGroup owner = (ViewGroup) view.getParent();
                        owner.removeView(view);
                        positionX = 0;
                        positionY = 0;
                        mListener.removePlayerFromField(mPlayer);
                    }
                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    FieldPlayerLayout view = (FieldPlayerLayout) event.getLocalState();
                    if (v.getId() != R.id.container) {
                        ViewGroup owner = (ViewGroup) view.getParent();
                        owner.removeView(view);
                        LinearLayout container = (LinearLayout) v;
                        container.addView(view);

                        positionX = fieldMap.get(container.getId())[0];
                        positionY = fieldMap.get(container.getId())[1];

                        if (container.getId() == R.id.teamALeftTop || container.getId() == R.id.teamALeftCenter || container.getId() == R.id.teamALeftBottom ||
                                container.getId() == R.id.teamACenterTop || container.getId() == R.id.teamACenterCenter || container.getId() == R.id.teamACenterBottom ||
                                container.getId() == R.id.teamARightTop || container.getId() == R.id.teamARightBottom || container.getId() == R.id.teamARightCenter) {
                            mListener.chosenTeam(1);
                            view.changeStroke(teamAColor);

                        } else {
                            mListener.chosenTeam(2);
                            view.changeStroke(teamBColor);
                        }

                    }
                    view.setVisibility(View.VISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
//                    v.setBackgroundDrawable(normalShape);
                default:
                    break;
            }
            return true;
        }
    }

    public void setNewPlayer(GamePlayersWithCaptain player) {
        mPlayer = player;
        View view = new FieldPlayerLayout(mContext, player);
        view.setOnTouchListener(new MyTouchListener());
        centerCenter.addView(view);
    }

    public interface OnFieldAction {
        void removePlayerFromField(GamePlayersWithCaptain player);

        void chosenTeam(int team);
    }

}
